'use strict';

const Enum = require('./enums');
const AgentDevice = require('./AgentDevice');
const AgentAction = require('./AgentAction');
const Agent = require('./Agent');
const ObsAgent = require('./ObsAgent');
const MQTT = require('./MQTTJS');
/*$REQUIRE_PLUGINS$*/



const inst_device = new AgentDevice('device', null);
inst_device.AgentDevice_agents_var = [];
for (var i = 0; i < 1; i++){
inst_device.AgentDevice_agents_var.push(new Agent.Agent());
}
inst_device.AgentDevice_device_obsAgent_var = [];
for (var i = 0; i < 1; i++){
inst_device.AgentDevice_device_obsAgent_var.push(new ObsAgent.ObsAgent());
}
inst_device.AgentDevice_agent_action_var = [];
for (var i = 0; i < 1; i++){
inst_device.AgentDevice_agent_action_var.push(new AgentAction.AgentAction());
}
inst_device.AgentDevice_start_var = false;
inst_device.AgentDevice_damping_var = 0.25;
inst_device.AgentDevice_dt_var = 0.1;

/*$PLUGINS$*/
var options = {
    host: '183.230.40.39',
    port: '6002',
    protocol: 'mqtt',
    clean: false,
	}
options.clientId = '661610255'
options.username = '394823'
options.password = 'Fl=NCZdBophMSaaNCUqjlKAtsag='
const mqtt = new MQTT("MQTT", options, ['done','reset'], ['singleAgentObsMsg'], inst_device);
/*Connecting internal ports...*/
/*Connecting ports...*/
inst_device.bus.on('realMsg_port!singleAgentObsMsg', (singleAgentObsMsg) => mqtt.receivesingleAgentObsMsgOnrealMsg_port(singleAgentObsMsg));

/*$PLUGINS_CONNECTORS$*/
inst_device._init();
/*$PLUGINS_END$*/

function terminate() {
	inst_device._stop();
	inst_device._delete();
};
/*terminate all things on SIGINT (e.g. CTRL+C)*/
if (process && process.on) {
	process.on('SIGINT', function() {
		terminate();
		mqtt._stop();
/*$STOP_PLUGINS$*/

		setTimeout(() => {
			process.exit();
		}, 1000);
	});
}
