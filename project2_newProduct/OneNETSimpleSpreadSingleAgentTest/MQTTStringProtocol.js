function MQTTStringProtocol(){
MQTTStringProtocol.prototype.parse = function(json) {
const msg = {};
try {
const parsed = JSON.parse(json);
JSON.parse(json, function(k, v) {
switch(k) {
case 'reset':
msg._msg = 'reset';
msg.y = parsed.reset.y;
break;
case 'actionCmdMsg':
msg._msg = 'actionCmdMsg';
msg.agent_action = parsed.actionCmdMsg.agent_action;
break;
case 'done':
msg._msg = 'done';
msg.y = parsed.done.y;
break;
default: break;
}
});
} catch (err) {
console.log("Cannot parse " + json + " because " + err);
};
return msg;
};

MQTTStringProtocol.prototype.singleAgentObsMsgToFormat = function(AgentObs) {
return JSON.stringify({singleAgentObsMsg: {AgentObs : AgentObs}});
};

};

module.exports = MQTTStringProtocol;
