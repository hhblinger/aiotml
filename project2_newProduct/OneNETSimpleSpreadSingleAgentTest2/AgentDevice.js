'use strict';

const Enum = require('./enums');
const Event = require('./events');
const StateJS = require('@steelbreeze/state');
const EventEmitter = require('events').EventEmitter;
const util = require('util');


/*
 * Definition for type : AgentDevice
 */

function AgentDevice(name, root) {
	this.name = name;
	this.root = (root === null)? this : root;
	this.ready = false;
	this.bus = (root === null)? new EventEmitter() : this.root.bus;
	
	this.build(name);
}

AgentDevice.prototype.build = function(session) {
	/*State machine (states and regions)*/
	/*Building root component*/
	this._statemachine = new StateJS.State('Test');
	let _initial_AgentDevice_Test = new StateJS.PseudoState('_initial', this._statemachine, StateJS.PseudoStateKind.Initial);
	let AgentDevice_Test_Init = new StateJS.State('Init', this._statemachine).entry(() => {
		((process.stdout && process.stdout.write) || console.log).call(process.stdout, ''+'init\n ');
		this.Envinit();
		setImmediate(() => {this.bus.emit('realMsg_port!singleAgentObsMsg', new Event.SingleAgentObsMsg('realMsg_port', this.AgentDevice_device_obsAgent_var))});
	});
	let AgentDevice_Test_set_action = new StateJS.State('set_action', this._statemachine).entry(() => {
		((process.stdout && process.stdout.write) || console.log).call(process.stdout, ''+'set_action\n ');
	});
	let AgentDevice_Test_step = new StateJS.State('step', this._statemachine).entry(() => {
		((process.stdout && process.stdout.write) || console.log).call(process.stdout, ''+'step\n ');
		this.step();
	});
	let AgentDevice_Test_stop = new StateJS.State('stop', this._statemachine).entry(() => {
		((process.stdout && process.stdout.write) || console.log).call(process.stdout, ''+'stop\n ');
	});
	_initial_AgentDevice_Test.to(AgentDevice_Test_Init);
	AgentDevice_Test_Init.to(AgentDevice_Test_set_action);
	AgentDevice_Test_step.to(AgentDevice_Test_set_action).effect(() => {
		setImmediate(() => {this.bus.emit('realMsg_port!singleAgentObsMsg', new Event.SingleAgentObsMsg('realMsg_port', this.AgentDevice_device_obsAgent_var))});
	});
	AgentDevice_Test_set_action.to(AgentDevice_Test_step).on(Event.ActionCmdMsg).when((actionCmdMsg) => {
		return actionCmdMsg.port === 'realMsg_port' && actionCmdMsg.type === 'actionCmdMsg';
	}).effect((actionCmdMsg) => {
		this.set_action(actionCmdMsg.agent_action);
	});
	AgentDevice_Test_set_action.to(AgentDevice_Test_Init).on(Event.Reset).when((reset) => {
		return reset.port === 'realMsg_port' && reset.type === 'reset';
	});
	AgentDevice_Test_set_action.to(AgentDevice_Test_stop).on(Event.Done).when((done) => {
		return done.port === 'realMsg_port' && done.type === 'done';
	});
}
AgentDevice.prototype.Envinit = function() {
	this.AgentDevice_agents_var[0].name=util.format('agent %d', 0);;
	this.AgentDevice_agents_var[0].silent=true;
	this.AgentDevice_agents_var[0].pos[0] = -0.2;
	this.AgentDevice_agents_var[0].pos[1] = 0.2;
	this.AgentDevice_agents_var[0].goalIndex=0;
	this.AgentDevice_agents_var[0].vel[0] = 0;
	this.AgentDevice_agents_var[0].vel[1] = 0;
	this.AgentDevice_agents_var[0].p_force[0] = 0;
	this.AgentDevice_agents_var[0].p_force[1] = 0;
	this.AgentDevice_agents_var[0].action_u[0] = 0;
	this.AgentDevice_agents_var[0].action_u[1] = 0;
	this.AgentDevice_device_obsAgent_var[0].index=2;
	this.AgentDevice_device_obsAgent_var[0].vel[0] = this.AgentDevice_agents_var[0].vel[0];
	this.AgentDevice_device_obsAgent_var[0].vel[1] = this.AgentDevice_agents_var[0].vel[1];
	this.AgentDevice_device_obsAgent_var[0].pos[0] = this.AgentDevice_agents_var[0].pos[0];
	this.AgentDevice_device_obsAgent_var[0].pos[1] = this.AgentDevice_agents_var[0].pos[1];
}

AgentDevice.prototype.set_action = function(AgentDevice_set_action_pAgents_var) {
	if(this.AgentDevice_agents_var[0].movable === true) {
	this.AgentDevice_agents_var[0].p_force[0] = AgentDevice_set_action_pAgents_var.force[0];
	this.AgentDevice_agents_var[0].p_force[1] = AgentDevice_set_action_pAgents_var.force[1];
	
	}
}

AgentDevice.prototype.step = function() {
	this.apply_action();
	this.update_state();
}

AgentDevice.prototype.apply_action = function() {
	
}

AgentDevice.prototype.update_state = function() {
	this.AgentDevice_agents_var[0].vel[0] = this.AgentDevice_agents_var[0].vel[0] * (1 - this.AgentDevice_damping_var);
	this.AgentDevice_agents_var[0].vel[1] = this.AgentDevice_agents_var[0].vel[1] * (1 - this.AgentDevice_damping_var);
	this.AgentDevice_agents_var[0].vel[0] = this.AgentDevice_agents_var[0].vel[0] + this.AgentDevice_agents_var[0].p_force[0] / this.AgentDevice_agents_var[0].mass * this.AgentDevice_dt_var;
	this.AgentDevice_agents_var[0].vel[1] = this.AgentDevice_agents_var[0].vel[1] + this.AgentDevice_agents_var[0].p_force[1] / this.AgentDevice_agents_var[0].mass * this.AgentDevice_dt_var;
	this.AgentDevice_agents_var[0].pos[0] = this.AgentDevice_agents_var[0].pos[0] + this.AgentDevice_agents_var[0].vel[0] * this.AgentDevice_dt_var;
	this.AgentDevice_agents_var[0].pos[1] = this.AgentDevice_agents_var[0].pos[1] + this.AgentDevice_agents_var[0].vel[1] * this.AgentDevice_dt_var;
	this.AgentDevice_device_obsAgent_var[0].vel[0] = this.AgentDevice_agents_var[0].vel[0];
	this.AgentDevice_device_obsAgent_var[0].vel[1] = this.AgentDevice_agents_var[0].vel[1];
	this.AgentDevice_device_obsAgent_var[0].pos[0] = this.AgentDevice_agents_var[0].pos[0];
	this.AgentDevice_device_obsAgent_var[0].pos[1] = this.AgentDevice_agents_var[0].pos[1];
}

AgentDevice.prototype._stop = function() {
	this.root = null;
	this.ready = false;
}

AgentDevice.prototype._delete = function() {
	this._statemachine = null;
	this._Test_instance = null;
	this.bus.removeAllListeners();
}

AgentDevice.prototype._init = function() {
	this._Test_instance = new StateJS.Instance("Test_instance", this._statemachine);
	this.ready = true;
}

AgentDevice.prototype._receive = function(msg) {
	if (this.ready) {
		this._Test_instance.evaluate(msg);
	} else {
		setTimeout(()=>this._receive(msg),0);
	}
}

AgentDevice.prototype.toString = function() {
	let result = 'instance ' + this.name + ':' + this.constructor.name + '\n';
	result += '\n\tdamping = ' + this.AgentDevice_damping_var;
	result += '\n\tstart = ' + this.AgentDevice_start_var;
	result += '\n\tdt = ' + this.AgentDevice_dt_var;
	result += '';
	return result;
}

module.exports = AgentDevice;
