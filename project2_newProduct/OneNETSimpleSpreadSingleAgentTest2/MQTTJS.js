const mqtt_lib = require('mqtt');
const Format = require('./MQTTStringProtocol');
const events = require('./events')

function MQTT(name, options, subtopic, pubtopic, instance) {
    this.name = name;
    this.ready = false;
    this.subtopic = subtopic;
    this.pubtopic = pubtopic;
    this.formatter = new Format();
    this.client = mqtt_lib.connect(options);
    var client = this.client;
    var formatter = this.formatter
    this.client.on('connect', () => {
        subtopic.forEach(topic => {
            client.subscribe(topic);
    
        });
    });

   	this.client.on('message', (topic, payload,packet) => {
      console.log('receive:' + payload.toString());
            const msg = formatter.parse(payload);
            /*$DISPATCH$*/
msg._port = 'realMsg_port';
switch(msg._msg) {
case'actionCmdMsg':
var outEvent = new events.ActionCmdMsg('realMsg_port', msg.agent_action);
instance._receive(outEvent);
break;
case'reset':
var outEvent = new events.Reset('realMsg_port', msg.y);
instance._receive(outEvent);
break;
case'done':
var outEvent = new events.Done('realMsg_port', msg.y);
instance._receive(outEvent);
break;
default: break;
}

        
   	});
}

/*$RECEIVERS$*/
MQTT.prototype.receivesingleAgentObsMsgOnrealMsg_port = function(singleAgentObsMsg) {
  console.log('send singleAgentObsMsg');
  this.client.publish('singleAgentObsMsg', this.formatter.singleAgentObsMsgToFormat(singleAgentObsMsg.AgentObs), {qos: 0, retain: true}, function(error, packet) {
    if(error != null){
      console.log("publish error:",error);
      console.log("packet:", packet);
    }
  });
};



MQTT.prototype._stop = function() {
	this.client.end();
};

module.exports = MQTT;
