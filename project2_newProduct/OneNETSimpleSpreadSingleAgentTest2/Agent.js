function Agent(){
this.name = null;
this.index = 0;
this.pos = [];
this.movable = true;
this.silent = false;
this.u_noise = 0;
this.u_range = 1.0;
this.size = 0.05;
this.mass = 1;
this.collide = true;
this.goalIndex = 0;
this.action_u = [];
this.action_c = [];
this.sensitivity = 5;
this.p_force = [];
this.vel = [];
this.device_id = null;
}
exports.Agent=Agent;
