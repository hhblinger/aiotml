'use strict';

const Enum = require('./enums');
const Environment = require('./Environment');
const Landmark = require('./Landmark');
const AgentAction = require('./AgentAction');
const Agent = require('./Agent');
const OneNET = require('./OneNETMQTTJS');
/*$REQUIRE_PLUGINS$*/

const websocket = require('./WSJS');
const inst_testdd = new Environment('testdd', null);
inst_testdd.Environment_landmarks_var = [];
for (var i = 0; i < 3; i++){
inst_testdd.Environment_landmarks_var.push(new Landmark.Landmark());
}
inst_testdd.Environment_agents_var = [];
for (var i = 0; i < 3; i++){
inst_testdd.Environment_agents_var.push(new Agent.Agent());
}
inst_testdd.Environment_agentaction_var = [];
for (var i = 0; i < 6; i++){
inst_testdd.Environment_agentaction_var.push(new AgentAction.AgentAction());
}
inst_testdd.Environment_sim_var = true;
inst_testdd.Environment_agent_num_var = 3;
inst_testdd.Environment_landmark_num_var = 3;
inst_testdd.Environment_entity_num_var = 6;
inst_testdd.Environment_contact_force_var = 100;
inst_testdd.Environment_contact_margin_var = 0.001;
inst_testdd.Environment_dt_var = 0.1;
inst_testdd.Environment_damping_var = 0.25;
inst_testdd.Environment_start_var = false;

/*$PLUGINS$*/
var options = {
    host: '183.230.40.39',
    port: '6002',
    protocol: 'mqtt',
    clean: false,
	}
options.clientId = '661614827'
options.username = '394823'
options.password = 'Fl=NCZdBophMSaaNCUqjlKAtsag='
const onenet = new OneNET("OneNET", options, ['singleAgentObsMsg','singleLandmarkObsMsg'], ['done','reset'], inst_testdd);
onenet.getOneNETdevices();

const ws = new websocket("WS", false, 9010, inst_testdd);

/*Connecting internal ports...*/
/*Connecting ports...*/
inst_testdd.bus.on('msg_port!obsmsg', (obsmsg) => ws.receiveobsmsgOnmsg_port(obsmsg));

inst_testdd.bus.on('realMsg_port!actionmsg', (actionmsg) => onenet.mqtt.receiveactionmsgOnrealMsg_port(actionmsg));
inst_testdd.bus.on('realMsg_port!reset', (reset) => onenet.mqtt.receiveresetOnrealMsg_port(reset));
inst_testdd.bus.on('realMsg_port!done', (done) => onenet.mqtt.receivedoneOnrealMsg_port(done));

/*$PLUGINS_CONNECTORS$*/
inst_testdd._init();
/*$PLUGINS_END$*/

function terminate() {
	inst_testdd._stop();
	inst_testdd._delete();
};
/*terminate all things on SIGINT (e.g. CTRL+C)*/
if (process && process.on) {
	process.on('SIGINT', function() {
		terminate();
		ws._stop();
		onenet.mqtt._stop();
/*$STOP_PLUGINS$*/


		setTimeout(() => {
			process.exit();
		}, 1000);
	});
}
