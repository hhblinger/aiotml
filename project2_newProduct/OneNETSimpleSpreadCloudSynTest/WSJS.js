const WebSocketServer = require('ws').Server;
const events = require('./events');
const Format = require('./WSStringProtocol');
global.gws = null;
function WS(name, debug, portnum, instance) {
    this.name = name;
    this.debug = debug;
    this.ready = false;

    this.formatter = new Format();
    this.wss = new WebSocketServer({port:portnum});

	this.wss.on('connection', function (ws) {
	    /*$CALLBACK$*/
        console.log(`[SERVER] connection()`);
        gws = ws;
        ws.on('message', function (data) {
        console.log(`[SERVER] Received: ${data}`);
        const formatter = new Format();
        const msg = formatter.parse(data);
        /*$DISPATCH$*/
msg._port = 'msg_port';
switch(msg._msg) {
case'actionmsg':
var outEvent = new events.Actionmsg('msg_port', msg.agent_action);
instance._receive(outEvent);
break;
case'reset':
var outEvent = new events.Reset('msg_port', msg.y);
instance._receive(outEvent);
break;
case'done':
var outEvent = new events.Done('msg_port', msg.y);
instance._receive(outEvent);
break;
default: break;
}

        })
        
     })
    
}

/*$RECEIVERS$*/
WS.prototype.receiveobsmsgOnmsg_port = function(obsmsg) {
gws.send(this.formatter.obsmsgToFormat(obsmsg.obsAgents, obsmsg.obsLandmarks), function ack(error) {if(error) console.error("error: " + error);});
};



WS.prototype._stop = function() {
	this.wss.close();
};

module.exports = WS;
