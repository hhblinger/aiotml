function WSStringProtocol(){
WSStringProtocol.prototype.parse = function(json) {
const msg = {};
try {
const parsed = JSON.parse(json);
JSON.parse(json, function(k, v) {
switch(k) {
case 'done':
msg._msg = 'done';
msg.y = parsed.done.y;
break;
case 'reset':
msg._msg = 'reset';
msg.y = parsed.reset.y;
break;
case 'actionmsg':
msg._msg = 'actionmsg';
msg.agent_action = parsed.actionmsg.agent_action;
break;
default: break;
}
});
} catch (err) {
console.log("Cannot parse " + json + " because " + err);
};
return msg;
};

WSStringProtocol.prototype.obsmsgToFormat = function(obsAgents, obsLandmarks) {
return JSON.stringify({obsmsg: {obsAgents : obsAgents, obsLandmarks : obsLandmarks}});
};

};

module.exports = WSStringProtocol;
