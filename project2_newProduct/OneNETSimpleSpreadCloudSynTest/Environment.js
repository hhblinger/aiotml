'use strict';

const Enum = require('./enums');
const Event = require('./events');
const StateJS = require('@steelbreeze/state');
const EventEmitter = require('events').EventEmitter;
const util = require('util');


/*
 * Definition for type : Environment
 */

function Environment(name, root) {
	this.name = name;
	this.root = (root === null)? this : root;
	this.ready = false;
	this.bus = (root === null)? new EventEmitter() : this.root.bus;
	
	this.build(name);
}

Environment.prototype.build = function(session) {
	/*State machine (states and regions)*/
	/*Building root component*/
	this._statemachine = new StateJS.State('Test');
	let _initial_Environment_Test = new StateJS.PseudoState('_initial', this._statemachine, StateJS.PseudoStateKind.Initial);
	let Environment_Test_Init = new StateJS.State('Init', this._statemachine).entry(() => {
		((process.stdout && process.stdout.write) || console.log).call(process.stdout, ''+'init\n ');
		if(this.Environment_start_var === false) {
		
		}
		this.Environment_start_var = true;
	});
	let Environment_Test_set_action = new StateJS.State('set_action', this._statemachine).entry(() => {
		((process.stdout && process.stdout.write) || console.log).call(process.stdout, ''+'set_action\n ');
	});
	let Environment_Test_step = new StateJS.State('step', this._statemachine).entry(() => {
		((process.stdout && process.stdout.write) || console.log).call(process.stdout, ''+'step\n ');
		setImmediate(() => {this.bus.emit('realMsg_port!actionmsg', new Event.Actionmsg('realMsg_port', this.Environment_agentaction_var))});
	});
	let Environment_Test_stop = new StateJS.State('stop', this._statemachine).entry(() => {
		((process.stdout && process.stdout.write) || console.log).call(process.stdout, ''+'stop\n ');
	});
	_initial_Environment_Test.to(Environment_Test_Init);
	Environment_Test_set_action.to(Environment_Test_stop).on(Event.Done).when((done) => {
		return done.port === 'msg_port' && done.type === 'done';
	}).effect((done) => {
		setImmediate(() => {this.bus.emit('realMsg_port!done', new Event.Done('realMsg_port', done.y))});
	});
	Environment_Test_set_action.to(Environment_Test_step).on(Event.Actionmsg).when((actionmsg) => {
		return actionmsg.port === 'msg_port' && actionmsg.type === 'actionmsg';
	}).effect((actionmsg) => {
		this.set_action(actionmsg.agent_action);
	});
	Environment_Test_set_action.to(Environment_Test_Init).on(Event.Reset).when((reset) => {
		return reset.port === 'msg_port' && reset.type === 'reset';
	}).effect((reset) => {
		setImmediate(() => {this.bus.emit('realMsg_port!reset', new Event.Reset('realMsg_port', reset.y))});
	});
	Environment_Test_step.to(Environment_Test_set_action).on(Event.SimDevicesMsg).when((simDevicesMsg) => {
		return simDevicesMsg.port === 'realMsg_port' && simDevicesMsg.type === 'simDevicesMsg';
	}).effect((simDevicesMsg) => {
		this.synObs(simDevicesMsg.deviceAgentObs, simDevicesMsg.deviceLandmarkObs);
		setImmediate(() => {this.bus.emit('msg_port!obsmsg', new Event.Obsmsg('msg_port', this.Environment_agents_var, this.Environment_landmarks_var))});
	});
	Environment_Test_Init.to(Environment_Test_set_action).on(Event.SimDevicesMsg).when((simDevicesMsg) => {
		return simDevicesMsg.port === 'realMsg_port' && simDevicesMsg.type === 'simDevicesMsg';
	}).effect((simDevicesMsg) => {
		this.synObs(simDevicesMsg.deviceAgentObs, simDevicesMsg.deviceLandmarkObs);
		setImmediate(() => {this.bus.emit('msg_port!obsmsg', new Event.Obsmsg('msg_port', this.Environment_agents_var, this.Environment_landmarks_var))});
	});
}
Environment.prototype.Envinit = function() {
	let i_var = 0;
	while(i_var < this.Environment_agent_num_var) {
	this.Environment_agents_var[i_var].name=util.format('agent %d', i_var);;
	this.Environment_agents_var[i_var].silent=true;
	this.Environment_agents_var[i_var].pos[0] = Math.random()*2 -1;
	this.Environment_agents_var[i_var].pos[1] = Math.random()*2 -1;
	this.Environment_agents_var[i_var].goalIndex=0;
	this.Environment_agents_var[i_var].vel[0] = 0;
	this.Environment_agents_var[i_var].vel[1] = 0;
	this.Environment_agents_var[i_var].p_force[0] = 0;
	this.Environment_agents_var[i_var].p_force[1] = 0;
	this.Environment_agents_var[i_var].action_u[0] = 0;
	this.Environment_agents_var[i_var].action_u[1] = 0;
	i_var++;
	
	}
	i_var = 0;
	while(i_var < this.Environment_landmark_num_var) {
	this.Environment_landmarks_var[i_var].name=util.format('landmark %d', i_var);;
	this.Environment_landmarks_var[i_var].silent=true;
	this.Environment_landmarks_var[i_var].pos[0] = Math.random()*2 -1;
	this.Environment_landmarks_var[i_var].pos[1] = Math.random()*2 -1;
	this.Environment_landmarks_var[i_var].vel[0] = 0;
	this.Environment_landmarks_var[i_var].vel[1] = 0;
	this.Environment_landmarks_var[i_var].p_force[0] = 0;
	this.Environment_landmarks_var[i_var].p_force[1] = 0;

	i_var++;
	
	}
}

Environment.prototype.set_action = function(Environment_set_action_pAgents_var) {
	let i_var = 0;
	let agent_action_var = [];
	while(i_var < this.Environment_agent_num_var) {
	agent_action_var = Environment_set_action_pAgents_var[i_var];
	this.Environment_agents_var[i_var].action_u[0] = agent_action_var[1]
		- agent_action_var[2]
	;
	this.Environment_agents_var[i_var].action_u[1] = agent_action_var[3]
		- agent_action_var[4]
	;
	this.Environment_agents_var[i_var].action_u[0] = this.Environment_agents_var[i_var].action_u[0] * this.Environment_agents_var[i_var].sensitivity;
	this.Environment_agents_var[i_var].action_u[1] = this.Environment_agents_var[i_var].action_u[1] * this.Environment_agents_var[i_var].sensitivity;
	if(this.Environment_agents_var[i_var].movable === true) {
	this.Environment_agents_var[i_var].p_force[0] = this.Environment_agents_var[i_var].action_u[0];
	this.Environment_agents_var[i_var].p_force[1] = this.Environment_agents_var[i_var].action_u[1];
	this.Environment_agentaction_var[i_var].force[0] = this.Environment_agents_var[i_var].action_u[0];
	this.Environment_agentaction_var[i_var].force[1] = this.Environment_agents_var[i_var].action_u[1];
	}
	i_var++;
	
	}
	i_var = 0;
	while(i_var < this.Environment_landmark_num_var) {
	this.Environment_landmarks_var[i_var].p_force[0] = 0;
	this.Environment_landmarks_var[i_var].p_force[1] = 0;
	this.Environment_agentaction_var[i_var+3].force[0] = 0;
	this.Environment_agentaction_var[i_var+3].force[1] = 0;
	i_var++;
	
	}
}

Environment.prototype.step = function() {
	this.physical_apply_env_force();
	this.physical_integrate_state();
}

Environment.prototype.synObs = function(Environment_synObs_pOAgents_var, Environment_synObs_pOLandmarks_var) {
	let i_var = 0;
	let obsAgents_var = Environment_synObs_pOAgents_var;
	let obsLandmarks_var = Environment_synObs_pOLandmarks_var;
	while(i_var < this.Environment_agent_num_var) {
	this.Environment_agents_var[i_var].pos[0] = obsAgents_var[i_var].pos[0];
	this.Environment_agents_var[i_var].pos[1] = obsAgents_var[i_var].pos[1];
	this.Environment_agents_var[i_var].vel[0] = obsAgents_var[i_var].vel[0];
	this.Environment_agents_var[i_var].vel[1] = obsAgents_var[i_var].vel[1];
	i_var++;
	
	}
	i_var = 0;
	while(i_var < this.Environment_landmark_num_var) {
	this.Environment_landmarks_var[i_var].pos[0] = obsLandmarks_var[i_var].pos[0];
	this.Environment_landmarks_var[i_var].pos[1] = obsLandmarks_var[i_var].pos[1];
	this.Environment_landmarks_var[i_var].vel[0] = obsLandmarks_var[i_var].vel[0];
	this.Environment_landmarks_var[i_var].vel[1] = obsLandmarks_var[i_var].vel[1];
	i_var++;
	
	}
}

Environment.prototype.physical_apply_env_force = function() {
	let flag_var = false;
	let i_var = 0;
	let j_var = 0;
	let delta_pos_var = [];
	let dist_var;
	let dist_min_var;
	let force_i_var = [];
	let force_j_var = [];
	while(i_var < this.Environment_entity_num_var) {
	j_var = 0;
	while(j_var < this.Environment_entity_num_var) {
	if(j_var <= i_var) {
	j_var++;
	flag_var = false;
	
	} else {
	if(i_var < this.Environment_agent_num_var && j_var < this.Environment_agent_num_var) {
	delta_pos_var[0] = this.Environment_agents_var[i_var].pos[0] - this.Environment_agents_var[j_var].pos[0];
	delta_pos_var[1] = this.Environment_agents_var[i_var].pos[1] - this.Environment_agents_var[j_var].pos[1];
	dist_min_var = this.Environment_agents_var[i_var].size + this.Environment_agents_var[j_var].size;
	if(this.Environment_agents_var[i_var].collide === false || this.Environment_agents_var[j_var].collide === false) {
	flag_var = true;
	
	}
	
	} else {
	if(i_var >= this.Environment_agent_num_var && j_var < this.Environment_agent_num_var) {
	delta_pos_var[0] = this.Environment_landmarks_var[i_var - this.Environment_agent_num_var].pos[0] - this.Environment_agents_var[j_var].pos[0];
	delta_pos_var[1] = this.Environment_landmarks_var[i_var - this.Environment_agent_num_var].pos[1] - this.Environment_agents_var[j_var].pos[1];
	dist_min_var = this.Environment_landmarks_var[i_var - this.Environment_agent_num_var].size + this.Environment_agents_var[j_var].size;
	if(this.Environment_landmarks_var[i_var - this.Environment_agent_num_var].collide === false || this.Environment_agents_var[j_var].collide === false) {
	flag_var = true;
	
	}
	
	} else {
	if(i_var < this.Environment_agent_num_var && j_var >= this.Environment_agent_num_var) {
	delta_pos_var[0] = this.Environment_agents_var[i_var].pos[0] - this.Environment_landmarks_var[j_var - this.Environment_agent_num_var].pos[0];
	delta_pos_var[1] = this.Environment_agents_var[i_var].pos[1] - this.Environment_landmarks_var[j_var - this.Environment_agent_num_var].pos[1];
	dist_min_var = this.Environment_agents_var[i_var].size + this.Environment_landmarks_var[j_var - this.Environment_agent_num_var].size;
	if(this.Environment_agents_var[i_var].collide === false || this.Environment_landmarks_var[j_var - this.Environment_agent_num_var].collide === false) {
	flag_var = true;
	
	}
	
	} else {
	delta_pos_var[0] = this.Environment_landmarks_var[i_var - this.Environment_agent_num_var].pos[0] - this.Environment_landmarks_var[j_var - this.Environment_agent_num_var].pos[0];
	delta_pos_var[1] = this.Environment_landmarks_var[i_var - this.Environment_agent_num_var].pos[1] - this.Environment_landmarks_var[j_var - this.Environment_agent_num_var].pos[1];
	dist_min_var = this.Environment_landmarks_var[i_var - this.Environment_agent_num_var].size + this.Environment_landmarks_var[j_var - this.Environment_agent_num_var].size;
	if(this.Environment_landmarks_var[i_var - this.Environment_agent_num_var].collide === false || this.Environment_landmarks_var[j_var - this.Environment_agent_num_var].collide === false) {
	flag_var = true;
	
	}
	
	}
	
	}
	
	}
	dist_var = delta_pos_var[0]
		* delta_pos_var[0]
		+ delta_pos_var[1]
		* delta_pos_var[1]
	;
	dist_var = Math.sqrt(dist_var);;
	let penetration_var;
	penetration_var = Math.log(Math.exp(-(dist_var - dist_min_var)/this.Environment_contact_margin_var) + 1) * this.Environment_contact_margin_var;;
	force_i_var[0] = this.Environment_contact_force_var * delta_pos_var[0]
		/ dist_var * penetration_var;
	force_i_var[1] = this.Environment_contact_force_var * delta_pos_var[1]
		/ dist_var * penetration_var;
	force_j_var[0] = 0 - force_i_var[0]
	;
	force_j_var[1] = 0 - force_i_var[1]
	;
	if(flag_var === true) {
	force_i_var[0] = 0;
	force_i_var[1] = 0;
	force_j_var[0] = 0;
	force_j_var[1] = 0;
	
	}
	if(i_var < this.Environment_agent_num_var && j_var < this.Environment_agent_num_var) {
	this.Environment_agents_var[i_var].p_force[0] = force_i_var[0]
		+ this.Environment_agents_var[i_var].p_force[0];
	this.Environment_agents_var[i_var].p_force[1] = force_i_var[1]
		+ this.Environment_agents_var[i_var].p_force[1];
	this.Environment_agents_var[j_var].p_force[0] = force_j_var[0]
		+ this.Environment_agents_var[j_var].p_force[0];
	this.Environment_agents_var[j_var].p_force[1] = force_j_var[1]
		+ this.Environment_agents_var[j_var].p_force[1];
	
	} else {
	if(i_var >= this.Environment_agent_num_var && j_var < this.Environment_agent_num_var) {
	this.Environment_landmarks_var[i_var - this.Environment_agent_num_var].p_force[0] = force_i_var[0]
		+ this.Environment_landmarks_var[i_var - this.Environment_agent_num_var].p_force[0];
	this.Environment_landmarks_var[i_var - this.Environment_agent_num_var].p_force[1] = force_i_var[1]
		+ this.Environment_landmarks_var[i_var - this.Environment_agent_num_var].p_force[1];
	this.Environment_agents_var[j_var].p_force[0] = force_j_var[0]
		+ this.Environment_agents_var[j_var].p_force[0];
	this.Environment_agents_var[j_var].p_force[1] = force_j_var[1]
		+ this.Environment_agents_var[j_var].p_force[1];
	
	} else {
	if(i_var < this.Environment_agent_num_var && j_var >= this.Environment_agent_num_var) {
	this.Environment_agents_var[i_var].p_force[0] = force_i_var[0]
		+ this.Environment_agents_var[i_var].p_force[0];
	this.Environment_agents_var[i_var].p_force[1] = force_i_var[1]
		+ this.Environment_agents_var[i_var].p_force[1];
	this.Environment_landmarks_var[j_var - this.Environment_agent_num_var].p_force[0] = force_j_var[0]
		+ this.Environment_landmarks_var[j_var - this.Environment_agent_num_var].p_force[0];
	this.Environment_landmarks_var[j_var - this.Environment_agent_num_var].p_force[1] = force_j_var[1]
		+ this.Environment_landmarks_var[j_var - this.Environment_agent_num_var].p_force[1];
	
	} else {
	this.Environment_landmarks_var[i_var - this.Environment_agent_num_var].p_force[0] = force_i_var[0]
		+ this.Environment_landmarks_var[i_var - this.Environment_agent_num_var].p_force[0];
	this.Environment_landmarks_var[i_var - this.Environment_agent_num_var].p_force[1] = force_i_var[1]
		+ this.Environment_landmarks_var[i_var - this.Environment_agent_num_var].p_force[1];
	this.Environment_landmarks_var[j_var - this.Environment_agent_num_var].p_force[0] = force_j_var[0]
		+ this.Environment_landmarks_var[j_var - this.Environment_agent_num_var].p_force[0];
	this.Environment_landmarks_var[j_var - this.Environment_agent_num_var].p_force[1] = force_j_var[1]
		+ this.Environment_landmarks_var[j_var - this.Environment_agent_num_var].p_force[1];
	
	}
	
	}
	
	}
	j_var++;
	
	}
	
	}
	i_var++;
	
	}
}

Environment.prototype.physical_integrate_state = function() {
	let i_var = 0;
	while(i_var < this.Environment_agent_num_var) {
	this.Environment_agents_var[i_var].vel[0] = this.Environment_agents_var[i_var].vel[0] * (1 - this.Environment_damping_var);
	this.Environment_agents_var[i_var].vel[1] = this.Environment_agents_var[i_var].vel[1] * (1 - this.Environment_damping_var);
	this.Environment_agents_var[i_var].vel[0] = this.Environment_agents_var[i_var].vel[0] + this.Environment_agents_var[i_var].p_force[0] / this.Environment_agents_var[i_var].mass * this.Environment_dt_var;
	this.Environment_agents_var[i_var].vel[1] = this.Environment_agents_var[i_var].vel[1] + this.Environment_agents_var[i_var].p_force[1] / this.Environment_agents_var[i_var].mass * this.Environment_dt_var;
	this.Environment_agents_var[i_var].pos[0] = this.Environment_agents_var[i_var].pos[0] + this.Environment_agents_var[i_var].vel[0] * this.Environment_dt_var;
	this.Environment_agents_var[i_var].pos[1] = this.Environment_agents_var[i_var].pos[1] + this.Environment_agents_var[i_var].vel[1] * this.Environment_dt_var;
	i_var++;
	
	}
	i_var = 0;
	while(i_var < this.Environment_landmark_num_var) {
	this.Environment_landmarks_var[i_var].vel[0] = this.Environment_landmarks_var[i_var].vel[0] * (1 - this.Environment_damping_var);
	this.Environment_landmarks_var[i_var].vel[1] = this.Environment_landmarks_var[i_var].vel[1] * (1 - this.Environment_damping_var);
	this.Environment_landmarks_var[i_var].vel[0] = this.Environment_landmarks_var[i_var].vel[0] + this.Environment_landmarks_var[i_var].p_force[0] / this.Environment_landmarks_var[i_var].mass * this.Environment_dt_var;
	this.Environment_landmarks_var[i_var].vel[1] = this.Environment_landmarks_var[i_var].vel[1] + this.Environment_landmarks_var[i_var].p_force[1] / this.Environment_landmarks_var[i_var].mass * this.Environment_dt_var;
	this.Environment_landmarks_var[i_var].pos[0] = this.Environment_landmarks_var[i_var].pos[0] + this.Environment_landmarks_var[i_var].vel[0] * this.Environment_dt_var;
	this.Environment_landmarks_var[i_var].pos[1] = this.Environment_landmarks_var[i_var].pos[1] + this.Environment_landmarks_var[i_var].vel[1] * this.Environment_dt_var;
	i_var++;
	
	}
}

Environment.prototype._stop = function() {
	this.root = null;
	this.ready = false;
}

Environment.prototype._delete = function() {
	this._statemachine = null;
	this._Test_instance = null;
	this.bus.removeAllListeners();
}

Environment.prototype._init = function() {
	this._Test_instance = new StateJS.Instance("Test_instance", this._statemachine);
	this.ready = true;
}

Environment.prototype._receive = function(msg) {
	if (this.ready) {
		this._Test_instance.evaluate(msg);
	} else {
		setTimeout(()=>this._receive(msg),0);
	}
}

Environment.prototype.toString = function() {
	let result = 'instance ' + this.name + ':' + this.constructor.name + '\n';
	result += '\n\tstart = ' + this.Environment_start_var;
	result += '\n\tdamping = ' + this.Environment_damping_var;
	result += '\n\tsim = ' + this.Environment_sim_var;
	result += '\n\tagent_num = ' + this.Environment_agent_num_var;
	result += '\n\tlandmark_num = ' + this.Environment_landmark_num_var;
	result += '\n\tentity_num = ' + this.Environment_entity_num_var;
	result += '\n\tcontact_margin = ' + this.Environment_contact_margin_var;
	result += '\n\tcontact_force = ' + this.Environment_contact_force_var;
	result += '\n\tdt = ' + this.Environment_dt_var;
	result += '';
	return result;
}

module.exports = Environment;
