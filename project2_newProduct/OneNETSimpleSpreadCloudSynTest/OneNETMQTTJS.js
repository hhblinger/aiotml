const request = require("request");
const fs = require('fs');
const async = require('async');
const util = require('util')
const UUID = require('uuid');
const mqtt_lib = require('mqtt');
const Format = require('./OneNETStringProtocol');
const events = require('./events')

function sortId(a,b){  
  return a.index-b.index
}

function MQTT(name, options, subtopic, pubtopic, instance, deviceInfo, receiveObs) {
    this.name = name;
    this.ready = false;
    this.subtopic = subtopic;
    this.pubtopic = pubtopic;
    this.formatter = new Format();
    this.deviceInfo = deviceInfo;
    this.deviceList = [];
    this.receiveObs = receiveObs;
    var singleAgentObsMsgList = [];
    var singleLandmarkObsMsgList = [];
    this.client = mqtt_lib.connect(options);
    var client = this.client;
    var formatter = this.formatter
    this.client.on('connect', () => {
        subtopic.forEach(topic => {
            client.subscribe(topic);
    
        });
    });
    //
     this.client.on('message', (topic, payload,packet) => {
         if (subtopic.includes(topic)) {
           console.log(payload.toString());
         const msg = formatter.parse(payload);
         	if(receiveObs.includes(topic)){   
         		/*$revice_obs$*/
switch(topic) {
case'singleAgentObsMsg':
singleAgentObsMsgList.push(msg.AgentObs[0]);
break;
case'singleLandmarkObsMsg':
singleLandmarkObsMsgList.push(msg.LandmarkObs[0]);
break;
default: break;
}
if(singleAgentObsMsgList.length === 3&&singleLandmarkObsMsgList.length === 3){
msg._msg = 'simDevicesMsg';
msg.deviceAgentObs = singleAgentObsMsgList.sort(sortId);
msg.deviceLandmarkObs = singleLandmarkObsMsgList.sort(sortId);
singleAgentObsMsgList = [];
singleLandmarkObsMsgList = [];

         	}

          /*$DISPATCH$*/
msg._port = 'realMsg_port';
switch(msg._msg) {
case'simDevicesMsg':
var outEvent = new events.SimDevicesMsg('realMsg_port', msg.deviceAgentObs, msg.deviceLandmarkObs);
instance._receive(outEvent);
break;
default: break;
}

          
         }
        }
     });
  }
  
  /*$RECEIVERS$*/
MQTT.prototype.receiveactionmsgOnrealMsg_port = function(actionmsg) {
var deviceList = this.deviceList;
var i_var = 0;
var formatcmd = this.formatter.actionmsgToFormat;
actionmsg.agent_action.forEach(element=> {
var sendData = formatcmd(element);
var deviceid = deviceList[i_var];i_var++;
var url = 'http://api.heclouds.com/cmds?device_id='+deviceid;
var options = {
            body:sendData,
            headers: {
'api-key':'Fl=NCZdBophMSaaNCUqjlKAtsag='
}
};
request.post(url, options, function(err, response, body){
            console.log('send cmd response');
            console.info(response.body);
        });
});
};
MQTT.prototype.receiveresetOnrealMsg_port = function(reset) {
this.client.publish('reset', this.formatter.resetToFormat(reset.y), {qos: 0, retain: true}, function(error, packet) {
    if(error != null){
      console.log("publish error:",error);
      console.log("packet:", packet);
    }
  });
};

MQTT.prototype.receivedoneOnrealMsg_port = function(done) {
this.client.publish('done', this.formatter.doneToFormat(done.y), {qos: 0, retain: true}, function(error, packet) {
    if(error != null){
      console.log("publish error:",error);
      console.log("packet:", packet);
    }
  });
};

  
  MQTT.prototype._stop = function() {
      this.client.end();
  };
  
//===============================
function OneNET(name, options, subtopic, pubtopic, instance){

	  this.deviceType = ['agent', 'landmark'];
    this.deviceNum = {'agent':3, 'landmark':3};
    this.devicesInfo = {};
    this.deviceList = [];
    this.receiveObs = ['singleAgentObsMsg', 'singleLandmarkObsMsg'];
    this.mqtt = new MQTT(name, options, subtopic, pubtopic, instance, this.devicesInfo, this.receiveObs);

}

OneNET.prototype.create_devices = function(deviceNum, seriesCallback){
  var reserve= this;
  var device_array = [];

  this.deviceType.forEach(e=>{
    for(var i = 0; i < deviceNum[e]; i++){
      var name = util.format(e+'_%d', i);
      device_array.push(name);
      reserve.devicesInfo[e] = [];
    }
  });

  async.map(device_array,
    function(data, callback) {
        var url='http://api.heclouds.com/register_de';
    var payload = {'sn': 'urn:uuid:'+UUID.v1().toString(), 'title': data};
    var options = {
        body:JSON.stringify(payload),
        headers: {
            "api-key": 'Fl=NCZdBophMSaaNCUqjlKAtsag=',
        },
        qs:{
            'register_code': '7vX3mSOVMk0UB45j'
        }
    };
    
    request.post(url, options, function(err, response, body){
        // console.info(response.body);
        var res = JSON.parse(response.body);
        if(res.error == 1)
            console.log('register failed');
        else{
            console.log('register succeed');
            device = {'device_type':data.split('_')[0],'index':data.split('_')[1]>> 0,"device_name":data,"device_id":res['data']['device_id'],"key":res['data']['key']};
            // reserve.devices.push(device);
            callback(null, device);
        }

    });
        
    },
    function(err, results) {
        // reserve.devices = results;
        results.forEach(e=>{
            
            reserve.devicesInfo[e['device_type']].push(e);

        });
        seriesCallback(null,null,1);
    }
  );
}

OneNET.prototype.getOneNETdevices = function(){
  var reserve = this;
  try{
    fs.accessSync('onenet_devices.conf', fs.constants.R_OK);
    var deviceInfo = JSON.parse(fs.readFileSync('onenet_devices.conf'));
    reserve.devicesInfo = deviceInfo;
    reserve.mqtt.deviceInfo = deviceInfo;

  }catch (err) {
    console.error('create device error');
    async.series(
      [
        function(callback){
          reserve.create_devices(reserve.deviceNum, callback);
        },  
        function(callback){
        // console.log(reserve.devicesInfo['agent']);
        reserve.mqtt.deviceInfo = reserve.devicesInfo;
          fs.writeFileSync('onenet_devices.conf',JSON.stringify(reserve.devicesInfo));
          callback(null,null,2);  
        }
      ]
    );
  }
  this.deviceType.forEach(e=>{
    for(var i = 0; i < reserve.deviceNum[e]; i++){
      reserve.deviceList.push(reserve.devicesInfo[e][i]['device_id']);
      reserve.mqtt.deviceList = reserve.deviceList;
    }
  });


}


module.exports = OneNET;