function OneNETStringProtocol(){
OneNETStringProtocol.prototype.parse = function(json) {
const msg = {};
try {
const parsed = JSON.parse(json);
JSON.parse(json, function(k, v) {
switch(k) {
case 'singleAgentObsMsg':
msg._msg = 'singleAgentObsMsg';
msg.AgentObs = parsed.singleAgentObsMsg.AgentObs;
break;
case 'simDevicesMsg':
msg._msg = 'simDevicesMsg';
msg.deviceAgentObs = parsed.simDevicesMsg.deviceAgentObs;
msg.deviceLandmarkObs = parsed.simDevicesMsg.deviceLandmarkObs;
break;
case 'singleLandmarkObsMsg':
msg._msg = 'singleLandmarkObsMsg';
msg.LandmarkObs = parsed.singleLandmarkObsMsg.LandmarkObs;
break;
default: break;
}
});
} catch (err) {
console.log("Cannot parse " + json + " because " + err);
};
return msg;
};

OneNETStringProtocol.prototype.doneToFormat = function(y) {
return JSON.stringify({done: {y : y}});
};

OneNETStringProtocol.prototype.actionmsgToFormat = function(agent_action) {
return JSON.stringify({actionCmdMsg: {agent_action : agent_action}});
};

OneNETStringProtocol.prototype.resetToFormat = function(y) {
return JSON.stringify({reset: {y : y}});
};

};

module.exports = OneNETStringProtocol;
