'use strict';

const Enum = require('./enums');
const Event = require('./events');
const StateJS = require('@steelbreeze/state');
const EventEmitter = require('events').EventEmitter;
const util = require('util');


/*
 * Definition for type : LandmarkDevice
 */

function LandmarkDevice(name, root) {
	this.name = name;
	this.root = (root === null)? this : root;
	this.ready = false;
	this.bus = (root === null)? new EventEmitter() : this.root.bus;
	
	this.build(name);
}

LandmarkDevice.prototype.build = function(session) {
	/*State machine (states and regions)*/
	/*Building root component*/
	this._statemachine = new StateJS.State('Test');
	let _initial_LandmarkDevice_Test = new StateJS.PseudoState('_initial', this._statemachine, StateJS.PseudoStateKind.Initial);
	let LandmarkDevice_Test_Init = new StateJS.State('Init', this._statemachine).entry(() => {
		((process.stdout && process.stdout.write) || console.log).call(process.stdout, ''+'init\n ');
		this.Envinit();
		setImmediate(() => {this.bus.emit('realMsg_port!singleLandmarkObsMsg', new Event.SingleLandmarkObsMsg('realMsg_port', this.LandmarkDevice_device_obsLandmark_var))});
	});
	let LandmarkDevice_Test_set_action = new StateJS.State('set_action', this._statemachine).entry(() => {
		((process.stdout && process.stdout.write) || console.log).call(process.stdout, ''+'set_action\n ');
	});
	let LandmarkDevice_Test_step = new StateJS.State('step', this._statemachine).entry(() => {
		((process.stdout && process.stdout.write) || console.log).call(process.stdout, ''+'step\n ');
		this.step();
	});
	let LandmarkDevice_Test_stop = new StateJS.State('stop', this._statemachine).entry(() => {
		((process.stdout && process.stdout.write) || console.log).call(process.stdout, ''+'stop\n ');
	});
	_initial_LandmarkDevice_Test.to(LandmarkDevice_Test_Init);
	LandmarkDevice_Test_Init.to(LandmarkDevice_Test_set_action);
	LandmarkDevice_Test_step.to(LandmarkDevice_Test_set_action).effect(() => {
		setImmediate(() => {this.bus.emit('realMsg_port!singleLandmarkObsMsg', new Event.SingleLandmarkObsMsg('realMsg_port', this.LandmarkDevice_device_obsLandmark_var))});
	});
	LandmarkDevice_Test_set_action.to(LandmarkDevice_Test_step).on(Event.ActionCmdMsg).when((actionCmdMsg) => {
		return actionCmdMsg.port === 'realMsg_port' && actionCmdMsg.type === 'actionCmdMsg';
	}).effect((actionCmdMsg) => {
		this.set_action(actionCmdMsg.agent_action);
	});
	LandmarkDevice_Test_set_action.to(LandmarkDevice_Test_Init).on(Event.Reset).when((reset) => {
		return reset.port === 'realMsg_port' && reset.type === 'reset';
	});
	LandmarkDevice_Test_set_action.to(LandmarkDevice_Test_stop).on(Event.Done).when((done) => {
		return done.port === 'realMsg_port' && done.type === 'done';
	});
}
LandmarkDevice.prototype.Envinit = function() {
	this.LandmarkDevice_landmarks_var.name=util.format('agent %d', 0);;
	this.LandmarkDevice_landmarks_var.silent=true;
	this.LandmarkDevice_landmarks_var.pos[0] = 0.8
	this.LandmarkDevice_landmarks_var.pos[1] = 0.8
	this.LandmarkDevice_landmarks_var.vel[0] = 0;
	this.LandmarkDevice_landmarks_var.vel[1] = 0;
	this.LandmarkDevice_landmarks_var.p_force[0] = 0;
	this.LandmarkDevice_landmarks_var.p_force[1] = 0;
	this.LandmarkDevice_device_obsLandmark_var[0].index=1;
	this.LandmarkDevice_device_obsLandmark_var[0].vel[0] = this.LandmarkDevice_landmarks_var.vel[0];
	this.LandmarkDevice_device_obsLandmark_var[0].vel[1] = this.LandmarkDevice_landmarks_var.vel[1];
	this.LandmarkDevice_device_obsLandmark_var[0].pos[0] = this.LandmarkDevice_landmarks_var.pos[0];
	this.LandmarkDevice_device_obsLandmark_var[0].pos[1] = this.LandmarkDevice_landmarks_var.pos[1];
}

LandmarkDevice.prototype.set_action = function(LandmarkDevice_set_action_pAgents_var) {
	if(this.LandmarkDevice_landmarks_var.movable === true) {
	this.LandmarkDevice_landmarks_var.p_force[0] = LandmarkDevice_set_action_pAgents_var.force[0];
	this.LandmarkDevice_landmarks_var.p_force[1] = LandmarkDevice_set_action_pAgents_var.force[1];
	
	}
}

LandmarkDevice.prototype.step = function() {
	this.apply_action();
	this.update_state();
}

LandmarkDevice.prototype.apply_action = function() {
	
}

LandmarkDevice.prototype.update_state = function() {
	
}

LandmarkDevice.prototype._stop = function() {
	this.root = null;
	this.ready = false;
}

LandmarkDevice.prototype._delete = function() {
	this._statemachine = null;
	this._Test_instance = null;
	this.bus.removeAllListeners();
}

LandmarkDevice.prototype._init = function() {
	this._Test_instance = new StateJS.Instance("Test_instance", this._statemachine);
	this.ready = true;
}

LandmarkDevice.prototype._receive = function(msg) {
	if (this.ready) {
		this._Test_instance.evaluate(msg);
	} else {
		setTimeout(()=>this._receive(msg),0);
	}
}

LandmarkDevice.prototype.toString = function() {
	let result = 'instance ' + this.name + ':' + this.constructor.name + '\n';
	result += '\n\tstart = ' + this.LandmarkDevice_start_var;
	result += '\n\tdamping = ' + this.LandmarkDevice_damping_var;
	result += '\n\tdt = ' + this.LandmarkDevice_dt_var;
	result += '';
	return result;
}

module.exports = LandmarkDevice;
