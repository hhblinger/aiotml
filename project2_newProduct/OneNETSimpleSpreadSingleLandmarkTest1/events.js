var DeviceLandmarkMsg = /** @class */ (function () {
  function DeviceLandmarkMsg(port,...params) {
    this.type = 'deviceLandmarkMsg';
    this.port = port;
    this.deviceLandmarkObs = params[0];
  }

  DeviceLandmarkMsg.prototype.is = function (type) {
    return this.type === type;
  };

  DeviceLandmarkMsg.prototype.toString = function () {
    return 'event ' + this.type + '?' + this.port + '(' + deviceLandmarkObs + ')';
  };

  return DeviceLandmarkMsg;
}());
exports.DeviceLandmarkMsg = DeviceLandmarkMsg;

var ActionCmdMsg = /** @class */ (function () {
  function ActionCmdMsg(port,...params) {
    this.type = 'actionCmdMsg';
    this.port = port;
    this.agent_action = params[0];
  }

  ActionCmdMsg.prototype.is = function (type) {
    return this.type === type;
  };

  ActionCmdMsg.prototype.toString = function () {
    return 'event ' + this.type + '?' + this.port + '(' + agent_action + ')';
  };

  return ActionCmdMsg;
}());
exports.ActionCmdMsg = ActionCmdMsg;

var SingleAgentObsMsg = /** @class */ (function () {
  function SingleAgentObsMsg(port,...params) {
    this.type = 'singleAgentObsMsg';
    this.port = port;
    this.AgentObs = params[0];
  }

  SingleAgentObsMsg.prototype.is = function (type) {
    return this.type === type;
  };

  SingleAgentObsMsg.prototype.toString = function () {
    return 'event ' + this.type + '?' + this.port + '(' + AgentObs + ')';
  };

  return SingleAgentObsMsg;
}());
exports.SingleAgentObsMsg = SingleAgentObsMsg;

var Reset = /** @class */ (function () {
  function Reset(port,...params) {
    this.type = 'reset';
    this.port = port;
    this.y = params[0];
  }

  Reset.prototype.is = function (type) {
    return this.type === type;
  };

  Reset.prototype.toString = function () {
    return 'event ' + this.type + '?' + this.port + '(' + y + ')';
  };

  return Reset;
}());
exports.Reset = Reset;

var ObsLandmarkmsg = /** @class */ (function () {
  function ObsLandmarkmsg(port,...params) {
    this.type = 'obsLandmarkmsg';
    this.port = port;
    this.o = params[0];
  }

  ObsLandmarkmsg.prototype.is = function (type) {
    return this.type === type;
  };

  ObsLandmarkmsg.prototype.toString = function () {
    return 'event ' + this.type + '?' + this.port + '(' + o + ')';
  };

  return ObsLandmarkmsg;
}());
exports.ObsLandmarkmsg = ObsLandmarkmsg;

var RealObsmsg = /** @class */ (function () {
  function RealObsmsg(port,...params) {
    this.type = 'realObsmsg';
    this.port = port;
    this.obsAgents = params[0];
    this.obsLandmarks = params[1];
  }

  RealObsmsg.prototype.is = function (type) {
    return this.type === type;
  };

  RealObsmsg.prototype.toString = function () {
    return 'event ' + this.type + '?' + this.port + '(' + obsAgents + ', ' + obsLandmarks + ')';
  };

  return RealObsmsg;
}());
exports.RealObsmsg = RealObsmsg;

var ObsAgentmsg = /** @class */ (function () {
  function ObsAgentmsg(port,...params) {
    this.type = 'obsAgentmsg';
    this.port = port;
    this.o = params[0];
  }

  ObsAgentmsg.prototype.is = function (type) {
    return this.type === type;
  };

  ObsAgentmsg.prototype.toString = function () {
    return 'event ' + this.type + '?' + this.port + '(' + o + ')';
  };

  return ObsAgentmsg;
}());
exports.ObsAgentmsg = ObsAgentmsg;

var Obsmsg = /** @class */ (function () {
  function Obsmsg(port,...params) {
    this.type = 'obsmsg';
    this.port = port;
    this.obsAgents = params[0];
    this.obsLandmarks = params[1];
  }

  Obsmsg.prototype.is = function (type) {
    return this.type === type;
  };

  Obsmsg.prototype.toString = function () {
    return 'event ' + this.type + '?' + this.port + '(' + obsAgents + ', ' + obsLandmarks + ')';
  };

  return Obsmsg;
}());
exports.Obsmsg = Obsmsg;

var SimDevicesMsg = /** @class */ (function () {
  function SimDevicesMsg(port,...params) {
    this.type = 'simDevicesMsg';
    this.port = port;
    this.deviceAgentObs = params[0];
    this.deviceLandmarkObs = params[1];
  }

  SimDevicesMsg.prototype.is = function (type) {
    return this.type === type;
  };

  SimDevicesMsg.prototype.toString = function () {
    return 'event ' + this.type + '?' + this.port + '(' + deviceAgentObs + ', ' + deviceLandmarkObs + ')';
  };

  return SimDevicesMsg;
}());
exports.SimDevicesMsg = SimDevicesMsg;

var SingleLandmarkObsMsg = /** @class */ (function () {
  function SingleLandmarkObsMsg(port,...params) {
    this.type = 'singleLandmarkObsMsg';
    this.port = port;
    this.LandmarkObs = params[0];
  }

  SingleLandmarkObsMsg.prototype.is = function (type) {
    return this.type === type;
  };

  SingleLandmarkObsMsg.prototype.toString = function () {
    return 'event ' + this.type + '?' + this.port + '(' + LandmarkObs + ')';
  };

  return SingleLandmarkObsMsg;
}());
exports.SingleLandmarkObsMsg = SingleLandmarkObsMsg;

var Done = /** @class */ (function () {
  function Done(port,...params) {
    this.type = 'done';
    this.port = port;
    this.y = params[0];
  }

  Done.prototype.is = function (type) {
    return this.type === type;
  };

  Done.prototype.toString = function () {
    return 'event ' + this.type + '?' + this.port + '(' + y + ')';
  };

  return Done;
}());
exports.Done = Done;

var DeviceAgentMsg = /** @class */ (function () {
  function DeviceAgentMsg(port,...params) {
    this.type = 'deviceAgentMsg';
    this.port = port;
    this.deviceAgentObs = params[0];
  }

  DeviceAgentMsg.prototype.is = function (type) {
    return this.type === type;
  };

  DeviceAgentMsg.prototype.toString = function () {
    return 'event ' + this.type + '?' + this.port + '(' + deviceAgentObs + ')';
  };

  return DeviceAgentMsg;
}());
exports.DeviceAgentMsg = DeviceAgentMsg;

var Actionmsg = /** @class */ (function () {
  function Actionmsg(port,...params) {
    this.type = 'actionmsg';
    this.port = port;
    this.agent_action = params[0];
  }

  Actionmsg.prototype.is = function (type) {
    return this.type === type;
  };

  Actionmsg.prototype.toString = function () {
    return 'event ' + this.type + '?' + this.port + '(' + agent_action + ')';
  };

  return Actionmsg;
}());
exports.Actionmsg = Actionmsg;

