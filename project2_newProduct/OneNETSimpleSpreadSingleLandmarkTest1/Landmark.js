function Landmark(){
this.name = null;
this.index = 0;
this.pos = [];
this.vel = [];
this.movable = false;
this.silent = false;
this.u_noise = 0;
this.u_range = 1.0;
this.size = 0.1;
this.mass = 5;
this.collide = false;
this.p_force = [];
this.device_id = null;
}
exports.Landmark=Landmark;
