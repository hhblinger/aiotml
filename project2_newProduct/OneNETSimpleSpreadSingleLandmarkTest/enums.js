/*Definition of Enumeration DigitalState*/
const DigitalState_ENUM = Object.freeze({
	LOW: 0,
	HIGH: 1,
});
exports.DigitalState_ENUM = DigitalState_ENUM;

