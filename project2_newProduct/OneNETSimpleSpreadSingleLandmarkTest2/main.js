'use strict';

const Enum = require('./enums');
const LandmarkDevice = require('./LandmarkDevice');
const ObsLandmark = require('./ObsLandmark');
const AgentAction = require('./AgentAction');
const Landmark = require('./Landmark');
const MQTT = require('./MQTTJS');
/*$REQUIRE_PLUGINS$*/



const inst_device = new LandmarkDevice('device', null);
var inst_device_landmarks = new Landmark.Landmark();
inst_device.LandmarkDevice_landmarks_var = inst_device_landmarks;
inst_device.LandmarkDevice_device_obsLandmark_var = [];
for (var i = 0; i < 1; i++){
inst_device.LandmarkDevice_device_obsLandmark_var.push(new ObsLandmark.ObsLandmark());
}
inst_device.LandmarkDevice_agent_action_var = [];
for (var i = 0; i < 1; i++){
inst_device.LandmarkDevice_agent_action_var.push(new AgentAction.AgentAction());
}
inst_device.LandmarkDevice_start_var = false;
inst_device.LandmarkDevice_damping_var = 0.25;
inst_device.LandmarkDevice_dt_var = 0.1;

/*$PLUGINS$*/
var options = {
    host: '183.230.40.39',
    port: '6002',
    protocol: 'mqtt',
    clean: false,
	}
    options.clientId = '661610565'
    options.username = '394823'
    options.password = 'Fl=NCZdBophMSaaNCUqjlKAtsag='
const mqtt = new MQTT("MQTT", options, ['done','reset'], ['singleLandmarkObsMsg'], inst_device);
/*Connecting internal ports...*/
/*Connecting ports...*/
inst_device.bus.on('realMsg_port!singleLandmarkObsMsg', (singleLandmarkObsMsg) => mqtt.receivesingleLandmarkObsMsgOnrealMsg_port(singleLandmarkObsMsg));

/*$PLUGINS_CONNECTORS$*/
inst_device._init();
/*$PLUGINS_END$*/

function terminate() {
	inst_device._stop();
	inst_device._delete();
};
/*terminate all things on SIGINT (e.g. CTRL+C)*/
if (process && process.on) {
	process.on('SIGINT', function() {
		terminate();
		mqtt._stop();
/*$STOP_PLUGINS$*/

		setTimeout(() => {
			process.exit();
		}, 1000);
	});
}
