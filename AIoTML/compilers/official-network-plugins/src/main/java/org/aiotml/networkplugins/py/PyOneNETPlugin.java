/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 */
package org.aiotml.networkplugins.py;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.aiotml.compilers.Context;
import org.aiotml.compilers.spi.NetworkPlugin;
import org.aiotml.compilers.spi.SerializationPlugin;
import org.aiotml.compilers.spi.NetworkPlugin.ThingPortMessage;
import org.aiotml.xtext.helpers.AnnotatedElementHelper;
import org.aiotml.xtext.aiotML.Configuration;
import org.aiotml.xtext.aiotML.ExternalConnector;
import org.aiotml.xtext.aiotML.Message;
import org.aiotml.xtext.aiotML.Parameter;
import org.aiotml.xtext.aiotML.Port;
import org.aiotml.xtext.aiotML.Protocol;

public class PyOneNETPlugin extends NetworkPlugin {

	@Override
	public String getPluginID() {
		return "PyOneNETPlugin";
	}

	@Override
	public List<String> getSupportedProtocols() {
		List<String> res = new ArrayList<>();
        res.add("onenet");
        res.add("ONENET");
        res.add("OneNET");
        return res;
	}

	@Override
	public List<String> getTargetedLanguages() {
		 List<String> res = new ArrayList<>();
	        res.add("python");
	        return res;
	}
	
	final Set<Message> messages = new HashSet<Message>();

    private void addMessage(Message m) {
        boolean contains = false;
        for(Message msg : messages) {
            if (EcoreUtil.equals(msg, m)) {
                contains = true;
                break;
            }
        }
        if (!contains) {
            messages.add(m);
        }
    }
	@Override
	public void generateNetworkLibrary(Configuration cfg, Context ctx, Set<Protocol> protocols) {
		System.out.println("generateNetworkLibrary " + cfg.getName() + ", " + protocols.size());
		StringBuilder builder;
        for (Protocol prot : protocols) {
            SerializationPlugin sp;
            try {
               sp = ctx.getSerializationPlugin(prot);
            } catch (UnsupportedEncodingException uee) {
                System.err.println("Could not get serialization plugin... Expect some errors in the generated code");
                uee.printStackTrace();
                return;
            }

            String serializers = "";
            messages.clear();
            for (ThingPortMessage tpm : getMessagesSent(cfg, prot)) {
                addMessage(tpm.m);
            }
            for(Message m : messages) {
                StringBuilder temp = new StringBuilder();
                serializers += sp.generateSerialization(temp, prot.getName() + "StringProtocol", m);
            }

            builder = new StringBuilder();
            messages.clear();
            for (ThingPortMessage tpm : getMessagesReceived(cfg, prot)) {
                addMessage(tpm.m);
            }
            sp.generateParserBody(builder, prot.getName() + "StringProtocol", null, messages, null);
//            final String result = builder.toString().replace("#$SERIALIZERS$#", serializers);

            new OneNETProtocol(ctx, prot, cfg, builder).generate();
        }
	}
	
	private class OneNETProtocol {
        Context ctx;
        Protocol prot;
        Configuration cfg;
        StringBuilder serial;
        private List<Port> ports = new ArrayList<Port>();

        public OneNETProtocol(Context ctx, Protocol prot, Configuration cfg, StringBuilder serial) {
            this.ctx = ctx;
            this.prot = prot;
            this.cfg = cfg;
            this.serial = serial;
        }

        private void addPort(Port p) {
            boolean contains = false;
            for (Port port : ports) {
                if (EcoreUtil.equals(port, p)) {
                    contains = true;
                    break;
                }
            }
            if (!contains) {
                ports.add(p);
            }
        }

        public void generate() {
            for (ThingPortMessage tpm : getMessagesSent(cfg, prot)) {
                addPort(tpm.p);
            }
            for (ThingPortMessage tpm : getMessagesReceived(cfg, prot)) {
                addPort(tpm.p);
            }
            String template = ctx.getTemplateByID("templates/PyOneNETPlugin.py");
            try {
                final File f = new File(ctx.getOutputDirectory() + "/WSPY.py");
                final OutputStream output = new FileOutputStream(f);
                IOUtils.write(template, output, Charset.forName("UTF-8"));
                IOUtils.closeQuietly(output);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
	}

}
