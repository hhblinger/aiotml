/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 */
package org.aiotml.compilers.python;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.aiotml.compilers.Context;
import org.aiotml.compilers.thing.common.CommonThingActionCompiler;
import org.aiotml.xtext.ByteValueConverter;
import org.aiotml.xtext.CharValueConverter;
import org.aiotml.xtext.StringValueConverter;
import org.aiotml.xtext.constraints.AiotMLHelpers;
import org.aiotml.xtext.constraints.Types;
import org.aiotml.xtext.helpers.AnnotatedElementHelper;
import org.aiotml.xtext.helpers.ConfigurationHelper;
import org.aiotml.xtext.helpers.ThingHelper;
import org.aiotml.xtext.helpers.AiotMLElementHelper;
import org.aiotml.xtext.helpers.TyperHelper;
import org.aiotml.xtext.aiotML.Action;
import org.aiotml.xtext.aiotML.ActionBlock;
import org.aiotml.xtext.aiotML.AndExpression;
import org.aiotml.xtext.aiotML.Append;
import org.aiotml.xtext.aiotML.ArrayIndex;
import org.aiotml.xtext.aiotML.ArrayInit;
import org.aiotml.xtext.aiotML.BooleanLiteral;
import org.aiotml.xtext.aiotML.ByteLiteral;
import org.aiotml.xtext.aiotML.CastExpression;
import org.aiotml.xtext.aiotML.CharLiteral;
import org.aiotml.xtext.aiotML.ClassFunctionCall;
import org.aiotml.xtext.aiotML.ConditionalAction;
import org.aiotml.xtext.aiotML.ConfigPropertyAssign;
import org.aiotml.xtext.aiotML.Cproperty;
import org.aiotml.xtext.aiotML.CpropertyAssignment;
import org.aiotml.xtext.aiotML.CpropertyReference;
import org.aiotml.xtext.aiotML.Decrement;
import org.aiotml.xtext.aiotML.DictAssignment;
import org.aiotml.xtext.aiotML.DivExpression;
import org.aiotml.xtext.aiotML.DoubleLiteral;
import org.aiotml.xtext.aiotML.EnumLiteralRef;
import org.aiotml.xtext.aiotML.EqualsExpression;
import org.aiotml.xtext.aiotML.ErrorAction;
import org.aiotml.xtext.aiotML.EventReference;
import org.aiotml.xtext.aiotML.Expression;
import org.aiotml.xtext.aiotML.ExpressionGroup;
import org.aiotml.xtext.aiotML.ExternExpression;
import org.aiotml.xtext.aiotML.ExternStatement;
import org.aiotml.xtext.aiotML.ExternalConnector;
import org.aiotml.xtext.aiotML.ForAction;
import org.aiotml.xtext.aiotML.Function;
import org.aiotml.xtext.aiotML.FunctionCallExpression;
import org.aiotml.xtext.aiotML.FunctionCallStatement;
import org.aiotml.xtext.aiotML.GreaterExpression;
import org.aiotml.xtext.aiotML.GreaterOrEqualExpression;
import org.aiotml.xtext.aiotML.Increment;
import org.aiotml.xtext.aiotML.IntegerLiteral;
import org.aiotml.xtext.aiotML.LocalVariable;
import org.aiotml.xtext.aiotML.LoopAction;
import org.aiotml.xtext.aiotML.LowerExpression;
import org.aiotml.xtext.aiotML.LowerOrEqualExpression;
import org.aiotml.xtext.aiotML.MinusExpression;
import org.aiotml.xtext.aiotML.ModExpression;
import org.aiotml.xtext.aiotML.NotEqualsExpression;
import org.aiotml.xtext.aiotML.NotExpression;
import org.aiotml.xtext.aiotML.OrExpression;
import org.aiotml.xtext.aiotML.Parameter;
import org.aiotml.xtext.aiotML.PlusExpression;
import org.aiotml.xtext.aiotML.PrintAction;
import org.aiotml.xtext.aiotML.Property;
import org.aiotml.xtext.aiotML.PropertyReference;
import org.aiotml.xtext.aiotML.ReceiveMessage;
import org.aiotml.xtext.aiotML.ReturnAction;
import org.aiotml.xtext.aiotML.SendAction;
import org.aiotml.xtext.aiotML.Session;
import org.aiotml.xtext.aiotML.StartSession;
import org.aiotml.xtext.aiotML.StrategyStream;
import org.aiotml.xtext.aiotML.StringLiteral;
import org.aiotml.xtext.aiotML.SubThing;
import org.aiotml.xtext.aiotML.Thing;
import org.aiotml.xtext.aiotML.TimesExpression;
import org.aiotml.xtext.aiotML.Type;
import org.aiotml.xtext.aiotML.UnaryMinus;
import org.aiotml.xtext.aiotML.Variable;
import org.aiotml.xtext.aiotML.VariableAssignment;
import org.aiotml.xtext.aiotML.ZipEnumerate;
import org.aiotml.xtext.validation.TypeChecker;

public class PyThingActionCompiler extends CommonThingActionCompiler {


	public void generate(Action action, StringBuilder builder, Context ctx, int tabformat) {
        if (action == null)
            return;
        if (action instanceof SendAction)
            generate((SendAction) action, builder, ctx, tabformat);
        else if (action instanceof VariableAssignment)
            generate((VariableAssignment) action, builder, ctx, tabformat);
        else if (action instanceof ActionBlock)
            generate((ActionBlock) action, builder, ctx, tabformat);
        else if (action instanceof ExternStatement)
            generate((ExternStatement) action, builder, ctx, tabformat);
        else if (action instanceof ConditionalAction)
            generate((ConditionalAction) action, builder, ctx, tabformat);
        else if (action instanceof LoopAction)
            generate((LoopAction) action, builder, ctx, tabformat);
        else if (action instanceof PrintAction)
            generate((PrintAction) action, builder, ctx, tabformat);
        else if (action instanceof ErrorAction)
            generate((ErrorAction) action, builder, ctx, tabformat);
        else if (action instanceof ReturnAction)
            generate((ReturnAction) action, builder, ctx, tabformat);
        else if (action instanceof LocalVariable)
            generate((LocalVariable) action, builder, ctx, tabformat);
        else if (action instanceof FunctionCallStatement)
            generate((FunctionCallStatement) action, builder, ctx, tabformat);
        else if (action instanceof Increment)
            generate((Increment) action, builder, ctx, tabformat);
        else if (action instanceof Decrement)
            generate((Decrement) action, builder, ctx, tabformat);
        else if (action instanceof StartSession) 
            generate((StartSession) action, builder, ctx, tabformat);
        else if (action instanceof ForAction) 
            generate((ForAction) action, builder, ctx, tabformat);      
        else if (action instanceof CpropertyAssignment) 
        	generate((CpropertyAssignment) action, builder, ctx, tabformat);     
        else if (action instanceof Append) 
        	generate((Append) action, builder, ctx, tabformat);
        else if (action instanceof DictAssignment) 
        	generate((DictAssignment) action, builder, ctx, tabformat);
        else if (action instanceof ZipEnumerate)
        	generate((ZipEnumerate) action, builder, ctx, tabformat);  
        else if (action instanceof StrategyStream)
        	generate((StrategyStream) action, builder, ctx, tabformat);
        else if (action instanceof ClassFunctionCall)
        	generate((ClassFunctionCall) action, builder, ctx, tabformat);   
        else {
            throw (new UnsupportedOperationException("This action (" + action.getClass().getName() + ") is unknown... Please update your action compilers as a new action/expression might have been introduced in AiotML"));
        }
    }
	
	public void generate(StrategyStream action, StringBuilder builder, Context ctx, int tabformat) {
		action.getInputs();
		action.getOutputs();
		action.getStrategy();
		retractappend(builder, tabformat);
//		ctx.firstToUpper(s.getName())
		if(action.getOutputs().get(0) instanceof Property || action.getOutputs().get(0) instanceof Cproperty) {
			builder.append("self.");
		}
		
		builder.append(ctx.getVariableName(action.getOutputs().get(0))+" = "+"[agent.action(obs) for agent, obs in zip(self.trainers, ");
		if(action.getInputs().get(0) instanceof Property || action.getInputs().get(0) instanceof Cproperty) {
			builder.append("self.");
			builder.append(ctx.getVariableName(action.getInputs().get(0))+")]\n");
		}
		retractappend(builder, tabformat);
		builder.append("self.display(agents_pos_var, landmark_pos_var)\n");
	}
	
	public void generate(Append action, StringBuilder builder, Context ctx, int tabformat) {
		retractappend(builder, tabformat);
		if(action.getPythonlist() instanceof Property || action.getPythonlist() instanceof Cproperty )
			builder.append("self.");
		builder.append(action.getPythonlist().getName() + "_var");
		if (action.getVars() != null){
			for(Variable v : action.getVars()) {
				builder.append("[");
				if(v instanceof Property || v instanceof Cproperty )
					builder.append("self.");
				builder.append(v.getName() + "_var");
				builder.append("]");
			}
		}
		builder.append(".append(");
		generate(action.getElement(),builder,ctx);
		builder.append(")\n");
	}
	
	public void generate(DictAssignment action, StringBuilder builder, Context ctx, int tabformat) {
		retractappend(builder, tabformat);
		if(action.getDict() instanceof Property || action.getDict() instanceof Cproperty )
			builder.append("self.");
		
		builder.append(action.getDict().getName());
		int elementLen = 0;
		while(elementLen++ < action.getElements().size()) {
			builder.append("[");
			generate(action.getElements().get(elementLen),builder,ctx);
			builder.append("]");
		}
		builder.append(" = ");
		generate(action.getVar(),builder,ctx);
	}
	
	public void generate(ClassFunctionCall action, StringBuilder builder, Context ctx, int tabformat) {
		retractappend(builder, tabformat);
		if(action.getProp() instanceof Property || action.getProp() instanceof Cproperty )
			builder.append("self.");
		builder.append(action.getProp().getName() +"_var");
		EList<Variable> elements = action.getElements();
		for(Variable e:elements) {
			builder.append("[");
			builder.append(e.getName() +"_var");
			builder.append("]");
		}
		builder.append(".");
		builder.append(action.getFname());
		builder.append("(");
		
		EList<Variable> p = action.getP();
		EList<Variable> pe = action.getPe();
		int lenDiff = p.size() - pe.size();
		int count = 0;
		while(lenDiff-->0) {
			if (count>0){
				builder.append(",");
			}
			builder.append(p.get(count).getName() + "_var");
			count++;
		}
		int peCount = 0;
		while(count<p.size()) {
			if (count>0){
				builder.append(",");
			}
			builder.append(p.get(count).getName()+ "_var");
			builder.append('[');
			builder.append(pe.get(peCount).getName()+ "_var");
			builder.append(']');
			peCount++;
			count++;
		}
		builder.append(")");
		builder.append("\n");
	}
	
	public void generate(ZipEnumerate action, StringBuilder builder, Context ctx, int tabformat) {
		retractappend(builder, tabformat);
		builder.append("for ");
		int var_len = 0;
		while(var_len < action.getVars().size()) {
			if(var_len == 0) {
				builder.append(action.getVars().get(var_len).getName());
			}
			else {
				builder.append(",");
				builder.append(action.getVars().get(var_len).getName());
			}
			var_len ++;
		}
		builder.append(" in ");
		builder.append(action.getFname());
		builder.append("(");
		int sets_len = 0;
		while(sets_len < action.getSets().size()) {
			if(sets_len == 0) {
				builder.append(action.getSets().get(sets_len).getName());
			}
			else {
				builder.append(",");
				builder.append(action.getSets().get(sets_len).getName());
			}
			sets_len ++;
		}
		builder.append(")");
		
		builder.append(" : \n");

        generate(action.getAction(), builder, ctx,tabformat);
	}
	
	public void retractappend(StringBuilder builder ,int b) {
		while(b-- > 0) {
			builder.append("	");
		}
	}

	public void generate(VariableAssignment action, StringBuilder builder, Context ctx, int tabformat) {
		retractappend(builder, tabformat);
		if(action.getProperty().getTypeRef()!= null) {
			if (action.getProperty().getTypeRef().getCardinality() != null && action.getIndex() != null) {
			// this is an array (and we want to affect just one index)
				if (action.getProperty() instanceof Property) {
					builder.append("self.");
				}

				builder.append(ctx.getVariableName(action.getProperty()));
				StringBuilder tempBuilder = new StringBuilder();
				generate(action.getIndex(), tempBuilder, ctx);
				builder.append("[" + tempBuilder.toString() + "]");
				
				if(action.getExtraProperty().size()==0) {
					builder.append(" = ");
					cast(action.getProperty().getTypeRef().getType(), false, action.getExpression(), builder, ctx);
					builder.append("\n");
				}
			}else {
			// simple variable or we re-affect the whole array
			if (action.getProperty() instanceof Property) {
				builder.append("self.");
			}

			builder.append(ctx.getVariableName(action.getProperty()));
			// 判断变量是否是数组或者字典，如是，可能会对数组或者字典的元素赋值
			if(action.getIndex()!=null) {
				builder.append("[");
				cast(action.getProperty().getTypeRef().getType(), action.getProperty().getTypeRef().isIsArray(),
						action.getIndex(), builder, ctx);
				builder.append("]");
			}
			if(action.getExtraProperty().size()==0) {
				builder.append(" = ");
				if (action.getExpression() != null) {
					cast(action.getProperty().getTypeRef().getType(), action.getProperty().getTypeRef().isIsArray(),
							action.getExpression(), builder, ctx);
				}else {
					generate(action.getCf(), builder,ctx, 0);
				}
				builder.append("\n");
			}
			}
		}else {
			if (action.getProperty() instanceof Property) {
				builder.append("self.");
			}

			builder.append(ctx.getVariableName(action.getProperty()));
			if(action.getIndex()!=null) {
				builder.append("[");
				generate(action.getIndex(), builder, ctx);
				builder.append("]");
			}
			if(action.getExtraProperty().size()==0) {
				builder.append(" = ");
				generate(action.getExpression(), builder, ctx);
				builder.append("\n");
			}
		}
		
		if(action.getExtraProperty().size()!=0) {
			int i = 0;
			int ii = 0;
			while(i<action.getExtraProperty().size()) {
				builder.append(",");
				if(action.getExtraProperty().get(i).getTypeRef()!= null) {
					if (action.getExtraProperty().get(i).getTypeRef().getCardinality() != null && action.getExtraIndex().size()!=0) {
					// this is an array (and we want to affect just one index)
						if (action.getExtraProperty().get(i) instanceof Property) {
							builder.append("self.");
						}

						builder.append(ctx.getVariableName(action.getExtraProperty().get(i)));
						if(ii<action.getExtraIndex().size()) {
							StringBuilder tempBuilder = new StringBuilder();
							generate(action.getExtraIndex().get(ii), tempBuilder, ctx);
							ii++;
							builder.append("[" + tempBuilder.toString() + "]");
						}
						builder.append(" = ");
						cast(action.getExtraProperty().get(i).getTypeRef().getType(), false, action.getExpression(), builder, ctx);
						builder.append("\n");
					}else {
					// simple variable or we re-affect the whole array
					if (action.getExtraProperty().get(i) instanceof Property) {
						builder.append("self.");
					}

					builder.append(ctx.getVariableName(action.getExtraProperty().get(i)));
					// 判断变量是否是数组或者字典，如是，可能会对数组或者字典的元素赋值
					if(action.getExtraIndex().size()!=0 && ii<action.getExtraIndex().size()) {
						builder.append("[");
						cast(action.getExtraProperty().get(i).getTypeRef().getType(), action.getExtraProperty().get(i).getTypeRef().isIsArray(),
								action.getExtraIndex().get(ii), builder, ctx);
						builder.append("]");
						ii++;
					}
					builder.append(" = ");
					if (action.getExpression() != null) {
						cast(action.getProperty().getTypeRef().getType(), action.getProperty().getTypeRef().isIsArray(),
								action.getExpression(), builder, ctx);
					}else {
						generate(action.getCf(), builder,ctx, 0);
					}
					builder.append("\n");
					}
				}else {
					if (action.getExtraProperty().get(i) instanceof Property) {
						builder.append("self.");
					}

					builder.append(ctx.getVariableName(action.getExtraProperty().get(i)));
					if(action.getExtraIndex().size()!=0 && ii<action.getExtraIndex().size()) {
						builder.append("[");
						generate(action.getExtraIndex().get(ii), builder, ctx);
						builder.append("]");
						ii++;
					}
					builder.append(" = ");
					generate(action.getExpression(), builder, ctx);
					builder.append("\n");
				}
				i++;
			}
		}
		
	}


	public void generate(CpropertyAssignment action, StringBuilder builder, Context ctx, int tabformat) {
		retractappend(builder, tabformat);
		if(action.getCpropertys() != null) {
			if (action.getCpropertys().get(0) instanceof Cproperty) {
				builder.append("self.");
			}

//			for (Variable c: action.getCproperty()) {
//				builder.append(ctx.getVariableName(c) + ".");
//			}
			
			if(action.getCpropertys().get(0).getTypeRef().getCardinality() != null && action.getCindex() != null) {
				builder.append(ctx.getVariableName(action.getCpropertys().get(0)));
				StringBuilder cTempBuilder = new StringBuilder();
				generate(action.getCindex(), cTempBuilder, ctx);
				builder.append("[" + cTempBuilder.toString() + "]");
				builder.append(".");
			} else {
				builder.append(ctx.getVariableName(action.getCpropertys().get(0)));
				builder.append(".");
			}
			
			//=============================================================================================
			if(action.getProperty().getTypeRef().getCardinality() != null && action.getPindex() != null) {
//				builder.append(ctx.getVariableName(action.getProperty()));
				builder.append(action.getProperty().getName() );
				StringBuilder tempBuilder = new StringBuilder();
				generate(action.getPindex(), tempBuilder, ctx);
				builder.append("[" + tempBuilder.toString() + "]");
				builder.append(" = ");
				cast(action.getProperty().getTypeRef().getType(), false, action.getExpression(), builder, ctx);
				builder.append("\n");
			} else {
				builder.append(action.getProperty().getName() );
				builder.append("=");
				cast(action.getProperty().getTypeRef().getType(), action.getProperty().getTypeRef().isIsArray(),
						action.getExpression(), builder, ctx);
				builder.append("\n");
			}
			
		}
		
		
	}
	//---------------------------------------------------------------------------------------------
	//---------------------------------------------------------------------------------------------
	public void generate(CpropertyReference expression, StringBuilder builder, Context ctx) {
//		retractappend("", builder, tabformat);
//		builder.append(" ");
		if(expression.getCpropertys() != null) {
			
//			for (Variable c: action.getCproperty()) {
//				builder.append(ctx.getVariableName(c) + ".");
//			}
			if (expression.getCpropertys().get(0) instanceof Cproperty) {
				builder.append("self.");
			}
			
			
			if(expression.getCindex() != null) {
				builder.append(ctx.getVariableName(expression.getCpropertys().get(0)));
				StringBuilder cTempBuilder = new StringBuilder();
				generate(expression.getCindex(), cTempBuilder, ctx);
				builder.append("[" + cTempBuilder.toString() + "]");
//				builder.append(".");
			} else {
				builder.append(ctx.getVariableName(expression.getCpropertys().get(0)));
//				builder.append(".");
			}
			
			if(expression.getProperty() != null) {
//				if(expression.getProperty().getTypeRef().getCardinality() != null ) {
						if(!expression.getCpropertys().get(0).getTypeRef().getType().getName().equals("pythonDict")) {
	//				builder.append(ctx.getVariableName(expression.getProperty()));
					builder.append(".");
					builder.append(expression.getProperty().getName());
					if(expression.getPindex() != null) {
						StringBuilder tempBuilder = new StringBuilder();
						generate(expression.getPindex(), tempBuilder, ctx);
						builder.append("[" + tempBuilder.toString() + "]");
					}
				}
				else {
					builder.append("[\"" + expression.getProperty().getName() + "\"]");
					}
				
			}
		}
	}
	

	public void generate(SendAction action, StringBuilder builder, Context ctx, int tabformat) {
		retractappend(builder, tabformat);
		boolean isSendOut = false;
        for (ExternalConnector eco : ConfigurationHelper.getExternalConnectors(ctx.getCurrentConfiguration())) {
            if (EcoreUtil.equals(action.getPort(), eco.getPort())) {
            	isSendOut = true;
            }
        }
        if (action.getMessage().getName().equals("singleLightActionMsg")) {
        	ExternalConnector eco = ConfigurationHelper.getExternalConnectors(ctx.getCurrentConfiguration()).get(0);
        	char firstCapital = eco.getProtocol().getName().charAt(0);
			if (firstCapital >= 'A' && firstCapital <= 'Z') {
			}else {
				builder.append("self.");
			}
			builder.append("mqtt.client.publish('" + action.getMessage().getName() + "', ");
			for (Parameter p : action.getMessage().getParameters()) {
				builder.append("str(" + p.getName() + "_var), ");
			}
			builder.append("qos=0)\n");
        	return;
        }
        builder.append("event = {}\n");
        retractappend(builder, tabformat);
        builder.append("event["+"'_msg'"+"] = ");
        if(isSendOut)
			builder.append("'" + action.getPort().getName() + "!" + action.getMessage().getName() + "'\n");
		else
			builder.append("'" + action.getPort().getName() + "'\n" );
        
        retractappend(builder, tabformat);
        
        
		for (Parameter p : action.getMessage().getParameters()) {
//			StringBuilder temp = new StringBuilder();
//			generate(ex, temp, ctx);
			if(p.getName().toString().equals("y")) {
			builder.append("event['" + p.getName() + "'] = ");
			builder.append(" 1" + "\n");
			}
			else if(p.getName().toString().equals("agent_action")) {
				builder.append("event['" + p.getName() + "'] = ");
				builder.append(" self.action_n_var" + "\n");
			}
			else if(p.getName().toString().equals("AgentObs")) {
				builder.append("event['" + p.getName() + "'] = ");
				builder.append(" self.device_obsAgent_var" + "\n");
			}
			else {
				builder.append("placeholder");
			}
//			generate(p, builder, ctx);
			retractappend(builder, tabformat);
			
		}
		builder.append("self.send_events.put(event)\n");
		
		
	}



	public void generate(FunctionCallStatement action, StringBuilder builder, Context ctx, int tabformat) {
		retractappend(builder, tabformat);
		builder.append("self.");
		builder.append(action.getFunction().getName() + "(");
		int i = 0;
		for (Expression p : action.getParameters()) {
			if (i > 0)
				builder.append(", ");
			generate(p, builder, ctx);
			i++;
		}
		builder.append(")\n");
	}
	

	public void generate(LocalVariable action, StringBuilder builder, Context ctx, int tabformat) {
		retractappend(builder, tabformat);
		builder.append(ctx.getVariableName(action));
		if (action.getInit() != null) {
			builder.append(" = ");
			generate(action.getInit(), builder, ctx);
		} else {
			if(action.getTensor() != null) {
				builder.append(" = [None] * ");
				builder.append(action.getTensor().getDims().get(0).getIntValue());
			}
			
			if(action.getTypeRef() != null) {
				if(action.getTypeRef().getCardinality() != null || action.getTypeRef().isIsArray())
					builder.append(" = []");
				if(action.isReadonly() && action.getTypeRef().getCardinality() == null)
					System.out.println("[ERROR] readonly variable " + action + " must be initialized");
				if(action.getTypeRef().getType().getName().equals("pythonList"))
					builder.append(" = []");
				if(action.getTypeRef().getType().getName().equals("pythonType"))
					builder.append(" = None");
				if(action.getTypeRef().getType().getName().equals("pythonDict"))
					builder.append(" = {}");
				
			}
		}		
//			}
//			if (action.getTypeRef() != null && action.getTypeRef().getCardinality() != null || action.getTypeRef().isIsArray()) {
//				builder.append(" = []");
//			}
//			if (action.getTypeRef() != null && action.isReadonly() && action.getTypeRef().getCardinality() == null)
//				System.out.println("[ERROR] readonly variable " + action + " must be initialized");
//			if(action.getTypeRef() != null && action.getTypeRef().getType().getName().equals("pythonList"))
//				builder.append(" = []");
//			if(action.getTypeRef() != null && action.getTypeRef().getType().getName().equals("pythonType"))
//				builder.append(" = None");
//			}
			
		builder.append("\n");
		
	}


	public void generate(ErrorAction action, StringBuilder builder, Context ctx, int tabformat) {
		builder.append("console.error(''");
		for (Expression msg : action.getMsg()) {
			builder.append("+");
			generate(msg, builder, ctx);
		}
		builder.append(");\n");
		// console.error() always prints lines
	}


	public void generate(PrintAction action, StringBuilder builder, Context ctx, int tabformat) {
		retractappend(builder, tabformat);
		builder.append("print(");
		for (Expression msg : action.getMsg()) {
			generate(msg, builder, ctx);
		}
		builder.append(")\n");
		// console.log() always prints lines
	}


	public void generate(PropertyReference expression, StringBuilder builder, Context ctx) {
		if (expression.getProperty() instanceof Parameter || expression.getProperty() instanceof LocalVariable) {
			builder.append(ctx.getVariableName(expression.getProperty()));
//			builder.append(expression.getProperty().getName());
		}
		else if (expression.getProperty() instanceof Property) {
			builder.append("self.");
			builder.append(ctx.getVariableName(expression.getProperty()));
						
		}
		
	}


	public void generate(EnumLiteralRef expression, StringBuilder builder, Context ctx) {
		builder.append("Enum." + ctx.firstToUpper(expression.getEnum().getName()) + "_ENUM."
				+ expression.getLiteral().getName().toUpperCase());
	}

	public void generate(FunctionCallExpression expression, StringBuilder builder, Context ctx, int tabformat) {
		retractappend(builder, tabformat);
		if (ctx.currentInstance != null)
			builder.append(ctx.currentInstance.getName() + ".");
		else
			builder.append("self.");
		builder.append(expression.getFunction().getName() + "(");
		int i = 0;
		for (Expression p : expression.getParameters()) {
			if (i > 0)
				builder.append(", ");
			generate(p, builder, ctx);
			i++;
		}
		builder.append(")");
	}




	public void generate(EventReference expression, StringBuilder builder, Context ctx) {
		builder.append((((ReceiveMessage) expression.getReceiveMsg()).getMessage().getName()));
		builder.append("[\"" + expression.getParameter().getName() + "\"]");
	}



	public void generate(ArrayInit expression, StringBuilder builder, Context ctx) {
		builder.append("[");
		for(Expression e : expression.getValues()) {
			if (expression.getValues().indexOf(e)>0)
				builder.append(", ");
			generate(e, builder, ctx);
		}
		builder.append("]");
	}


	public void generate(ForAction fa, StringBuilder builder, Context ctx, int tabformat) {
		retractappend(builder, tabformat);
		builder.append("for "+ ctx.getVariableName(fa.getVariable()) + " in ");
		if (fa.getArray().getProperty() instanceof Property) {
			builder.append("self.");
		}
		builder.append(ctx.getVariableName(fa.getArray().getProperty()));
		builder.append(":\n");
		generate(fa.getAction(), builder, ctx,tabformat);

	}
	
	

    public void cast(Type type, boolean isArray, Expression exp, StringBuilder builder, Context ctx) {
        generate(exp, builder, ctx);
    }


    public void generate(ActionBlock action, StringBuilder builder, Context ctx, int tabformat) {
//    	retractappend("", builder, tabformat);
        for (Action a : action.getActions()) {
            generate(a, builder, ctx, tabformat+1);
        }

    }


    public void generate(ExternStatement action, StringBuilder builder, Context ctx, int tabformat) {
    	retractappend(builder, tabformat);
        builder.append(action.getStatement());
        for (Expression e : action.getSegments()) {
            generate(e, builder, ctx);
        }
        builder.append("\n");
    }


    public void generate(ConditionalAction action, StringBuilder builder, Context ctx, int tabformat) {
    	retractappend(builder, tabformat);
        builder.append("if ");
        generate(action.getCondition(), builder, ctx);
        builder.append(" : \n");
        generate(action.getAction(), builder, ctx, tabformat);
        
        if (action.getElseAction() != null) {
        	retractappend(builder, tabformat);
            builder.append("else :\n");
            generate(action.getElseAction(), builder, ctx,tabformat);
            builder.append("\n");
        }

    }


    public void generate(LoopAction action, StringBuilder builder, Context ctx, int tabformat) {
    	retractappend(builder, tabformat);
    	builder.append("while ");
        generate(action.getCondition(), builder, ctx);
        builder.append(" : \n");

        generate(action.getAction(), builder, ctx,tabformat);
        builder.append("\n");
    }


    public void generate(ReturnAction action, StringBuilder builder, Context ctx, int tabformat) {
    	retractappend(builder, tabformat);
        builder.append("return");
        Function parent;
        if (action.getExp() != null) {
        	builder.append(" ");
	        parent = AiotMLHelpers.findContainingFunction(action);
	        boolean isArray = false;
	        if (action.getExp() instanceof PropertyReference) {
	            PropertyReference pr = (PropertyReference) action.getExp();
	            isArray = pr.getProperty().getTypeRef().isIsArray() || pr.getProperty().getTypeRef().getCardinality() != null;
	        }
	        cast(parent.getTypeRef().getType(), isArray, action.getExp(), builder, ctx);
        }
        if (action.getExtraExp().size()!=0) {
        	int i = 0;
        	while(i < action.getExtraExp().size()) {
        		builder.append(",");
        		parent = AiotMLHelpers.findContainingFunction(action);
        		boolean isArray = false;
        		if (action.getExtraExp().get(i) instanceof PropertyReference) {
        			PropertyReference pr = (PropertyReference) action.getExtraExp().get(i);
        			isArray = pr.getProperty().getTypeRef().isIsArray() || pr.getProperty().getTypeRef().getCardinality() != null;
        		}
        		cast(parent.getTypeRef().getType(), isArray, action.getExtraExp().get(i), builder, ctx);
        		i++;
        	}
        }
        
        
        builder.append("\n");
    }


    public void generate(Increment action, StringBuilder builder, Context ctx, int tabformat) {
    	retractappend(builder, tabformat);
    	builder.append(ctx.getVariableName(action.getVar()));
        builder.append("++\n");
    }


    public void generate(Decrement action, StringBuilder builder, Context ctx, int tabformat) {
    	retractappend(builder, tabformat);
    	builder.append(ctx.getVariableName(action.getVar()));
        builder.append("--\n");
    }


    //AiotML expressions that can be compiled the same way for any imperative language like (Java, JS, C)
    public void generate(BooleanLiteral expression, StringBuilder builder, Context ctx) {
        builder.append(ctx.firstToUpper(Boolean.toString(expression.isBoolValue())));    	
    }
    
    public void generate(ArrayIndex expression, StringBuilder builder, Context ctx) {
        generate(expression.getArray(), builder, ctx);
        builder.append("[");
        generate(expression.getIndex(), builder, ctx);
        builder.append("]");
    }

    public void generate(OrExpression expression, StringBuilder builder, Context ctx) {
        generate(expression.getLhs(), builder, ctx);
        builder.append(" or ");
        generate(expression.getRhs(), builder, ctx);
    }


    public void generate(AndExpression expression, StringBuilder builder, Context ctx) {
        generate(expression.getLhs(), builder, ctx);
        builder.append(" and ");
        generate(expression.getRhs(), builder, ctx);
    }


    public void generate(LowerExpression expression, StringBuilder builder, Context ctx) {
        generate(expression.getLhs(), builder, ctx);
        builder.append(" < ");
        generate(expression.getRhs(), builder, ctx);
    }


    public void generate(GreaterExpression expression, StringBuilder builder, Context ctx) {
        generate(expression.getLhs(), builder, ctx);
        builder.append(" > ");
        generate(expression.getRhs(), builder, ctx);
    }


    public void generate(LowerOrEqualExpression expression, StringBuilder builder, Context ctx) {
        generate(expression.getLhs(), builder, ctx);
        builder.append(" <= ");
        generate(expression.getRhs(), builder, ctx);
    }


    public void generate(GreaterOrEqualExpression expression, StringBuilder builder, Context ctx) {
        generate(expression.getLhs(), builder, ctx);
        builder.append(" >= ");
        generate(expression.getRhs(), builder, ctx);
    }


    public void generate(EqualsExpression expression, StringBuilder builder, Context ctx) {
        generate(expression.getLhs(), builder, ctx);
        builder.append(" == ");
        generate(expression.getRhs(), builder, ctx);
    }


    public void generate(NotEqualsExpression expression, StringBuilder builder, Context ctx) {
        generate(expression.getLhs(), builder, ctx);
        builder.append(" != ");
        generate(expression.getRhs(), builder, ctx);
    }


    public void generate(PlusExpression expression, StringBuilder builder, Context ctx) {
        generate(expression.getLhs(), builder, ctx);
        builder.append(" + ");
        generate(expression.getRhs(), builder, ctx);
    }


    public void generate(MinusExpression expression, StringBuilder builder, Context ctx) {
        generate(expression.getLhs(), builder, ctx);
        builder.append(" - ");
        generate(expression.getRhs(), builder, ctx);
    }


    public void generate(TimesExpression expression, StringBuilder builder, Context ctx) {
        generate(expression.getLhs(), builder, ctx);
        builder.append(" * ");
        generate(expression.getRhs(), builder, ctx);
    }


    public void generate(DivExpression expression, StringBuilder builder, Context ctx) {
        generate(expression.getLhs(), builder, ctx);
        builder.append(" / ");
        generate(expression.getRhs(), builder, ctx);
    }


    public void generate(ModExpression expression, StringBuilder builder, Context ctx) {
        generate(expression.getLhs(), builder, ctx);
        builder.append(" % ");
        generate(expression.getRhs(), builder, ctx);
    }

    public void generate(NotExpression expression, StringBuilder builder, Context ctx) {
        builder.append(" not ");
        generate(expression.getTerm(), builder, ctx);
//        builder.append(")");
    }

    
    public void generate(FunctionCallExpression expression, StringBuilder builder, Context ctx) {
		if (ctx.currentInstance != null)
			builder.append(ctx.currentInstance.getName() + ".");
		else
			builder.append("self."); 
		builder.append(expression.getFunction().getName() + "(");
		int i = 0;
		for (Expression p : expression.getParameters()) {
			if (i > 0)
				builder.append(", ");
			generate(p, builder, ctx);
			i++;
		}
		builder.append(")");
	}

}
