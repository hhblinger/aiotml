/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 */
package org.aiotml.compilers.python;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;
import org.aiotml.compilers.Context;
import org.aiotml.compilers.builder.Section;
import org.aiotml.compilers.builder.SourceBuilder;
import org.aiotml.compilers.thing.common.FSMBasedThingImplCompiler;
import org.aiotml.compilers.thing.common.NewFSMBasedThingImplCompiler;
import org.aiotml.xtext.constraints.AiotMLHelpers;
import org.aiotml.xtext.helpers.AnnotatedElementHelper;
import org.aiotml.xtext.helpers.CompositeStateHelper;
import org.aiotml.xtext.helpers.ConfigurationHelper;
import org.aiotml.xtext.helpers.StateHelper;
import org.aiotml.xtext.helpers.ThingHelper;
import org.aiotml.xtext.helpers.AiotMLElementHelper;
import org.aiotml.xtext.aiotML.AbstractConnector;
import org.aiotml.xtext.aiotML.Action;
import org.aiotml.xtext.aiotML.ActionBlock;
import org.aiotml.xtext.aiotML.AiotMLModel;
import org.aiotml.xtext.aiotML.CompositeState;
import org.aiotml.xtext.aiotML.Configuration;
import org.aiotml.xtext.aiotML.Connector;
import org.aiotml.xtext.aiotML.Cproperty;
import org.aiotml.xtext.aiotML.Expression;
import org.aiotml.xtext.aiotML.ExternalConnector;
import org.aiotml.xtext.aiotML.FinalState;
import org.aiotml.xtext.aiotML.Function;
import org.aiotml.xtext.aiotML.FunctionCallStatement;
import org.aiotml.xtext.aiotML.Handler;
import org.aiotml.xtext.aiotML.IntegerLiteral;
import org.aiotml.xtext.aiotML.InternalTransition;
import org.aiotml.xtext.aiotML.Layer;
import org.aiotml.xtext.aiotML.Message;
import org.aiotml.xtext.aiotML.Model;
import org.aiotml.xtext.aiotML.Parameter;
import org.aiotml.xtext.aiotML.Port;
import org.aiotml.xtext.aiotML.Property;
import org.aiotml.xtext.aiotML.ProvidedPort;
import org.aiotml.xtext.aiotML.ReceiveMessage;
import org.aiotml.xtext.aiotML.Region;
import org.aiotml.xtext.aiotML.Sequ;
import org.aiotml.xtext.aiotML.Session;
import org.aiotml.xtext.aiotML.State;
import org.aiotml.xtext.aiotML.StateContainer;
import org.aiotml.xtext.aiotML.Strategy;
import org.aiotml.xtext.aiotML.StrategyProperty;
import org.aiotml.xtext.aiotML.MultiAgent;
import org.aiotml.xtext.aiotML.MultiAgentProperty;
import org.aiotml.xtext.aiotML.SubThing;
import org.aiotml.xtext.aiotML.Thing;
import org.aiotml.xtext.aiotML.Transition;
import org.aiotml.xtext.aiotML.impl.ExternalConnectorImpl;

public class PyThingImplCompiler extends FSMBasedThingImplCompiler {
	protected ArrayList<String> msgList = new ArrayList<String>();
	protected HashMap<String, ArrayList<String>> msgPara = new HashMap<String, ArrayList<String>>();
	protected ArrayList<String> statelist = new ArrayList<String>();
	protected HashMap<String, ArrayList<String>> state_msg = new HashMap<String, ArrayList<String>>();

	public void generateImplementation(Thing thing, Context ctx) {

		String template = new String();
		Copier copier = new Copier();
		AiotMLModel result = (AiotMLModel) copier.copy(AiotMLHelpers.findContainingModel(thing));
		
		if (result.getTemplates().size()>0) {
			template = result.getTemplates().get(0).getPath().toString();
			template = ctx.getTemplateByID(template);
		}else {
			template = getThingInit(thing, ctx);
		}
		
//		StringBuilder eventsBuilder = ctx.getBuilder("events.py");
//		StringBuilder builder = ctx.getBuilder(getThingPath(thing, ctx));
		StringBuilder modelBuilder = new StringBuilder();
		StringBuilder subBuilder = new StringBuilder();
		StringBuilder propertyBuilder = new StringBuilder();
		template = template.replace("#$THINENAME$#", ctx.firstToUpper(thing.getName()));
		generateSubThings(thing, subBuilder, ctx);
		template = template.replace("#$SUBTHINGS$#", "#$SUBTHINGS$#\n" + subBuilder.toString());
		generateMain(thing, propertyBuilder, ctx);
		template = template.replace("#$PROPERTYS$#", "#$PROPERTYS$#\n" + propertyBuilder.toString());
		StringBuilder functionBuilder = new StringBuilder();
		for (Function f : ThingHelper.allConcreteFunctions(thing)) {
			generateFunction(f, thing, functionBuilder, ctx);
		}
		template = template.replace("#$FUNCTIONS$#", "#$FUNCTIONS$#\n" + functionBuilder.toString());
		//protocol
		StringBuilder mqttBuilder = new StringBuilder();
		generateMqtt(thing, mqttBuilder, ctx);
		template = template.replace("#$MQTT$#", "#$MQTT$#\n" + mqttBuilder.toString());
		//protocol Init
		StringBuilder protocolInitBuilder = new StringBuilder();
		generateProtocolInit(thing, protocolInitBuilder, ctx);
		template = template.replace("#$PROINIT$#", "#$PROINIT$#\n" + protocolInitBuilder.toString());

		// Connector:TODO
//		Map<String, String> port2pro = new HashMap<String, String>();
//		for (Configuration cf : result.getConfigs()) {
//			for (AbstractConnector ac : cf.getConnectors()) {
//				if (ac.getClass().getSimpleName().equals("ExternalConnectorImpl")) {
//					ExternalConnectorImpl ec = (ExternalConnectorImpl) ac;
//					port2pro.put(ec.getPort().toString(), ec.getProtocol().toString());
//				}
//			}
//		}
		
//		// Port
//		EList<Message> ms = thing.getPorts().get(0).getSends();
//		EList<Message> mr = thing.getPorts().get(0).getReceives();
//		System.out.println(port2pro);
//		System.out.println(ms.get(0).toString());
//		System.out.println(mr.get(0).toString());
		
//		String mqtt = ctx.getTemplateByID("templates/mqtt.py");
//		for(Strategy stra : ConfigurationHelper.allStrategys((Configuration)thing.eContainer())) {
		String p_function_name = "";
		for (MultiAgentProperty mapro : thing.getMas()) {
			MultiAgent ma = (MultiAgent) mapro.getTypeRef().getType();
			Strategy strategy = (Strategy) ma.getAgents().get(0).getAmodel().getTypeRef().getType();
			if (strategy.getDevices() != null) {
				for (Cproperty c : strategy.getDevices()) {
					if (AnnotatedElementHelper.hasAnnotation(c, "agent"))
						template = template.replace("#$AGENTNUM$#",
								String.valueOf(((IntegerLiteral) (c.getTypeRef().getCardinality())).getIntValue()));
					if (AnnotatedElementHelper.hasAnnotation(c, "landmark"))
						template = template.replace("#$LANDMARKNUM$#",
								String.valueOf(((IntegerLiteral) (c.getTypeRef().getCardinality())).getIntValue()));
				}
			}
			if (strategy.getActiondims().size() != 0) {
				template = template.replace("#$ACTIONDIM$#",
						String.valueOf(strategy.getActiondims().get(0).getIntValue()));
			}
			if (strategy.getObsdims().size() != 0) {
				template = template.replace("#$OBSDIM$#", String.valueOf(strategy.getObsdims().get(0).getIntValue()));
			}
//			template.replace("");
//			strategy.getRewdims();

			Model mp = (Model) strategy.getPmodel().getTypeRef().getType();
			Model mq = (Model) strategy.getQmodel().getTypeRef().getType();

			// first way :strategy add
			String scope = "";
			if (mp.getScope() != null) {
				scope = mp.getScope();
			}
			Boolean reuse = false;
			if (mp.getReuse() != null) {
				reuse = mp.getReuse().isBoolValue();
			}
			// get Parameters
			EList<String> inParameter = mp.getIn();
			EList<String> outParameter = mp.getOut();
			EList<Layer> layers = mp.getLayers();
			List<String> layerParameter = new ArrayList<String>();
			for (Layer l : layers) {// get parameters of layers
				if (l.getNoutputs() != null && !layerParameter.contains(l.getNoutputs())) {
					layerParameter.add(l.getNoutputs());
				}
				if (l.getKsize() != null && !layerParameter.contains(l.getKsize())) {
					layerParameter.add(l.getKsize());
				}
				if (l.getStride() != null && !layerParameter.contains(l.getStride())) {
					layerParameter.add(l.getStride());
				}
			}
			List<String> InParameters = new ArrayList<String>();
			for (String i : inParameter) {
				if (!outParameter.contains(i)) {
					InParameters.add(i);
				}
			}
			EList<Sequ> sequs = mp.getSequs();
			List<String> inputDim = new ArrayList<String>();
			for (Sequ i : sequs) {
				if (i.getDrateID() != null && !inputDim.contains(i.getDrateID())) {
					inputDim.add(i.getDrateID());
				}
				if (i.getUnitsID() != null && !inputDim.contains(i.getUnitsID())) {
					inputDim.add(i.getUnitsID());
				}
				if (i.getIdim() != null&& !inputDim.contains(i.getIdim())) {
					inputDim.add(i.getIdim());
				}
			}
			if(mp.getLrID() != null && !inputDim.contains(mp.getLrID())) {
				inputDim.add(mp.getLrID());
			}
			if(mp.getMomentumID() != null && !inputDim.contains(mp.getMomentumID())) {
				inputDim.add(mp.getMomentumID());
			}
			// generate parameters
			int countParameter = 0;
			modelBuilder.append("def " + mp.getName() + "(");
			p_function_name = mp.getName().toUpperCase();
			for (String i : InParameters) {
				if (countParameter != 0) {
					modelBuilder.append(", ");
				}
				modelBuilder.append(i);
				countParameter++;
			}
			for (String i : layerParameter) {
				if (countParameter != 0) {
					modelBuilder.append(", ");
				}
				modelBuilder.append(i);
				countParameter++;
			}
			if (scope != "") {
				modelBuilder.append(", " + scope);
			}
			if (mp.getReuse() != null) {
				if (reuse == false) {
					modelBuilder.append(", reuse = False");
				} else {
					modelBuilder.append(", reuse = True");
				}
			}
			if (mp.getN() != null) {
				modelBuilder.append(", default_num_outputs = " + mp.getN().getIntValue());
			}
			if (mp.getK() != null) {
				modelBuilder.append(", default_kernel_size = " + mp.getN().getIntValue());
			}
			if (mp.getS() != null) {
				modelBuilder.append(", default_stride = " + mp.getN().getIntValue());
			}
			if (mp.getIfsequ().isBoolValue() == true) {
				for (String i : inputDim) {
					if (countParameter != 0) {
						modelBuilder.append(", ");
					}
					modelBuilder.append(i);
					countParameter++;
				}
			}
			modelBuilder.append("):\n");
			// generate layers
			if (scope != "") {
				modelBuilder.append("    with tf.variable_scope(" + scope + ", reuse=reuse):\n");
			}
			for (int i = 0; i < mp.getLayers().size(); i++) {
				modelBuilder.append("		");
				modelBuilder.append(mp.getOut().get(i) + " = ");
				modelBuilder.append("layers." + mp.getLayers().get(i).getLtype() + "(");
				modelBuilder.append(mp.getIn().get(i));
				if (mp.getLayers().get(i).getNoutputs() != null) {
					modelBuilder.append(", num_outputs = " + mp.getLayers().get(i).getNoutputs());
				} else {
					if (mp.getN() != null) {
						modelBuilder.append(", num_outputs = default_num_outputs");
					}
				}
				if (mp.getLayers().get(i).getKsize() != null) {
					modelBuilder.append(", ksize = " + mp.getLayers().get(i).getKsize());
				} else {
					if (mp.getK() != null) {
						modelBuilder.append(", num_outputs = default_kernel_size");
					}
				}
				if (mp.getLayers().get(i).getStride() != null) {
					modelBuilder.append(", stride = " + mp.getLayers().get(i).getStride());
				} else {
					if (mp.getS() != null) {
						modelBuilder.append(", num_outputs = default_stride");
					}
				}
				if (mp.getLayers().get(i).getAf() != null) {
					modelBuilder.append(", activation_fn = ");
					if (mp.getLayers().get(i).getAf() == "None") {
						modelBuilder.append("None");
					} else {
						modelBuilder.append("tf.nn." + mp.getLayers().get(i).getAf() + ")\n");
					}
				}
			}
			if (outParameter.size() > 0) {
				modelBuilder.append("		return " + outParameter.get(outParameter.size() - 1) + "\n");
			}

			// second way :strategy add
			if (mp.getIfsequ() != null && mp.getIfsequ().isBoolValue() == true) {
				modelBuilder.append("    model = Sequential()\n");
				for (Sequ i : mp.getSequs()) {
					modelBuilder.append("    model.add(");
					if (i.getLtype().toString().equals("Dense")) {
						modelBuilder.append("Dense(");
						if (i.getUnits() != null) {
							modelBuilder.append(i.getUnits().getIntValue());
						}
						if (i.getUnitsID() != null) {
							modelBuilder.append(i.getUnitsID());
						}
						if (i.getIdim() != null) {
							modelBuilder.append(", input_dim=" + i.getIdim());
						}
						modelBuilder.append(", activation='" + i.getAf() + "'))\n");
					} else if (i.getLtype().toString().equals("Dropout")) {
						if (i.getDrate() != null) {
							modelBuilder.append("Dropout(" + i.getDrate().getDoubleValue() + "))\n");
						}
						if (i.getDrateID() != null) {
							modelBuilder.append("Dropout(" + i.getDrateID() + "))\n");
						}
						
					} else {

					}
				}
				modelBuilder.append("    model.compile(loss='" + mp.getLoss() + "', ");
				modelBuilder.append("optimizer=" + mp.getOptimizer());
				if (mp.getLr() != null || mp.getLrID() != null || mp.getMomentum() != null) {
					modelBuilder.append("(");
					if (mp.getLr() != null) {
						modelBuilder.append("lr=" + mp.getLr().getDoubleValue());
					}else if (mp.getLrID() != null) {
						modelBuilder.append("lr=" + mp.getLrID());
					}
					if (mp.getMomentum() != null) {
						if (mp.getLr() != null || mp.getLrID() != null) {
							modelBuilder.append(", ");
						}
						modelBuilder.append("momentum=" + mp.getMomentum());
					}
					modelBuilder.append(")");
				}
				modelBuilder.append(")\n");
				modelBuilder.append("    return model");
			}

			if (mp != mq) {
				System.out.print("mq != mp");
			}
		}
		String pf_replace_token = "#$" + p_function_name + "$#";
		template = template.replace(pf_replace_token, pf_replace_token + "\n" + modelBuilder.toString());
//		for (Port p : AiotMLHelpers.allPorts(thing)) {
//			if (!AnnotatedElementHelper.isDefined(p, "public", "false")) {
//				for (Message m : p.getReceives()) {
//					
//					for (Parameter pa : m.getParameters()) {
//						if (m.getParameters().indexOf(pa) > 0)
//							builder.append(", ");
//						builder.append(ctx.protectKeyword(ctx.getVariableName(pa)));
//					}
//				}
//			}
//		}

//		for (Property p : AiotMLHelpers.allProperties(thing)) {
//			if (p.getTypeRef().getCardinality() != null || p.getTypeRef().isIsArray()) {
//				builder.append("		self." + ctx.getVariableName(p) + " = []");
//			} else {
//				builder.append("		self." + ctx.getVariableName(p) + " = ");
//				if (p.getInit() != null) {
//					ctx.getCompiler().getThingActionCompiler().generate(p.getInit(), builder, ctx);
//				} else {
//					builder.append("None");
//				}
//			}
//			builder.append("\n");
//		}

//		for (Property p : ThingHelper.allPropertiesInDepth(thing)) {
////			builder.append("private ");
////			builder.append(JavaHelper.getJavaType(p.getTypeRef().getType(), p.getTypeRef().isIsArray(), ctx) + " " + ctx.getVariableName(p));
////			builder.append(";\n");
//		}
//		for (Property p : ThingHelper.allSessionsProperties(thing)) {
//		}
//
//		builder.append("//Message types\n");
//		
//		for (Message m : AiotMLHelpers.allMessages(thing)) {
//
//		}
		StringBuilder transfuncBuilder = new StringBuilder();
		StringBuilder statefuncBuilder = new StringBuilder();
		/* ----- Build state-machine ----- */
		statefuncBuilder.append("	matter = " + ctx.firstToUpper(thing.getName()) + "()\n");
		for (CompositeState b : AiotMLHelpers.allStateMachines(thing))
			generateStateMachine(b, statefuncBuilder, transfuncBuilder, ctx);
		StringBuilder smFunctioBuilder = new StringBuilder();
		for (CompositeState b : AiotMLHelpers.allStateMachines(thing))
			generateSmFunction(b, smFunctioBuilder, ctx);// enter
		StringBuilder mainBuilder = new StringBuilder();
		for (CompositeState b : AiotMLHelpers.allStateMachines(thing))
			generateMainLoop(b, mainBuilder, ctx);

		template = template.replace("#$STATECREATE$#", "#$STATECREATE$#\n" + statefuncBuilder.toString());
		template = template.replace("#$TRANSFUNCTION$#", "#$TRANSFUNCTION$#\n" + transfuncBuilder.toString());
		template = template.replace("#$TRANSFUNCTION$#", "#$TRANSFUNCTION$#\n" + smFunctioBuilder.toString());
		template = template.replace("#$MAINLOOP$#", "#$MAINLOOP$#\n" + mainBuilder.toString());

		try {
			final File f = new File(ctx.getOutputDirectory() + "/" + ctx.firstToUpper(thing.getName()) + ".py");
			final OutputStream output = new FileOutputStream(f);
			IOUtils.write(template, output, Charset.forName("UTF-8"));
			IOUtils.closeQuietly(output);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	protected String getThingPath(Thing thing, Context ctx) {
		return ctx.firstToUpper(thing.getName()) + ".py";
	}
	
	protected String getThingInit(Thing thing, Context ctx) {
		StringBuilder template = new StringBuilder();
		long subSize = thing.getCproperties().size();
		if (subSize>0){
			template.append("#$SUBTHINGS$#\n\n");
		}
		
		template.append("class #$THINENAME$# (object):\n");
		template.append("	def __init__(self):\n");
		template.append("		self.events = queue.Queue()\n");
		template.append("		self.send_events = queue.Queue()\n");
		template.append("#$PROPERTYS$#\n\n");
		template.append("#$PROINIT$#\n\n");
		
		long funcSize = ThingHelper.allConcreteFunctions(thing).size();
		if (funcSize>0) {
			template.append("#$FUNCTIONS$#\n\n");
		}
		
		template.append("#$TRANSFUNCTION$#\n\n");
		
		template.append("#$MAINLOOP$#\n\n");
		
		Boolean mqtt = false;
		for (ExternalConnector eco : ConfigurationHelper.getExternalConnectors(ctx.getCurrentConfiguration())) {
			if (eco.getProtocol().getName().toLowerCase().equals("onenet")) {
				mqtt = true;
			}
		}
		if(mqtt) {
			template.append("#$MQTT$#\n\n");
		}
		
		for (MultiAgentProperty mapro : thing.getMas()) {
			MultiAgent ma = (MultiAgent) mapro.getTypeRef().getType();
			Strategy strategy = (Strategy) ma.getAgents().get(0).getAmodel().getTypeRef().getType();
			Model mp = (Model) strategy.getPmodel().getTypeRef().getType();
			Model mq = (Model) strategy.getQmodel().getTypeRef().getType();
			String p_function_name = mp.getName().toUpperCase();
			String pf_replace_token = "#$" + p_function_name + "$#";
			
			template.append(pf_replace_token + "\n\n");
		}
		
		template.append("def CreateStateMachine():\n");
		template.append("#$STATECREATE$#");
		template.append("	pass");
		
		return template.toString();
	}

	protected void generateSmFunction(CompositeState sm, StringBuilder builder, Context ctx) {

		for (State s : sm.getSubstate()) {
			if (s.getEntry() != null) {
				String state_name = "state_" + AiotMLElementHelper.qname(s, "_");
				builder.append("	");
				builder.append("def " + state_name + "_enter" + "(self, fakePara):\n");
				PyThingActionCompiler compiler = (PyThingActionCompiler) ctx.getCompiler().getThingActionCompiler();
				compiler.generate(s.getEntry(), builder, ctx, 1);
			}
		}
	}
	protected void generateProtocolInit(Thing thing, StringBuilder builder, Context ctx) {
		for (ExternalConnector eco : ConfigurationHelper.getExternalConnectors(ctx.getCurrentConfiguration())) {
			if (eco.getProtocol().getName().equals("WS")) {
				String ip = AnnotatedElementHelper.annotationOrElse(eco.getProtocol(), "ip", "''");
				String port = AnnotatedElementHelper.annotationOrElse(eco.getProtocol(), "port", "''");
				builder.append("		self." + eco.getProtocol().getName() + " = WS('WS', '" + ip + "','" + port+ "', self.events)\n");
			}else if (eco.getProtocol().getName().toLowerCase().equals("onenet")) {
				String ip = AnnotatedElementHelper.annotationOrElse(eco.getProtocol(), "ip", "''");
				String port = AnnotatedElementHelper.annotationOrElse(eco.getProtocol(), "port", "''");
				String name = AnnotatedElementHelper.annotationOrElse(eco.getProtocol(), "name", "''");
				String devideId = AnnotatedElementHelper.annotationOrElse(eco.getProtocol(), "devideId", "''");
				String productId = AnnotatedElementHelper.annotationOrElse(eco.getProtocol(), "productId", "''");
				String password = AnnotatedElementHelper.annotationOrElse(eco.getProtocol(), "password", "''");
				String subscribes = AnnotatedElementHelper.annotationOrElse(eco.getProtocol(), "subscribes", "''");
				char firstCapital = eco.getProtocol().getName().charAt(0);
				if (firstCapital >= 'A' && firstCapital <= 'Z') {
					builder.append("	subscribes = " + subscribes + "\n");
					builder.append("	mqtt = MQTT('" + name + "', '" + ip + "', " + port + ", '" + devideId  + "', '" + productId  + "', '"
							 + password  + "', subscribes)\n");
				}else {
					builder.append("	self.subscribes = " + subscribes + "\n");
					builder.append("	self.mqtt = MQTT('" + name + "', '" + ip + "', " + port + ", '" + devideId  + "', '" + productId  + "', '"
							 + password  + "', self.subscribes)\n");
				}
			}
		}
	}

	protected void generateMain(Thing thing, StringBuilder builder, Context ctx) {
//		builder.append("class "+ ctx.firstToUpper(thing.getName())+ ":\n");
//		builder.append("	def __init__(self):\n");

		for (Cproperty cp : thing.getCproperties()) {
			builder.append("		");
			builder.append("self." + ctx.getVariableName(cp) + "=");
			builder.append(ctx.firstToUpper(cp.getTypeRef().getType().getName()) + "()\n");
		}

		for (Property p : thing.getProperties()) {
			if (p.getTensor() != null) {
				builder.append("		self." + ctx.getVariableName(p));
				builder.append(" = [None] * ");
				builder.append(p.getTensor().getDims().get(0).getIntValue());
//				builder.append("		self." + ctx.getVariableName(p) + " = placeholder");
			} else if (p.getTypeRef() != null && p.getTypeRef().getCardinality() != null || p.getTypeRef().isIsArray()
					|| p.getTypeRef().getType().getName().equals("pythonList")) {
//				subthingsbuilder.append("this." + ctx.getVariableName(p) + " = []");
				builder.append("		self." + ctx.getVariableName(p) + " = []");
			} else {
//				subthingsbuilder.append("this." + ctx.getVariableName(p) + " = ");
				builder.append("		self." + ctx.getVariableName(p) + " = ");
				if (p.getInit() != null) {
					ctx.getCompiler().getThingActionCompiler().generate(p.getInit(), builder, ctx);
				} else {
					builder.append("None");
				}
			}

			builder.append("\n");
		}

	}

	protected void generateSubThings(Thing thing, StringBuilder builder, Context ctx) {

		for (Cproperty cp : thing.getCproperties()) {

			SubThing s = (SubThing) cp.getTypeRef().getType();

			builder.append("class " + ctx.firstToUpper(s.getName()) + ":\n");
			builder.append("	def __init__(self):\n");

			for (Property p : s.getProperties()) {
				if (p.getTypeRef().getCardinality() != null || p.getTypeRef().isIsArray()) {
//					subthingsbuilder.append("this." + ctx.getVariableName(p) + " = []");

					long arrayLen = ((IntegerLiteral) (p.getTypeRef().getCardinality())).getIntValue();
					if (p.getTypeRef().getType().getName().equals("Double")) {
						builder.append("		self." + p.getName() + " = [");
						builder.append("0");
						while (--arrayLen > 0) {
							builder.append(", 0");
						}
						builder.append("]");
					} else {
						builder.append("		self." + p.getName() + " = []");
					}

				} else {
//					subthingsbuilder.append("this." + ctx.getVariableName(p) + " = ");
					builder.append("		self." + p.getName() + " = ");
					if (p.getInit() != null) {
						ctx.getCompiler().getThingActionCompiler().generate(p.getInit(), builder, ctx);
					} else {
						builder.append("None");
					}
				}
				builder.append("\n");
			}
		}
	}

	protected void generateMainLoop(CompositeState sm, StringBuilder builder, Context ctx) {
		builder.append("	def Mainloop(self):\n");
		builder.append("		if not self.events.empty():\n");
		builder.append("			msg = self.events.get()\n");
		int k = 0;
		Set<String> msgSet = new HashSet<String>();
		for (String msg : this.msgList) {
			msgSet.add(msg);
		}
		for (String msg : msgSet) {
			if (k > 0)
				builder.append("			elif msg['_msg'] == '" + msg + "':\n");
			else
				builder.append("			if msg['_msg'] == '" + msg + "':\n");

			builder.append("				self." + msg + "(msg)\n");

			k++;
		}

		k = 0;

		// 判断send_events是否为空，不为空发送消息
		builder.append("		if not matter.send_events.empty():\n");
		builder.append("			msg = matter.send_events.get()\n");
		builder.append("			if msg[\"_msg\"] == \"msg_port!actionmsg\":\n");
		builder.append("				matter.WS.sendactionmsgOnmsg_port(msg[\"agent_action\"])\n");
		builder.append("			elif msg[\"_msg\"] == \"msg_port!reset\":\n");
		builder.append("				matter.WS.sendresetOnmsg_port(msg[\"y\"])\n");
		builder.append("			elif msg[\"_msg\"] == \"msg_port!done\":\n");
		builder.append("				matter.WS.senddoneOnmsg_port(msg[\"y\"])\n");

		for (State state : sm.getSubstate()) {
			String state_name = "state_" + AiotMLElementHelper.qname(state, "_");

			if (k > 0)
				builder.append("		elif self.state == '" + state_name + "':\n");
			else
				builder.append("		if self.state == '" + state_name + "':\n");
			k++;
			int i = 0;
			if (state.getOutgoing().isEmpty())
				builder.append("			pass\n");
			for (Transition t : state.getOutgoing()) {
				String tstateName = "state_" + AiotMLElementHelper.qname(t.getTarget(), "_");
				if (t.getGuard() != null) {
					if (i > 0)
						builder.append("			elif ");
					else
						builder.append("			if ");
					i++;
					ctx.getCompiler().getThingActionCompiler().generate(t.getGuard(), builder, ctx);
					builder.append(":\n");
				}

				if (t.getEvent() != null) {
//					builder.append("'"+t.getEvent().getName()+"'");
					ReceiveMessage r = (ReceiveMessage) t.getEvent();
					Boolean sendout = false;
					for (ExternalConnector eco : ConfigurationHelper
							.getExternalConnectors(ctx.getCurrentConfiguration())) {
						if (EcoreUtil.equals(r.getPort(), eco.getPort())) {
							sendout = true;
							builder.append("			self." + eco.getProtocol().getName() + ".receive_data()\n");
						}
					}
					if (sendout) {// 闂佸憡鍔曢幊姗�宕曢弶鎴旀闁割偆鍠嶇槐锟�
//						builder.append("	matter."+((ReceiveMessage)t.getEvent()).getName()+"("+((ReceiveMessage)t.getEvent()).getName()+")\n");
					}
				} else {// 闂佸搫鍟版慨鐢电矓閻戣棄绠掗柨鐕傛嫹
					builder.append("				self.to_" + tstateName + "(0)\n");// fake parameter
//					int j = 0;
//					for(String s: msgPara.get(state_msg.get(state_name))) { 
//						if(j>0)
//							builder.append(",");
//						builder.append(s);
//					}
//					builder.append(")\n");
				}
//				
			}

		}

	}

	protected void generateStateMachine(CompositeState sm, StringBuilder builder, StringBuilder transFuncBuilder,
			Context ctx) {

		StringBuilder statesBuilder = new StringBuilder();
		String initState = "state_" + AiotMLElementHelper.qname(sm.getInitial(), "_");
		int i = 0;
		for (State s : sm.getSubstate()) {
			if (!(s instanceof Session)) {
//				generateState(s, builder, ctx);
				String state_name = "\"" + "state_" + AiotMLElementHelper.qname(s, "_") + "\"";
				statelist.add(state_name);
				if (i > 0)
					statesBuilder.append(",");
				statesBuilder.append(state_name);
				i++;
			}
		}
		builder.append("	states = [" + statesBuilder.toString() + "]\n");
		builder.append("	machine = Machine(model=matter, states=states, initial= '" + initState + "')\n");
		for (State s : sm.getSubstate()) {
			if (s.getEntry() != null) {
				String state_name = "state_" + AiotMLElementHelper.qname(s, "_");
				builder.append("	machine.on_enter_" + state_name + "('" + state_name + "_enter')\n");
			}
		}

		for (State s : sm.getSubstate()) {
			String state_name = "state_" + AiotMLElementHelper.qname(s, "_");
			for (InternalTransition it : s.getInternal()) {
//				buildTransitionsHelper(builder, ctx, s, it);
			}
			ArrayList<String> stateList = new ArrayList<String>();
			for (Transition t : s.getOutgoing()) {
//				buildTransitionsHelper(builder, ctx, s, t);
				String tstate_name = "state_" + AiotMLElementHelper.qname(t.getTarget(), "_");
				builder.append("	machine.add_transition(");
//				builder.append();
				if (t.getEvent() != null) {
					ReceiveMessage r = (ReceiveMessage) t.getEvent();
					builder.append("'" + r.getMessage().getName() + "', ");
					ArrayList<String> paras = new ArrayList<String>();
					for (Parameter p : r.getMessage().getParameters()) {
						paras.add(p.getName()); // TODO value?
					}
					msgList.add(r.getMessage().getName());
					msgPara.put(r.getMessage().getName(), paras); // neednt just obsmsg

				} else {
					builder.append("'to_" + tstate_name + "',");// DELETE
//					msgList.add("to_"+tstate_name);// DELETE
//					msgPara.put("to_"+tstate_name, null);// DELETE
//					state_msg.put(state_name, "to_"+tstate_name); // DELETE
				}
				stateList.add(tstate_name);
				builder.append("'" + state_name + "'");//
				builder.append(", '" + tstate_name + "'");
				if (t.getAction() != null) {

					PyThingActionCompiler compiler = (PyThingActionCompiler) ctx.getCompiler().getThingActionCompiler();
					if (t.getAction() instanceof ActionBlock && ((ActionBlock) t.getAction()).getActions() != null) {
//						ActionBlock ab = (ActionBlock)t.getAction();
						builder.append(", before='transFunction_" + state_name + "to" + tstate_name + "'");
						if (t.getEvent() != null) {
							ReceiveMessage r = (ReceiveMessage) t.getEvent();
							transFuncBuilder.append("	def transFunction_" + state_name + "to" + tstate_name + "(self,"
									+ r.getMessage().getName() + "):\n");
						} else
							transFuncBuilder
									.append("	def transFunction_" + state_name + "to" + tstate_name + "(self):\n");

						if (((ActionBlock) t.getAction()).getActions().isEmpty()) {
							transFuncBuilder.append("		pass\n");
						}
						for (Action a : ((ActionBlock) t.getAction()).getActions()) {
							if (a instanceof FunctionCallStatement) {
								transFuncBuilder.append(
										"		self." + ((FunctionCallStatement) a).getFunction().getName() + "(");
								int index = 0;
								for (Expression p : ((FunctionCallStatement) a).getParameters()) {
									if (index > 0)
										transFuncBuilder.append(", ");
									compiler.generate(p, transFuncBuilder, ctx);
									index++;
								}
								transFuncBuilder.append(")\n");
							} else {
								compiler.generate(a, transFuncBuilder, ctx, 2);

							}
						}
//						FunctionCallStatement fc = (FunctionCallStatement)t.getAction();
//						builder.append(", before='"+fc.getFunction().getName()+"'");
					} else {
//						compiler.generate(t.getAction(), builder, ctx);
					}
				}
				builder.append(")\n");
//				if (t.getGuard() != null) {
//					
//					
//				}
			}
			state_msg.put(state_name, stateList);
		}

	}

	protected void generateAtomicState(State s, StringBuilder builder, Context ctx) {
		final String state_name = "state_" + AiotMLElementHelper.qname(s, "_");
		if (s instanceof FinalState) {
//			builder.append("final FinalState " + state_name + " = new FinalState(\"" + s.getName() + "\");\n");

		} else {
			builder.append("final AtomicState " + state_name + " = new AtomicState(\"" + s.getName() + "\");\n");
		}

		if (s.getEntry() != null || s.getExit() != null || s instanceof FinalState) {
			if (s.getEntry() != null || s instanceof FinalState) {
				builder.append(state_name + ".onEntry(()->{\n");
				if (s.getEntry() != null)
					ctx.getCompiler().getThingActionCompiler().generate(s.getEntry(), builder, ctx);
				if (s instanceof FinalState) {
					builder.append("stop();\n");
					builder.append("delete();\n");
				}
				builder.append("});\n");
			}
			if (s.getExit() != null) {
				builder.append(state_name + ".onExit(()->{\n");
				if (s.getExit() != null)
					ctx.getCompiler().getThingActionCompiler().generate(s.getExit(), builder, ctx);
				builder.append("});\n\n");
			}
		}
	}

	private void buildTransition(StringBuilder builder, Context ctx, State s, Handler i, ReceiveMessage msg) {
		// TODO:Insert debug logs if needed from the profile
		// DebugProfile debugProfile =
		// ctx.getCompiler().getDebugProfiles().get(AiotMLHelpers.findContainingThing(s));
		String handler_name = "";
		if (i.getName() != null)
			handler_name = i.getName();
		else
			handler_name = "h" + Integer.toString(i.hashCode());

		if (i instanceof Transition) {
			Transition t = (Transition) i;
			builder.append("Transition " + handler_name + " = new Transition();\n");
			builder.append(handler_name + ".from(state_" + AiotMLElementHelper.qname(s, "_") + ").to(state_"
					+ AiotMLElementHelper.qname(t.getTarget(), "_") + ");\n");
		} else {
			builder.append("not transition");
//			builder.append(handler_name + ".from(state_" + AiotMLElementHelper.qname(s, "_") + ");\n");
		}

		if (msg != null) {
			builder.append(handler_name + ".event(" + msg.getMessage().getName() + "Type);\n");
		}

		if (i.getGuard() != null) {
			builder.append(handler_name + ".guard((Event e)->{\n");
			if (msg != null && msg.getMessage().getParameters().size() > 0) {
				builder.append("final " + ctx.firstToUpper(msg.getMessage().getName()) + "MessageType."
						+ ctx.firstToUpper(msg.getMessage().getName()) + "Message " + msg.getMessage().getName()
						+ " = (" + ctx.firstToUpper(msg.getMessage().getName()) + "MessageType."
						+ ctx.firstToUpper(msg.getMessage().getName()) + "Message) e;\n");
			}
			builder.append("return ");
			ctx.getCompiler().getThingActionCompiler().generate(i.getGuard(), builder, ctx);
			builder.append(";\n");
			builder.append("});\n\n");
		}

		if (msg != null) {
			builder.append(handler_name + ".port(" + msg.getPort().getName() + "_port);\n");
		}

		if (i.getAction() != null) {
			builder.append(handler_name + ".action((Event e)->{\n");
			if (msg != null && msg.getMessage().getParameters().size() > 0) {
				builder.append("final " + ctx.firstToUpper(msg.getMessage().getName()) + "MessageType."
						+ ctx.firstToUpper(msg.getMessage().getName()) + "Message " + msg.getMessage().getName()
						+ " = (" + ctx.firstToUpper(msg.getMessage().getName()) + "MessageType."
						+ ctx.firstToUpper(msg.getMessage().getName()) + "Message) e;\n");
			}
			ctx.getCompiler().getThingActionCompiler().generate(i.getAction(), builder, ctx);
			builder.append("});\n\n");
		}
	}

	private void buildTransitionsHelper(StringBuilder builder, Context ctx, State s, Handler i) {
		if (i.getEvent() != null) {
			ReceiveMessage r = (ReceiveMessage) i.getEvent();
			buildTransition(builder, ctx, s, i, r);
		} else {
			buildTransition(builder, ctx, s, i, null);
		}
	}

	protected void generateTransition(Transition t, Message msg, Port p, StringBuilder builder, Context ctx) {

	}

	protected void generateFunction(Function f, Thing thing, StringBuilder builder, Context ctx) {
		char firstCapital = f.getName().charAt(0);
		int tabformat = 0;
		if (firstCapital >= 'A' && firstCapital <= 'Z') {
			builder.append("def " + f.getName() + "(");
		}else {
			builder.append("	def " + f.getName() + "(");
			builder.append("self, ");
			tabformat = 1;
		}
		int count = 0;
		for (Parameter pa : f.getParameters()) {
			if (count>0) {
				builder.append(", ");
			}
			builder.append(ctx.getVariableName(pa));
			count++;
		}
		builder.append("):\n");
//		StringBuilder actionBuilder = new StringBuilder();
		PyThingActionCompiler compiler = (PyThingActionCompiler) ctx.getCompiler().getThingActionCompiler();
		compiler.generate(f.getBody(), builder, ctx, tabformat);
//		builder.append("	" + actionBuilder.toString());
		builder.append("\n");
	}
	
	protected void generateMqtt(Thing thing, StringBuilder builder, Context ctx) {
		Boolean ifMqtt = false;
		for (ExternalConnector eco : ConfigurationHelper.getExternalConnectors(ctx.getCurrentConfiguration())) {
			if (eco.getProtocol().getName().toLowerCase().equals("onenet")) {
				ifMqtt = true;
			}
		}
		if (!ifMqtt) {
			return;
		}
		
		builder.append("class MQTT:\n");
		builder.append("    def __init__(self, name, ip, portnum, devid, proid, pwd, subscribes):\n");
		builder.append("        self.client = mqtt.Client(client_id=devid, protocol=mqtt.MQTTv311)\n");
		builder.append("        self.client.on_connect = self.on_connect\n");
		builder.append("        self.client.on_publish = self.on_publish\n");
		builder.append("        self.client.on_message = self.on_message\n");
		builder.append("        self.client.username_pw_set(username=proid, password=pwd)\n");
		builder.append("        self.client.connect(ip, port=portnum, keepalive=1200)\n");
		builder.append("        for ss in subscribes:\n");
		builder.append("            self.client.subscribe(ss, qos=0)\n");
		builder.append("        self.client.loop_start()\n");
		builder.append("\n    def on_publish(self, client, userdata, mid):\n        pass\n\n");
		builder.append("    def on_connect(self, client, userdata, flags, rc):\n        pass\n\n");
		builder.append("    def on_disconnect(self, client, userdata, flags, rc):\n        pass\n\n");
		builder.append("    def on_message(self, client, userdata, msg):\n");
		builder.append("        rec = msg.payload.decode()\n");
		builder.append("		rec = rec[1:len(rec)-1]\n");
		builder.append("        data = rec.split(',')\n");
		for (ExternalConnector eco : ConfigurationHelper.getExternalConnectors(ctx.getCurrentConfiguration())) {
			if (eco.getProtocol().getName().toLowerCase().equals("onenet")) {
				String subscribes = AnnotatedElementHelper.annotationOrElse(eco.getProtocol(), "subscribes", "''");
				if (subscribes.equals("['singleLightActionMsg']")){
					builder.append("		if data[2] == self.name:\n");
					builder.append("			LightControl(int(data[0]),int(data[1]))\n");
				}
			}
		}
	}

}
