/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 */
package org.aiotml.compilers.python;

import org.aiotml.compilers.Context;
import org.aiotml.compilers.AiotMLCompiler;
import org.aiotml.compilers.configuration.CfgBuildCompiler;
import org.aiotml.compilers.configuration.CfgMainGenerator;
import org.aiotml.compilers.thing.NewThingActionCompiler;
import org.aiotml.compilers.thing.ThingActionCompiler;
import org.aiotml.compilers.thing.ThingApiCompiler;
import org.aiotml.compilers.thing.ThingImplCompiler;
import org.aiotml.compilers.thing.common.FSMBasedThingImplCompiler;
import org.aiotml.compilers.utils.OpaqueAiotMLCompiler;
import org.aiotml.utilities.logging.Logger;
import org.aiotml.xtext.constraints.AiotMLHelpers;
import org.aiotml.xtext.helpers.ConfigurationHelper;
import org.aiotml.xtext.aiotML.Configuration;
import org.aiotml.xtext.aiotML.Thing;
import org.aiotml.xtext.aiotML.AiotMLModel;

public class PyCompiler extends OpaqueAiotMLCompiler{
	public PyCompiler() {
		super(new PyThingActionCompiler(), new PyThingApiCompiler(), new CfgMainGenerator(),
                new CfgBuildCompiler(), new PyThingImplCompiler());
		
	}
	
	
	public PyCompiler(PyThingActionCompiler PyThingActionCompiler, ThingApiCompiler thingApiCompiler,
			CfgMainGenerator mainCompiler, CfgBuildCompiler cfgBuildCompiler, PyThingImplCompiler PyThingImplCompiler) {
		super(PyThingActionCompiler, thingApiCompiler, mainCompiler, cfgBuildCompiler, PyThingImplCompiler);
		this.ctx = new PyContext(this);
	}
	
	
	@Override
    public boolean do_call_compiler(Configuration cfg, Logger log, String... options) {
        ctx.setCurrentConfiguration(cfg);
        System.out.println("python will do compile");
        compile(cfg, AiotMLHelpers.findContainingModel(cfg), true, ctx);
//        ctx.getCompiler().getCfgBuildCompiler().generateDockerFile(cfg, ctx);
//        ctx.getCompiler().getCfgBuildCompiler().generateBuildScript(cfg, ctx);
        //ctx.writeGeneratedCodeToFiles();
        ctx.generateNetworkLibs(cfg);


        ctx.generateStrategyLibs(cfg);
        return true;
    }
	
	private void compile(Configuration t, AiotMLModel model, boolean isNode, Context ctx) {

        for (Thing thing : ConfigurationHelper.allThings(t)) {
            ctx.getCompiler().getThingImplCompiler().generateImplementation(thing, ctx);
        }
//        ctx.getCompiler().getMainCompiler().generateMainAndInit(t, model, ctx);
    }
	
	@Override
	public AiotMLCompiler clone() {
		return new PyCompiler();
	}

	@Override
	public String getID() {
		return "python";
	}

	@Override
	public String getName() {
		return "python for strategy";
	}

	@Override
	public String getDescription() {
		return "Generates python code for the strategy.";
	}
}
