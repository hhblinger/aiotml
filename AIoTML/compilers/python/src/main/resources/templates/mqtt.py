import paho.mqtt.client as mqtt

class MQTT:
    def __init__(self, ip, portnum, devid, proid, pwd, subscribes):
        self.client = mqtt.Client(client_id=devid, protocol=mqtt.MQTTv311)
        self.client.on_connect = self.on_connect
        self.client.on_publish = self.on_publish
        self.client.on_message = self.on_message
        self.client.username_pw_set(username=proid, password=pwd)
        self.client.connect(ip, port=portnum, keepalive=1200)
        for ss in subscribes:
            self.client.subscribe(ss, qos=0)

        self.client.loop_start()

    def on_publish(self, client, userdata, mid):
        pass

    def on_connect(self, client, userdata, flags, rc):
        pass

    def on_disconnect(self, client, userdata, flags, rc):
        pass

    def on_message(self, client, userdata, msg):
        rec = msg.payload.decode()
        data = rec.split(',')
        # MessageHandle