/**
 * *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 */
package org.aiotml.xtext.aiotML.impl;

import java.util.Collection;

import org.aiotml.xtext.aiotML.AiotMLPackage;
import org.aiotml.xtext.aiotML.BooleanLiteral;
import org.aiotml.xtext.aiotML.DoubleLiteral;
import org.aiotml.xtext.aiotML.IntegerLiteral;
import org.aiotml.xtext.aiotML.Layer;
import org.aiotml.xtext.aiotML.Model;
import org.aiotml.xtext.aiotML.Sequ;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.aiotml.xtext.aiotML.impl.ModelImpl#getScope <em>Scope</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.impl.ModelImpl#getReuse <em>Reuse</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.impl.ModelImpl#getN <em>N</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.impl.ModelImpl#getK <em>K</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.impl.ModelImpl#getS <em>S</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.impl.ModelImpl#getIn <em>In</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.impl.ModelImpl#getLayers <em>Layers</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.impl.ModelImpl#getOut <em>Out</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.impl.ModelImpl#getIfsequ <em>Ifsequ</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.impl.ModelImpl#getSequs <em>Sequs</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.impl.ModelImpl#getLoss <em>Loss</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.impl.ModelImpl#getOptimizer <em>Optimizer</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.impl.ModelImpl#getLr <em>Lr</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.impl.ModelImpl#getLrID <em>Lr ID</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.impl.ModelImpl#getMomentum <em>Momentum</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.impl.ModelImpl#getMomentumID <em>Momentum ID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ModelImpl extends TypeImpl implements Model
{
  /**
   * The default value of the '{@link #getScope() <em>Scope</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getScope()
   * @generated
   * @ordered
   */
  protected static final String SCOPE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getScope() <em>Scope</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getScope()
   * @generated
   * @ordered
   */
  protected String scope = SCOPE_EDEFAULT;

  /**
   * The cached value of the '{@link #getReuse() <em>Reuse</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getReuse()
   * @generated
   * @ordered
   */
  protected BooleanLiteral reuse;

  /**
   * The cached value of the '{@link #getN() <em>N</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getN()
   * @generated
   * @ordered
   */
  protected IntegerLiteral n;

  /**
   * The cached value of the '{@link #getK() <em>K</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getK()
   * @generated
   * @ordered
   */
  protected IntegerLiteral k;

  /**
   * The cached value of the '{@link #getS() <em>S</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getS()
   * @generated
   * @ordered
   */
  protected IntegerLiteral s;

  /**
   * The cached value of the '{@link #getIn() <em>In</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIn()
   * @generated
   * @ordered
   */
  protected EList<String> in;

  /**
   * The cached value of the '{@link #getLayers() <em>Layers</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLayers()
   * @generated
   * @ordered
   */
  protected EList<Layer> layers;

  /**
   * The cached value of the '{@link #getOut() <em>Out</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOut()
   * @generated
   * @ordered
   */
  protected EList<String> out;

  /**
   * The cached value of the '{@link #getIfsequ() <em>Ifsequ</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIfsequ()
   * @generated
   * @ordered
   */
  protected BooleanLiteral ifsequ;

  /**
   * The cached value of the '{@link #getSequs() <em>Sequs</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSequs()
   * @generated
   * @ordered
   */
  protected EList<Sequ> sequs;

  /**
   * The default value of the '{@link #getLoss() <em>Loss</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLoss()
   * @generated
   * @ordered
   */
  protected static final String LOSS_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getLoss() <em>Loss</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLoss()
   * @generated
   * @ordered
   */
  protected String loss = LOSS_EDEFAULT;

  /**
   * The default value of the '{@link #getOptimizer() <em>Optimizer</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOptimizer()
   * @generated
   * @ordered
   */
  protected static final String OPTIMIZER_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getOptimizer() <em>Optimizer</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getOptimizer()
   * @generated
   * @ordered
   */
  protected String optimizer = OPTIMIZER_EDEFAULT;

  /**
   * The cached value of the '{@link #getLr() <em>Lr</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLr()
   * @generated
   * @ordered
   */
  protected DoubleLiteral lr;

  /**
   * The default value of the '{@link #getLrID() <em>Lr ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLrID()
   * @generated
   * @ordered
   */
  protected static final String LR_ID_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getLrID() <em>Lr ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLrID()
   * @generated
   * @ordered
   */
  protected String lrID = LR_ID_EDEFAULT;

  /**
   * The cached value of the '{@link #getMomentum() <em>Momentum</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMomentum()
   * @generated
   * @ordered
   */
  protected DoubleLiteral momentum;

  /**
   * The default value of the '{@link #getMomentumID() <em>Momentum ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMomentumID()
   * @generated
   * @ordered
   */
  protected static final String MOMENTUM_ID_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getMomentumID() <em>Momentum ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMomentumID()
   * @generated
   * @ordered
   */
  protected String momentumID = MOMENTUM_ID_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ModelImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return AiotMLPackage.Literals.MODEL;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getScope()
  {
    return scope;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setScope(String newScope)
  {
    String oldScope = scope;
    scope = newScope;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AiotMLPackage.MODEL__SCOPE, oldScope, scope));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public BooleanLiteral getReuse()
  {
    return reuse;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetReuse(BooleanLiteral newReuse, NotificationChain msgs)
  {
    BooleanLiteral oldReuse = reuse;
    reuse = newReuse;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AiotMLPackage.MODEL__REUSE, oldReuse, newReuse);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setReuse(BooleanLiteral newReuse)
  {
    if (newReuse != reuse)
    {
      NotificationChain msgs = null;
      if (reuse != null)
        msgs = ((InternalEObject)reuse).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AiotMLPackage.MODEL__REUSE, null, msgs);
      if (newReuse != null)
        msgs = ((InternalEObject)newReuse).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AiotMLPackage.MODEL__REUSE, null, msgs);
      msgs = basicSetReuse(newReuse, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AiotMLPackage.MODEL__REUSE, newReuse, newReuse));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public IntegerLiteral getN()
  {
    return n;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetN(IntegerLiteral newN, NotificationChain msgs)
  {
    IntegerLiteral oldN = n;
    n = newN;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AiotMLPackage.MODEL__N, oldN, newN);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setN(IntegerLiteral newN)
  {
    if (newN != n)
    {
      NotificationChain msgs = null;
      if (n != null)
        msgs = ((InternalEObject)n).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AiotMLPackage.MODEL__N, null, msgs);
      if (newN != null)
        msgs = ((InternalEObject)newN).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AiotMLPackage.MODEL__N, null, msgs);
      msgs = basicSetN(newN, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AiotMLPackage.MODEL__N, newN, newN));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public IntegerLiteral getK()
  {
    return k;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetK(IntegerLiteral newK, NotificationChain msgs)
  {
    IntegerLiteral oldK = k;
    k = newK;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AiotMLPackage.MODEL__K, oldK, newK);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setK(IntegerLiteral newK)
  {
    if (newK != k)
    {
      NotificationChain msgs = null;
      if (k != null)
        msgs = ((InternalEObject)k).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AiotMLPackage.MODEL__K, null, msgs);
      if (newK != null)
        msgs = ((InternalEObject)newK).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AiotMLPackage.MODEL__K, null, msgs);
      msgs = basicSetK(newK, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AiotMLPackage.MODEL__K, newK, newK));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public IntegerLiteral getS()
  {
    return s;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetS(IntegerLiteral newS, NotificationChain msgs)
  {
    IntegerLiteral oldS = s;
    s = newS;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AiotMLPackage.MODEL__S, oldS, newS);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setS(IntegerLiteral newS)
  {
    if (newS != s)
    {
      NotificationChain msgs = null;
      if (s != null)
        msgs = ((InternalEObject)s).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AiotMLPackage.MODEL__S, null, msgs);
      if (newS != null)
        msgs = ((InternalEObject)newS).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AiotMLPackage.MODEL__S, null, msgs);
      msgs = basicSetS(newS, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AiotMLPackage.MODEL__S, newS, newS));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<String> getIn()
  {
    if (in == null)
    {
      in = new EDataTypeEList<String>(String.class, this, AiotMLPackage.MODEL__IN);
    }
    return in;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<Layer> getLayers()
  {
    if (layers == null)
    {
      layers = new EObjectContainmentEList<Layer>(Layer.class, this, AiotMLPackage.MODEL__LAYERS);
    }
    return layers;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<String> getOut()
  {
    if (out == null)
    {
      out = new EDataTypeEList<String>(String.class, this, AiotMLPackage.MODEL__OUT);
    }
    return out;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public BooleanLiteral getIfsequ()
  {
    return ifsequ;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetIfsequ(BooleanLiteral newIfsequ, NotificationChain msgs)
  {
    BooleanLiteral oldIfsequ = ifsequ;
    ifsequ = newIfsequ;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AiotMLPackage.MODEL__IFSEQU, oldIfsequ, newIfsequ);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setIfsequ(BooleanLiteral newIfsequ)
  {
    if (newIfsequ != ifsequ)
    {
      NotificationChain msgs = null;
      if (ifsequ != null)
        msgs = ((InternalEObject)ifsequ).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AiotMLPackage.MODEL__IFSEQU, null, msgs);
      if (newIfsequ != null)
        msgs = ((InternalEObject)newIfsequ).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AiotMLPackage.MODEL__IFSEQU, null, msgs);
      msgs = basicSetIfsequ(newIfsequ, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AiotMLPackage.MODEL__IFSEQU, newIfsequ, newIfsequ));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<Sequ> getSequs()
  {
    if (sequs == null)
    {
      sequs = new EObjectContainmentEList<Sequ>(Sequ.class, this, AiotMLPackage.MODEL__SEQUS);
    }
    return sequs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getLoss()
  {
    return loss;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setLoss(String newLoss)
  {
    String oldLoss = loss;
    loss = newLoss;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AiotMLPackage.MODEL__LOSS, oldLoss, loss));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getOptimizer()
  {
    return optimizer;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setOptimizer(String newOptimizer)
  {
    String oldOptimizer = optimizer;
    optimizer = newOptimizer;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AiotMLPackage.MODEL__OPTIMIZER, oldOptimizer, optimizer));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public DoubleLiteral getLr()
  {
    return lr;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetLr(DoubleLiteral newLr, NotificationChain msgs)
  {
    DoubleLiteral oldLr = lr;
    lr = newLr;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AiotMLPackage.MODEL__LR, oldLr, newLr);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setLr(DoubleLiteral newLr)
  {
    if (newLr != lr)
    {
      NotificationChain msgs = null;
      if (lr != null)
        msgs = ((InternalEObject)lr).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AiotMLPackage.MODEL__LR, null, msgs);
      if (newLr != null)
        msgs = ((InternalEObject)newLr).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AiotMLPackage.MODEL__LR, null, msgs);
      msgs = basicSetLr(newLr, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AiotMLPackage.MODEL__LR, newLr, newLr));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getLrID()
  {
    return lrID;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setLrID(String newLrID)
  {
    String oldLrID = lrID;
    lrID = newLrID;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AiotMLPackage.MODEL__LR_ID, oldLrID, lrID));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public DoubleLiteral getMomentum()
  {
    return momentum;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetMomentum(DoubleLiteral newMomentum, NotificationChain msgs)
  {
    DoubleLiteral oldMomentum = momentum;
    momentum = newMomentum;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AiotMLPackage.MODEL__MOMENTUM, oldMomentum, newMomentum);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setMomentum(DoubleLiteral newMomentum)
  {
    if (newMomentum != momentum)
    {
      NotificationChain msgs = null;
      if (momentum != null)
        msgs = ((InternalEObject)momentum).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AiotMLPackage.MODEL__MOMENTUM, null, msgs);
      if (newMomentum != null)
        msgs = ((InternalEObject)newMomentum).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AiotMLPackage.MODEL__MOMENTUM, null, msgs);
      msgs = basicSetMomentum(newMomentum, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AiotMLPackage.MODEL__MOMENTUM, newMomentum, newMomentum));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getMomentumID()
  {
    return momentumID;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setMomentumID(String newMomentumID)
  {
    String oldMomentumID = momentumID;
    momentumID = newMomentumID;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AiotMLPackage.MODEL__MOMENTUM_ID, oldMomentumID, momentumID));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case AiotMLPackage.MODEL__REUSE:
        return basicSetReuse(null, msgs);
      case AiotMLPackage.MODEL__N:
        return basicSetN(null, msgs);
      case AiotMLPackage.MODEL__K:
        return basicSetK(null, msgs);
      case AiotMLPackage.MODEL__S:
        return basicSetS(null, msgs);
      case AiotMLPackage.MODEL__LAYERS:
        return ((InternalEList<?>)getLayers()).basicRemove(otherEnd, msgs);
      case AiotMLPackage.MODEL__IFSEQU:
        return basicSetIfsequ(null, msgs);
      case AiotMLPackage.MODEL__SEQUS:
        return ((InternalEList<?>)getSequs()).basicRemove(otherEnd, msgs);
      case AiotMLPackage.MODEL__LR:
        return basicSetLr(null, msgs);
      case AiotMLPackage.MODEL__MOMENTUM:
        return basicSetMomentum(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case AiotMLPackage.MODEL__SCOPE:
        return getScope();
      case AiotMLPackage.MODEL__REUSE:
        return getReuse();
      case AiotMLPackage.MODEL__N:
        return getN();
      case AiotMLPackage.MODEL__K:
        return getK();
      case AiotMLPackage.MODEL__S:
        return getS();
      case AiotMLPackage.MODEL__IN:
        return getIn();
      case AiotMLPackage.MODEL__LAYERS:
        return getLayers();
      case AiotMLPackage.MODEL__OUT:
        return getOut();
      case AiotMLPackage.MODEL__IFSEQU:
        return getIfsequ();
      case AiotMLPackage.MODEL__SEQUS:
        return getSequs();
      case AiotMLPackage.MODEL__LOSS:
        return getLoss();
      case AiotMLPackage.MODEL__OPTIMIZER:
        return getOptimizer();
      case AiotMLPackage.MODEL__LR:
        return getLr();
      case AiotMLPackage.MODEL__LR_ID:
        return getLrID();
      case AiotMLPackage.MODEL__MOMENTUM:
        return getMomentum();
      case AiotMLPackage.MODEL__MOMENTUM_ID:
        return getMomentumID();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case AiotMLPackage.MODEL__SCOPE:
        setScope((String)newValue);
        return;
      case AiotMLPackage.MODEL__REUSE:
        setReuse((BooleanLiteral)newValue);
        return;
      case AiotMLPackage.MODEL__N:
        setN((IntegerLiteral)newValue);
        return;
      case AiotMLPackage.MODEL__K:
        setK((IntegerLiteral)newValue);
        return;
      case AiotMLPackage.MODEL__S:
        setS((IntegerLiteral)newValue);
        return;
      case AiotMLPackage.MODEL__IN:
        getIn().clear();
        getIn().addAll((Collection<? extends String>)newValue);
        return;
      case AiotMLPackage.MODEL__LAYERS:
        getLayers().clear();
        getLayers().addAll((Collection<? extends Layer>)newValue);
        return;
      case AiotMLPackage.MODEL__OUT:
        getOut().clear();
        getOut().addAll((Collection<? extends String>)newValue);
        return;
      case AiotMLPackage.MODEL__IFSEQU:
        setIfsequ((BooleanLiteral)newValue);
        return;
      case AiotMLPackage.MODEL__SEQUS:
        getSequs().clear();
        getSequs().addAll((Collection<? extends Sequ>)newValue);
        return;
      case AiotMLPackage.MODEL__LOSS:
        setLoss((String)newValue);
        return;
      case AiotMLPackage.MODEL__OPTIMIZER:
        setOptimizer((String)newValue);
        return;
      case AiotMLPackage.MODEL__LR:
        setLr((DoubleLiteral)newValue);
        return;
      case AiotMLPackage.MODEL__LR_ID:
        setLrID((String)newValue);
        return;
      case AiotMLPackage.MODEL__MOMENTUM:
        setMomentum((DoubleLiteral)newValue);
        return;
      case AiotMLPackage.MODEL__MOMENTUM_ID:
        setMomentumID((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case AiotMLPackage.MODEL__SCOPE:
        setScope(SCOPE_EDEFAULT);
        return;
      case AiotMLPackage.MODEL__REUSE:
        setReuse((BooleanLiteral)null);
        return;
      case AiotMLPackage.MODEL__N:
        setN((IntegerLiteral)null);
        return;
      case AiotMLPackage.MODEL__K:
        setK((IntegerLiteral)null);
        return;
      case AiotMLPackage.MODEL__S:
        setS((IntegerLiteral)null);
        return;
      case AiotMLPackage.MODEL__IN:
        getIn().clear();
        return;
      case AiotMLPackage.MODEL__LAYERS:
        getLayers().clear();
        return;
      case AiotMLPackage.MODEL__OUT:
        getOut().clear();
        return;
      case AiotMLPackage.MODEL__IFSEQU:
        setIfsequ((BooleanLiteral)null);
        return;
      case AiotMLPackage.MODEL__SEQUS:
        getSequs().clear();
        return;
      case AiotMLPackage.MODEL__LOSS:
        setLoss(LOSS_EDEFAULT);
        return;
      case AiotMLPackage.MODEL__OPTIMIZER:
        setOptimizer(OPTIMIZER_EDEFAULT);
        return;
      case AiotMLPackage.MODEL__LR:
        setLr((DoubleLiteral)null);
        return;
      case AiotMLPackage.MODEL__LR_ID:
        setLrID(LR_ID_EDEFAULT);
        return;
      case AiotMLPackage.MODEL__MOMENTUM:
        setMomentum((DoubleLiteral)null);
        return;
      case AiotMLPackage.MODEL__MOMENTUM_ID:
        setMomentumID(MOMENTUM_ID_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case AiotMLPackage.MODEL__SCOPE:
        return SCOPE_EDEFAULT == null ? scope != null : !SCOPE_EDEFAULT.equals(scope);
      case AiotMLPackage.MODEL__REUSE:
        return reuse != null;
      case AiotMLPackage.MODEL__N:
        return n != null;
      case AiotMLPackage.MODEL__K:
        return k != null;
      case AiotMLPackage.MODEL__S:
        return s != null;
      case AiotMLPackage.MODEL__IN:
        return in != null && !in.isEmpty();
      case AiotMLPackage.MODEL__LAYERS:
        return layers != null && !layers.isEmpty();
      case AiotMLPackage.MODEL__OUT:
        return out != null && !out.isEmpty();
      case AiotMLPackage.MODEL__IFSEQU:
        return ifsequ != null;
      case AiotMLPackage.MODEL__SEQUS:
        return sequs != null && !sequs.isEmpty();
      case AiotMLPackage.MODEL__LOSS:
        return LOSS_EDEFAULT == null ? loss != null : !LOSS_EDEFAULT.equals(loss);
      case AiotMLPackage.MODEL__OPTIMIZER:
        return OPTIMIZER_EDEFAULT == null ? optimizer != null : !OPTIMIZER_EDEFAULT.equals(optimizer);
      case AiotMLPackage.MODEL__LR:
        return lr != null;
      case AiotMLPackage.MODEL__LR_ID:
        return LR_ID_EDEFAULT == null ? lrID != null : !LR_ID_EDEFAULT.equals(lrID);
      case AiotMLPackage.MODEL__MOMENTUM:
        return momentum != null;
      case AiotMLPackage.MODEL__MOMENTUM_ID:
        return MOMENTUM_ID_EDEFAULT == null ? momentumID != null : !MOMENTUM_ID_EDEFAULT.equals(momentumID);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (scope: ");
    result.append(scope);
    result.append(", in: ");
    result.append(in);
    result.append(", out: ");
    result.append(out);
    result.append(", loss: ");
    result.append(loss);
    result.append(", optimizer: ");
    result.append(optimizer);
    result.append(", lrID: ");
    result.append(lrID);
    result.append(", momentumID: ");
    result.append(momentumID);
    result.append(')');
    return result.toString();
  }

} //ModelImpl
