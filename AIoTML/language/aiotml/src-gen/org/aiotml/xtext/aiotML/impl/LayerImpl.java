/**
 * *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 */
package org.aiotml.xtext.aiotML.impl;

import org.aiotml.xtext.aiotML.AiotMLPackage;
import org.aiotml.xtext.aiotML.Layer;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Layer</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.aiotml.xtext.aiotML.impl.LayerImpl#getLtype <em>Ltype</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.impl.LayerImpl#getNoutputs <em>Noutputs</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.impl.LayerImpl#getKsize <em>Ksize</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.impl.LayerImpl#getStride <em>Stride</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.impl.LayerImpl#getAf <em>Af</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LayerImpl extends MinimalEObjectImpl.Container implements Layer
{
  /**
   * The default value of the '{@link #getLtype() <em>Ltype</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLtype()
   * @generated
   * @ordered
   */
  protected static final String LTYPE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getLtype() <em>Ltype</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLtype()
   * @generated
   * @ordered
   */
  protected String ltype = LTYPE_EDEFAULT;

  /**
   * The default value of the '{@link #getNoutputs() <em>Noutputs</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNoutputs()
   * @generated
   * @ordered
   */
  protected static final String NOUTPUTS_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getNoutputs() <em>Noutputs</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getNoutputs()
   * @generated
   * @ordered
   */
  protected String noutputs = NOUTPUTS_EDEFAULT;

  /**
   * The default value of the '{@link #getKsize() <em>Ksize</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getKsize()
   * @generated
   * @ordered
   */
  protected static final String KSIZE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getKsize() <em>Ksize</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getKsize()
   * @generated
   * @ordered
   */
  protected String ksize = KSIZE_EDEFAULT;

  /**
   * The default value of the '{@link #getStride() <em>Stride</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getStride()
   * @generated
   * @ordered
   */
  protected static final String STRIDE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getStride() <em>Stride</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getStride()
   * @generated
   * @ordered
   */
  protected String stride = STRIDE_EDEFAULT;

  /**
   * The default value of the '{@link #getAf() <em>Af</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAf()
   * @generated
   * @ordered
   */
  protected static final String AF_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getAf() <em>Af</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAf()
   * @generated
   * @ordered
   */
  protected String af = AF_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected LayerImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return AiotMLPackage.Literals.LAYER;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getLtype()
  {
    return ltype;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setLtype(String newLtype)
  {
    String oldLtype = ltype;
    ltype = newLtype;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AiotMLPackage.LAYER__LTYPE, oldLtype, ltype));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getNoutputs()
  {
    return noutputs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setNoutputs(String newNoutputs)
  {
    String oldNoutputs = noutputs;
    noutputs = newNoutputs;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AiotMLPackage.LAYER__NOUTPUTS, oldNoutputs, noutputs));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getKsize()
  {
    return ksize;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setKsize(String newKsize)
  {
    String oldKsize = ksize;
    ksize = newKsize;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AiotMLPackage.LAYER__KSIZE, oldKsize, ksize));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getStride()
  {
    return stride;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setStride(String newStride)
  {
    String oldStride = stride;
    stride = newStride;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AiotMLPackage.LAYER__STRIDE, oldStride, stride));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getAf()
  {
    return af;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setAf(String newAf)
  {
    String oldAf = af;
    af = newAf;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AiotMLPackage.LAYER__AF, oldAf, af));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case AiotMLPackage.LAYER__LTYPE:
        return getLtype();
      case AiotMLPackage.LAYER__NOUTPUTS:
        return getNoutputs();
      case AiotMLPackage.LAYER__KSIZE:
        return getKsize();
      case AiotMLPackage.LAYER__STRIDE:
        return getStride();
      case AiotMLPackage.LAYER__AF:
        return getAf();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case AiotMLPackage.LAYER__LTYPE:
        setLtype((String)newValue);
        return;
      case AiotMLPackage.LAYER__NOUTPUTS:
        setNoutputs((String)newValue);
        return;
      case AiotMLPackage.LAYER__KSIZE:
        setKsize((String)newValue);
        return;
      case AiotMLPackage.LAYER__STRIDE:
        setStride((String)newValue);
        return;
      case AiotMLPackage.LAYER__AF:
        setAf((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case AiotMLPackage.LAYER__LTYPE:
        setLtype(LTYPE_EDEFAULT);
        return;
      case AiotMLPackage.LAYER__NOUTPUTS:
        setNoutputs(NOUTPUTS_EDEFAULT);
        return;
      case AiotMLPackage.LAYER__KSIZE:
        setKsize(KSIZE_EDEFAULT);
        return;
      case AiotMLPackage.LAYER__STRIDE:
        setStride(STRIDE_EDEFAULT);
        return;
      case AiotMLPackage.LAYER__AF:
        setAf(AF_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case AiotMLPackage.LAYER__LTYPE:
        return LTYPE_EDEFAULT == null ? ltype != null : !LTYPE_EDEFAULT.equals(ltype);
      case AiotMLPackage.LAYER__NOUTPUTS:
        return NOUTPUTS_EDEFAULT == null ? noutputs != null : !NOUTPUTS_EDEFAULT.equals(noutputs);
      case AiotMLPackage.LAYER__KSIZE:
        return KSIZE_EDEFAULT == null ? ksize != null : !KSIZE_EDEFAULT.equals(ksize);
      case AiotMLPackage.LAYER__STRIDE:
        return STRIDE_EDEFAULT == null ? stride != null : !STRIDE_EDEFAULT.equals(stride);
      case AiotMLPackage.LAYER__AF:
        return AF_EDEFAULT == null ? af != null : !AF_EDEFAULT.equals(af);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (ltype: ");
    result.append(ltype);
    result.append(", noutputs: ");
    result.append(noutputs);
    result.append(", ksize: ");
    result.append(ksize);
    result.append(", stride: ");
    result.append(stride);
    result.append(", af: ");
    result.append(af);
    result.append(')');
    return result.toString();
  }

} //LayerImpl
