/**
 * *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 */
package org.aiotml.xtext.aiotML.impl;

import java.util.Collection;

import org.aiotml.xtext.aiotML.AiotMLPackage;
import org.aiotml.xtext.aiotML.ClassFunctionCall;
import org.aiotml.xtext.aiotML.Variable;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Class Function Call</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.aiotml.xtext.aiotML.impl.ClassFunctionCallImpl#getProp <em>Prop</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.impl.ClassFunctionCallImpl#getElements <em>Elements</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.impl.ClassFunctionCallImpl#getFname <em>Fname</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.impl.ClassFunctionCallImpl#getP <em>P</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.impl.ClassFunctionCallImpl#getPe <em>Pe</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ClassFunctionCallImpl extends ActionImpl implements ClassFunctionCall
{
  /**
   * The cached value of the '{@link #getProp() <em>Prop</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getProp()
   * @generated
   * @ordered
   */
  protected Variable prop;

  /**
   * The cached value of the '{@link #getElements() <em>Elements</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getElements()
   * @generated
   * @ordered
   */
  protected EList<Variable> elements;

  /**
   * The default value of the '{@link #getFname() <em>Fname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFname()
   * @generated
   * @ordered
   */
  protected static final String FNAME_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getFname() <em>Fname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFname()
   * @generated
   * @ordered
   */
  protected String fname = FNAME_EDEFAULT;

  /**
   * The cached value of the '{@link #getP() <em>P</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getP()
   * @generated
   * @ordered
   */
  protected EList<Variable> p;

  /**
   * The cached value of the '{@link #getPe() <em>Pe</em>}' reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPe()
   * @generated
   * @ordered
   */
  protected EList<Variable> pe;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ClassFunctionCallImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return AiotMLPackage.Literals.CLASS_FUNCTION_CALL;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Variable getProp()
  {
    if (prop != null && prop.eIsProxy())
    {
      InternalEObject oldProp = (InternalEObject)prop;
      prop = (Variable)eResolveProxy(oldProp);
      if (prop != oldProp)
      {
        if (eNotificationRequired())
          eNotify(new ENotificationImpl(this, Notification.RESOLVE, AiotMLPackage.CLASS_FUNCTION_CALL__PROP, oldProp, prop));
      }
    }
    return prop;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Variable basicGetProp()
  {
    return prop;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setProp(Variable newProp)
  {
    Variable oldProp = prop;
    prop = newProp;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AiotMLPackage.CLASS_FUNCTION_CALL__PROP, oldProp, prop));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<Variable> getElements()
  {
    if (elements == null)
    {
      elements = new EObjectResolvingEList<Variable>(Variable.class, this, AiotMLPackage.CLASS_FUNCTION_CALL__ELEMENTS);
    }
    return elements;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getFname()
  {
    return fname;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setFname(String newFname)
  {
    String oldFname = fname;
    fname = newFname;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AiotMLPackage.CLASS_FUNCTION_CALL__FNAME, oldFname, fname));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<Variable> getP()
  {
    if (p == null)
    {
      p = new EObjectResolvingEList<Variable>(Variable.class, this, AiotMLPackage.CLASS_FUNCTION_CALL__P);
    }
    return p;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EList<Variable> getPe()
  {
    if (pe == null)
    {
      pe = new EObjectResolvingEList<Variable>(Variable.class, this, AiotMLPackage.CLASS_FUNCTION_CALL__PE);
    }
    return pe;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case AiotMLPackage.CLASS_FUNCTION_CALL__PROP:
        if (resolve) return getProp();
        return basicGetProp();
      case AiotMLPackage.CLASS_FUNCTION_CALL__ELEMENTS:
        return getElements();
      case AiotMLPackage.CLASS_FUNCTION_CALL__FNAME:
        return getFname();
      case AiotMLPackage.CLASS_FUNCTION_CALL__P:
        return getP();
      case AiotMLPackage.CLASS_FUNCTION_CALL__PE:
        return getPe();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case AiotMLPackage.CLASS_FUNCTION_CALL__PROP:
        setProp((Variable)newValue);
        return;
      case AiotMLPackage.CLASS_FUNCTION_CALL__ELEMENTS:
        getElements().clear();
        getElements().addAll((Collection<? extends Variable>)newValue);
        return;
      case AiotMLPackage.CLASS_FUNCTION_CALL__FNAME:
        setFname((String)newValue);
        return;
      case AiotMLPackage.CLASS_FUNCTION_CALL__P:
        getP().clear();
        getP().addAll((Collection<? extends Variable>)newValue);
        return;
      case AiotMLPackage.CLASS_FUNCTION_CALL__PE:
        getPe().clear();
        getPe().addAll((Collection<? extends Variable>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case AiotMLPackage.CLASS_FUNCTION_CALL__PROP:
        setProp((Variable)null);
        return;
      case AiotMLPackage.CLASS_FUNCTION_CALL__ELEMENTS:
        getElements().clear();
        return;
      case AiotMLPackage.CLASS_FUNCTION_CALL__FNAME:
        setFname(FNAME_EDEFAULT);
        return;
      case AiotMLPackage.CLASS_FUNCTION_CALL__P:
        getP().clear();
        return;
      case AiotMLPackage.CLASS_FUNCTION_CALL__PE:
        getPe().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case AiotMLPackage.CLASS_FUNCTION_CALL__PROP:
        return prop != null;
      case AiotMLPackage.CLASS_FUNCTION_CALL__ELEMENTS:
        return elements != null && !elements.isEmpty();
      case AiotMLPackage.CLASS_FUNCTION_CALL__FNAME:
        return FNAME_EDEFAULT == null ? fname != null : !FNAME_EDEFAULT.equals(fname);
      case AiotMLPackage.CLASS_FUNCTION_CALL__P:
        return p != null && !p.isEmpty();
      case AiotMLPackage.CLASS_FUNCTION_CALL__PE:
        return pe != null && !pe.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (fname: ");
    result.append(fname);
    result.append(')');
    return result.toString();
  }

} //ClassFunctionCallImpl
