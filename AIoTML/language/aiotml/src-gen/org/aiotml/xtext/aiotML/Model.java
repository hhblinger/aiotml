/**
 * *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 */
package org.aiotml.xtext.aiotML;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.aiotml.xtext.aiotML.Model#getScope <em>Scope</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.Model#getReuse <em>Reuse</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.Model#getN <em>N</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.Model#getK <em>K</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.Model#getS <em>S</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.Model#getIn <em>In</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.Model#getLayers <em>Layers</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.Model#getOut <em>Out</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.Model#getIfsequ <em>Ifsequ</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.Model#getSequs <em>Sequs</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.Model#getLoss <em>Loss</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.Model#getOptimizer <em>Optimizer</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.Model#getLr <em>Lr</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.Model#getLrID <em>Lr ID</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.Model#getMomentum <em>Momentum</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.Model#getMomentumID <em>Momentum ID</em>}</li>
 * </ul>
 *
 * @see org.aiotml.xtext.aiotML.AiotMLPackage#getModel()
 * @model
 * @generated
 */
public interface Model extends Type
{
  /**
   * Returns the value of the '<em><b>Scope</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Scope</em>' attribute.
   * @see #setScope(String)
   * @see org.aiotml.xtext.aiotML.AiotMLPackage#getModel_Scope()
   * @model
   * @generated
   */
  String getScope();

  /**
   * Sets the value of the '{@link org.aiotml.xtext.aiotML.Model#getScope <em>Scope</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Scope</em>' attribute.
   * @see #getScope()
   * @generated
   */
  void setScope(String value);

  /**
   * Returns the value of the '<em><b>Reuse</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Reuse</em>' containment reference.
   * @see #setReuse(BooleanLiteral)
   * @see org.aiotml.xtext.aiotML.AiotMLPackage#getModel_Reuse()
   * @model containment="true"
   * @generated
   */
  BooleanLiteral getReuse();

  /**
   * Sets the value of the '{@link org.aiotml.xtext.aiotML.Model#getReuse <em>Reuse</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Reuse</em>' containment reference.
   * @see #getReuse()
   * @generated
   */
  void setReuse(BooleanLiteral value);

  /**
   * Returns the value of the '<em><b>N</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>N</em>' containment reference.
   * @see #setN(IntegerLiteral)
   * @see org.aiotml.xtext.aiotML.AiotMLPackage#getModel_N()
   * @model containment="true"
   * @generated
   */
  IntegerLiteral getN();

  /**
   * Sets the value of the '{@link org.aiotml.xtext.aiotML.Model#getN <em>N</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>N</em>' containment reference.
   * @see #getN()
   * @generated
   */
  void setN(IntegerLiteral value);

  /**
   * Returns the value of the '<em><b>K</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>K</em>' containment reference.
   * @see #setK(IntegerLiteral)
   * @see org.aiotml.xtext.aiotML.AiotMLPackage#getModel_K()
   * @model containment="true"
   * @generated
   */
  IntegerLiteral getK();

  /**
   * Sets the value of the '{@link org.aiotml.xtext.aiotML.Model#getK <em>K</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>K</em>' containment reference.
   * @see #getK()
   * @generated
   */
  void setK(IntegerLiteral value);

  /**
   * Returns the value of the '<em><b>S</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>S</em>' containment reference.
   * @see #setS(IntegerLiteral)
   * @see org.aiotml.xtext.aiotML.AiotMLPackage#getModel_S()
   * @model containment="true"
   * @generated
   */
  IntegerLiteral getS();

  /**
   * Sets the value of the '{@link org.aiotml.xtext.aiotML.Model#getS <em>S</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>S</em>' containment reference.
   * @see #getS()
   * @generated
   */
  void setS(IntegerLiteral value);

  /**
   * Returns the value of the '<em><b>In</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>In</em>' attribute list.
   * @see org.aiotml.xtext.aiotML.AiotMLPackage#getModel_In()
   * @model unique="false"
   * @generated
   */
  EList<String> getIn();

  /**
   * Returns the value of the '<em><b>Layers</b></em>' containment reference list.
   * The list contents are of type {@link org.aiotml.xtext.aiotML.Layer}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Layers</em>' containment reference list.
   * @see org.aiotml.xtext.aiotML.AiotMLPackage#getModel_Layers()
   * @model containment="true"
   * @generated
   */
  EList<Layer> getLayers();

  /**
   * Returns the value of the '<em><b>Out</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Out</em>' attribute list.
   * @see org.aiotml.xtext.aiotML.AiotMLPackage#getModel_Out()
   * @model unique="false"
   * @generated
   */
  EList<String> getOut();

  /**
   * Returns the value of the '<em><b>Ifsequ</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ifsequ</em>' containment reference.
   * @see #setIfsequ(BooleanLiteral)
   * @see org.aiotml.xtext.aiotML.AiotMLPackage#getModel_Ifsequ()
   * @model containment="true"
   * @generated
   */
  BooleanLiteral getIfsequ();

  /**
   * Sets the value of the '{@link org.aiotml.xtext.aiotML.Model#getIfsequ <em>Ifsequ</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ifsequ</em>' containment reference.
   * @see #getIfsequ()
   * @generated
   */
  void setIfsequ(BooleanLiteral value);

  /**
   * Returns the value of the '<em><b>Sequs</b></em>' containment reference list.
   * The list contents are of type {@link org.aiotml.xtext.aiotML.Sequ}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Sequs</em>' containment reference list.
   * @see org.aiotml.xtext.aiotML.AiotMLPackage#getModel_Sequs()
   * @model containment="true"
   * @generated
   */
  EList<Sequ> getSequs();

  /**
   * Returns the value of the '<em><b>Loss</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Loss</em>' attribute.
   * @see #setLoss(String)
   * @see org.aiotml.xtext.aiotML.AiotMLPackage#getModel_Loss()
   * @model
   * @generated
   */
  String getLoss();

  /**
   * Sets the value of the '{@link org.aiotml.xtext.aiotML.Model#getLoss <em>Loss</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Loss</em>' attribute.
   * @see #getLoss()
   * @generated
   */
  void setLoss(String value);

  /**
   * Returns the value of the '<em><b>Optimizer</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Optimizer</em>' attribute.
   * @see #setOptimizer(String)
   * @see org.aiotml.xtext.aiotML.AiotMLPackage#getModel_Optimizer()
   * @model
   * @generated
   */
  String getOptimizer();

  /**
   * Sets the value of the '{@link org.aiotml.xtext.aiotML.Model#getOptimizer <em>Optimizer</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Optimizer</em>' attribute.
   * @see #getOptimizer()
   * @generated
   */
  void setOptimizer(String value);

  /**
   * Returns the value of the '<em><b>Lr</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Lr</em>' containment reference.
   * @see #setLr(DoubleLiteral)
   * @see org.aiotml.xtext.aiotML.AiotMLPackage#getModel_Lr()
   * @model containment="true"
   * @generated
   */
  DoubleLiteral getLr();

  /**
   * Sets the value of the '{@link org.aiotml.xtext.aiotML.Model#getLr <em>Lr</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Lr</em>' containment reference.
   * @see #getLr()
   * @generated
   */
  void setLr(DoubleLiteral value);

  /**
   * Returns the value of the '<em><b>Lr ID</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Lr ID</em>' attribute.
   * @see #setLrID(String)
   * @see org.aiotml.xtext.aiotML.AiotMLPackage#getModel_LrID()
   * @model
   * @generated
   */
  String getLrID();

  /**
   * Sets the value of the '{@link org.aiotml.xtext.aiotML.Model#getLrID <em>Lr ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Lr ID</em>' attribute.
   * @see #getLrID()
   * @generated
   */
  void setLrID(String value);

  /**
   * Returns the value of the '<em><b>Momentum</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Momentum</em>' containment reference.
   * @see #setMomentum(DoubleLiteral)
   * @see org.aiotml.xtext.aiotML.AiotMLPackage#getModel_Momentum()
   * @model containment="true"
   * @generated
   */
  DoubleLiteral getMomentum();

  /**
   * Sets the value of the '{@link org.aiotml.xtext.aiotML.Model#getMomentum <em>Momentum</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Momentum</em>' containment reference.
   * @see #getMomentum()
   * @generated
   */
  void setMomentum(DoubleLiteral value);

  /**
   * Returns the value of the '<em><b>Momentum ID</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Momentum ID</em>' attribute.
   * @see #setMomentumID(String)
   * @see org.aiotml.xtext.aiotML.AiotMLPackage#getModel_MomentumID()
   * @model
   * @generated
   */
  String getMomentumID();

  /**
   * Sets the value of the '{@link org.aiotml.xtext.aiotML.Model#getMomentumID <em>Momentum ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Momentum ID</em>' attribute.
   * @see #getMomentumID()
   * @generated
   */
  void setMomentumID(String value);

} // Model
