/**
 * *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 */
package org.aiotml.xtext.aiotML;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Layer</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.aiotml.xtext.aiotML.Layer#getLtype <em>Ltype</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.Layer#getNoutputs <em>Noutputs</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.Layer#getKsize <em>Ksize</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.Layer#getStride <em>Stride</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.Layer#getAf <em>Af</em>}</li>
 * </ul>
 *
 * @see org.aiotml.xtext.aiotML.AiotMLPackage#getLayer()
 * @model
 * @generated
 */
public interface Layer extends EObject
{
  /**
   * Returns the value of the '<em><b>Ltype</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ltype</em>' attribute.
   * @see #setLtype(String)
   * @see org.aiotml.xtext.aiotML.AiotMLPackage#getLayer_Ltype()
   * @model
   * @generated
   */
  String getLtype();

  /**
   * Sets the value of the '{@link org.aiotml.xtext.aiotML.Layer#getLtype <em>Ltype</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ltype</em>' attribute.
   * @see #getLtype()
   * @generated
   */
  void setLtype(String value);

  /**
   * Returns the value of the '<em><b>Noutputs</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Noutputs</em>' attribute.
   * @see #setNoutputs(String)
   * @see org.aiotml.xtext.aiotML.AiotMLPackage#getLayer_Noutputs()
   * @model
   * @generated
   */
  String getNoutputs();

  /**
   * Sets the value of the '{@link org.aiotml.xtext.aiotML.Layer#getNoutputs <em>Noutputs</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Noutputs</em>' attribute.
   * @see #getNoutputs()
   * @generated
   */
  void setNoutputs(String value);

  /**
   * Returns the value of the '<em><b>Ksize</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ksize</em>' attribute.
   * @see #setKsize(String)
   * @see org.aiotml.xtext.aiotML.AiotMLPackage#getLayer_Ksize()
   * @model
   * @generated
   */
  String getKsize();

  /**
   * Sets the value of the '{@link org.aiotml.xtext.aiotML.Layer#getKsize <em>Ksize</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ksize</em>' attribute.
   * @see #getKsize()
   * @generated
   */
  void setKsize(String value);

  /**
   * Returns the value of the '<em><b>Stride</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Stride</em>' attribute.
   * @see #setStride(String)
   * @see org.aiotml.xtext.aiotML.AiotMLPackage#getLayer_Stride()
   * @model
   * @generated
   */
  String getStride();

  /**
   * Sets the value of the '{@link org.aiotml.xtext.aiotML.Layer#getStride <em>Stride</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Stride</em>' attribute.
   * @see #getStride()
   * @generated
   */
  void setStride(String value);

  /**
   * Returns the value of the '<em><b>Af</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Af</em>' attribute.
   * @see #setAf(String)
   * @see org.aiotml.xtext.aiotML.AiotMLPackage#getLayer_Af()
   * @model
   * @generated
   */
  String getAf();

  /**
   * Sets the value of the '{@link org.aiotml.xtext.aiotML.Layer#getAf <em>Af</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Af</em>' attribute.
   * @see #getAf()
   * @generated
   */
  void setAf(String value);

} // Layer
