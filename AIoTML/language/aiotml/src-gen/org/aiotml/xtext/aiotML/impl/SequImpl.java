/**
 * *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 */
package org.aiotml.xtext.aiotML.impl;

import org.aiotml.xtext.aiotML.AiotMLPackage;
import org.aiotml.xtext.aiotML.DoubleLiteral;
import org.aiotml.xtext.aiotML.IntegerLiteral;
import org.aiotml.xtext.aiotML.Sequ;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sequ</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link org.aiotml.xtext.aiotML.impl.SequImpl#getLtype <em>Ltype</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.impl.SequImpl#getDrate <em>Drate</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.impl.SequImpl#getDrateID <em>Drate ID</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.impl.SequImpl#getUnits <em>Units</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.impl.SequImpl#getUnitsID <em>Units ID</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.impl.SequImpl#getIdim <em>Idim</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.impl.SequImpl#getAf <em>Af</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SequImpl extends MinimalEObjectImpl.Container implements Sequ
{
  /**
   * The default value of the '{@link #getLtype() <em>Ltype</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLtype()
   * @generated
   * @ordered
   */
  protected static final String LTYPE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getLtype() <em>Ltype</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLtype()
   * @generated
   * @ordered
   */
  protected String ltype = LTYPE_EDEFAULT;

  /**
   * The cached value of the '{@link #getDrate() <em>Drate</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDrate()
   * @generated
   * @ordered
   */
  protected DoubleLiteral drate;

  /**
   * The default value of the '{@link #getDrateID() <em>Drate ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDrateID()
   * @generated
   * @ordered
   */
  protected static final String DRATE_ID_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getDrateID() <em>Drate ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getDrateID()
   * @generated
   * @ordered
   */
  protected String drateID = DRATE_ID_EDEFAULT;

  /**
   * The cached value of the '{@link #getUnits() <em>Units</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUnits()
   * @generated
   * @ordered
   */
  protected IntegerLiteral units;

  /**
   * The default value of the '{@link #getUnitsID() <em>Units ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUnitsID()
   * @generated
   * @ordered
   */
  protected static final String UNITS_ID_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getUnitsID() <em>Units ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUnitsID()
   * @generated
   * @ordered
   */
  protected String unitsID = UNITS_ID_EDEFAULT;

  /**
   * The default value of the '{@link #getIdim() <em>Idim</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIdim()
   * @generated
   * @ordered
   */
  protected static final String IDIM_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getIdim() <em>Idim</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getIdim()
   * @generated
   * @ordered
   */
  protected String idim = IDIM_EDEFAULT;

  /**
   * The default value of the '{@link #getAf() <em>Af</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAf()
   * @generated
   * @ordered
   */
  protected static final String AF_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getAf() <em>Af</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAf()
   * @generated
   * @ordered
   */
  protected String af = AF_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SequImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return AiotMLPackage.Literals.SEQU;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getLtype()
  {
    return ltype;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setLtype(String newLtype)
  {
    String oldLtype = ltype;
    ltype = newLtype;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AiotMLPackage.SEQU__LTYPE, oldLtype, ltype));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public DoubleLiteral getDrate()
  {
    return drate;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetDrate(DoubleLiteral newDrate, NotificationChain msgs)
  {
    DoubleLiteral oldDrate = drate;
    drate = newDrate;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AiotMLPackage.SEQU__DRATE, oldDrate, newDrate);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setDrate(DoubleLiteral newDrate)
  {
    if (newDrate != drate)
    {
      NotificationChain msgs = null;
      if (drate != null)
        msgs = ((InternalEObject)drate).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AiotMLPackage.SEQU__DRATE, null, msgs);
      if (newDrate != null)
        msgs = ((InternalEObject)newDrate).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AiotMLPackage.SEQU__DRATE, null, msgs);
      msgs = basicSetDrate(newDrate, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AiotMLPackage.SEQU__DRATE, newDrate, newDrate));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getDrateID()
  {
    return drateID;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setDrateID(String newDrateID)
  {
    String oldDrateID = drateID;
    drateID = newDrateID;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AiotMLPackage.SEQU__DRATE_ID, oldDrateID, drateID));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public IntegerLiteral getUnits()
  {
    return units;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetUnits(IntegerLiteral newUnits, NotificationChain msgs)
  {
    IntegerLiteral oldUnits = units;
    units = newUnits;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, AiotMLPackage.SEQU__UNITS, oldUnits, newUnits);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setUnits(IntegerLiteral newUnits)
  {
    if (newUnits != units)
    {
      NotificationChain msgs = null;
      if (units != null)
        msgs = ((InternalEObject)units).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - AiotMLPackage.SEQU__UNITS, null, msgs);
      if (newUnits != null)
        msgs = ((InternalEObject)newUnits).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - AiotMLPackage.SEQU__UNITS, null, msgs);
      msgs = basicSetUnits(newUnits, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AiotMLPackage.SEQU__UNITS, newUnits, newUnits));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getUnitsID()
  {
    return unitsID;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setUnitsID(String newUnitsID)
  {
    String oldUnitsID = unitsID;
    unitsID = newUnitsID;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AiotMLPackage.SEQU__UNITS_ID, oldUnitsID, unitsID));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getIdim()
  {
    return idim;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setIdim(String newIdim)
  {
    String oldIdim = idim;
    idim = newIdim;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AiotMLPackage.SEQU__IDIM, oldIdim, idim));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String getAf()
  {
    return af;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void setAf(String newAf)
  {
    String oldAf = af;
    af = newAf;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, AiotMLPackage.SEQU__AF, oldAf, af));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case AiotMLPackage.SEQU__DRATE:
        return basicSetDrate(null, msgs);
      case AiotMLPackage.SEQU__UNITS:
        return basicSetUnits(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case AiotMLPackage.SEQU__LTYPE:
        return getLtype();
      case AiotMLPackage.SEQU__DRATE:
        return getDrate();
      case AiotMLPackage.SEQU__DRATE_ID:
        return getDrateID();
      case AiotMLPackage.SEQU__UNITS:
        return getUnits();
      case AiotMLPackage.SEQU__UNITS_ID:
        return getUnitsID();
      case AiotMLPackage.SEQU__IDIM:
        return getIdim();
      case AiotMLPackage.SEQU__AF:
        return getAf();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case AiotMLPackage.SEQU__LTYPE:
        setLtype((String)newValue);
        return;
      case AiotMLPackage.SEQU__DRATE:
        setDrate((DoubleLiteral)newValue);
        return;
      case AiotMLPackage.SEQU__DRATE_ID:
        setDrateID((String)newValue);
        return;
      case AiotMLPackage.SEQU__UNITS:
        setUnits((IntegerLiteral)newValue);
        return;
      case AiotMLPackage.SEQU__UNITS_ID:
        setUnitsID((String)newValue);
        return;
      case AiotMLPackage.SEQU__IDIM:
        setIdim((String)newValue);
        return;
      case AiotMLPackage.SEQU__AF:
        setAf((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case AiotMLPackage.SEQU__LTYPE:
        setLtype(LTYPE_EDEFAULT);
        return;
      case AiotMLPackage.SEQU__DRATE:
        setDrate((DoubleLiteral)null);
        return;
      case AiotMLPackage.SEQU__DRATE_ID:
        setDrateID(DRATE_ID_EDEFAULT);
        return;
      case AiotMLPackage.SEQU__UNITS:
        setUnits((IntegerLiteral)null);
        return;
      case AiotMLPackage.SEQU__UNITS_ID:
        setUnitsID(UNITS_ID_EDEFAULT);
        return;
      case AiotMLPackage.SEQU__IDIM:
        setIdim(IDIM_EDEFAULT);
        return;
      case AiotMLPackage.SEQU__AF:
        setAf(AF_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case AiotMLPackage.SEQU__LTYPE:
        return LTYPE_EDEFAULT == null ? ltype != null : !LTYPE_EDEFAULT.equals(ltype);
      case AiotMLPackage.SEQU__DRATE:
        return drate != null;
      case AiotMLPackage.SEQU__DRATE_ID:
        return DRATE_ID_EDEFAULT == null ? drateID != null : !DRATE_ID_EDEFAULT.equals(drateID);
      case AiotMLPackage.SEQU__UNITS:
        return units != null;
      case AiotMLPackage.SEQU__UNITS_ID:
        return UNITS_ID_EDEFAULT == null ? unitsID != null : !UNITS_ID_EDEFAULT.equals(unitsID);
      case AiotMLPackage.SEQU__IDIM:
        return IDIM_EDEFAULT == null ? idim != null : !IDIM_EDEFAULT.equals(idim);
      case AiotMLPackage.SEQU__AF:
        return AF_EDEFAULT == null ? af != null : !AF_EDEFAULT.equals(af);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuilder result = new StringBuilder(super.toString());
    result.append(" (ltype: ");
    result.append(ltype);
    result.append(", drateID: ");
    result.append(drateID);
    result.append(", unitsID: ");
    result.append(unitsID);
    result.append(", idim: ");
    result.append(idim);
    result.append(", af: ");
    result.append(af);
    result.append(')');
    return result.toString();
  }

} //SequImpl
