/**
 * *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 */
package org.aiotml.xtext.aiotML;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Class Function Call</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.aiotml.xtext.aiotML.ClassFunctionCall#getProp <em>Prop</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.ClassFunctionCall#getElements <em>Elements</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.ClassFunctionCall#getFname <em>Fname</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.ClassFunctionCall#getP <em>P</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.ClassFunctionCall#getPe <em>Pe</em>}</li>
 * </ul>
 *
 * @see org.aiotml.xtext.aiotML.AiotMLPackage#getClassFunctionCall()
 * @model
 * @generated
 */
public interface ClassFunctionCall extends Action
{
  /**
   * Returns the value of the '<em><b>Prop</b></em>' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Prop</em>' reference.
   * @see #setProp(Variable)
   * @see org.aiotml.xtext.aiotML.AiotMLPackage#getClassFunctionCall_Prop()
   * @model
   * @generated
   */
  Variable getProp();

  /**
   * Sets the value of the '{@link org.aiotml.xtext.aiotML.ClassFunctionCall#getProp <em>Prop</em>}' reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Prop</em>' reference.
   * @see #getProp()
   * @generated
   */
  void setProp(Variable value);

  /**
   * Returns the value of the '<em><b>Elements</b></em>' reference list.
   * The list contents are of type {@link org.aiotml.xtext.aiotML.Variable}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Elements</em>' reference list.
   * @see org.aiotml.xtext.aiotML.AiotMLPackage#getClassFunctionCall_Elements()
   * @model
   * @generated
   */
  EList<Variable> getElements();

  /**
   * Returns the value of the '<em><b>Fname</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Fname</em>' attribute.
   * @see #setFname(String)
   * @see org.aiotml.xtext.aiotML.AiotMLPackage#getClassFunctionCall_Fname()
   * @model
   * @generated
   */
  String getFname();

  /**
   * Sets the value of the '{@link org.aiotml.xtext.aiotML.ClassFunctionCall#getFname <em>Fname</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Fname</em>' attribute.
   * @see #getFname()
   * @generated
   */
  void setFname(String value);

  /**
   * Returns the value of the '<em><b>P</b></em>' reference list.
   * The list contents are of type {@link org.aiotml.xtext.aiotML.Variable}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>P</em>' reference list.
   * @see org.aiotml.xtext.aiotML.AiotMLPackage#getClassFunctionCall_P()
   * @model
   * @generated
   */
  EList<Variable> getP();

  /**
   * Returns the value of the '<em><b>Pe</b></em>' reference list.
   * The list contents are of type {@link org.aiotml.xtext.aiotML.Variable}.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Pe</em>' reference list.
   * @see org.aiotml.xtext.aiotML.AiotMLPackage#getClassFunctionCall_Pe()
   * @model
   * @generated
   */
  EList<Variable> getPe();

} // ClassFunctionCall
