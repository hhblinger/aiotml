/**
 * *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *  *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *  *
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 */
package org.aiotml.xtext.aiotML;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sequ</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link org.aiotml.xtext.aiotML.Sequ#getLtype <em>Ltype</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.Sequ#getDrate <em>Drate</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.Sequ#getDrateID <em>Drate ID</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.Sequ#getUnits <em>Units</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.Sequ#getUnitsID <em>Units ID</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.Sequ#getIdim <em>Idim</em>}</li>
 *   <li>{@link org.aiotml.xtext.aiotML.Sequ#getAf <em>Af</em>}</li>
 * </ul>
 *
 * @see org.aiotml.xtext.aiotML.AiotMLPackage#getSequ()
 * @model
 * @generated
 */
public interface Sequ extends EObject
{
  /**
   * Returns the value of the '<em><b>Ltype</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Ltype</em>' attribute.
   * @see #setLtype(String)
   * @see org.aiotml.xtext.aiotML.AiotMLPackage#getSequ_Ltype()
   * @model
   * @generated
   */
  String getLtype();

  /**
   * Sets the value of the '{@link org.aiotml.xtext.aiotML.Sequ#getLtype <em>Ltype</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Ltype</em>' attribute.
   * @see #getLtype()
   * @generated
   */
  void setLtype(String value);

  /**
   * Returns the value of the '<em><b>Drate</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Drate</em>' containment reference.
   * @see #setDrate(DoubleLiteral)
   * @see org.aiotml.xtext.aiotML.AiotMLPackage#getSequ_Drate()
   * @model containment="true"
   * @generated
   */
  DoubleLiteral getDrate();

  /**
   * Sets the value of the '{@link org.aiotml.xtext.aiotML.Sequ#getDrate <em>Drate</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Drate</em>' containment reference.
   * @see #getDrate()
   * @generated
   */
  void setDrate(DoubleLiteral value);

  /**
   * Returns the value of the '<em><b>Drate ID</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Drate ID</em>' attribute.
   * @see #setDrateID(String)
   * @see org.aiotml.xtext.aiotML.AiotMLPackage#getSequ_DrateID()
   * @model
   * @generated
   */
  String getDrateID();

  /**
   * Sets the value of the '{@link org.aiotml.xtext.aiotML.Sequ#getDrateID <em>Drate ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Drate ID</em>' attribute.
   * @see #getDrateID()
   * @generated
   */
  void setDrateID(String value);

  /**
   * Returns the value of the '<em><b>Units</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Units</em>' containment reference.
   * @see #setUnits(IntegerLiteral)
   * @see org.aiotml.xtext.aiotML.AiotMLPackage#getSequ_Units()
   * @model containment="true"
   * @generated
   */
  IntegerLiteral getUnits();

  /**
   * Sets the value of the '{@link org.aiotml.xtext.aiotML.Sequ#getUnits <em>Units</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Units</em>' containment reference.
   * @see #getUnits()
   * @generated
   */
  void setUnits(IntegerLiteral value);

  /**
   * Returns the value of the '<em><b>Units ID</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Units ID</em>' attribute.
   * @see #setUnitsID(String)
   * @see org.aiotml.xtext.aiotML.AiotMLPackage#getSequ_UnitsID()
   * @model
   * @generated
   */
  String getUnitsID();

  /**
   * Sets the value of the '{@link org.aiotml.xtext.aiotML.Sequ#getUnitsID <em>Units ID</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Units ID</em>' attribute.
   * @see #getUnitsID()
   * @generated
   */
  void setUnitsID(String value);

  /**
   * Returns the value of the '<em><b>Idim</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Idim</em>' attribute.
   * @see #setIdim(String)
   * @see org.aiotml.xtext.aiotML.AiotMLPackage#getSequ_Idim()
   * @model
   * @generated
   */
  String getIdim();

  /**
   * Sets the value of the '{@link org.aiotml.xtext.aiotML.Sequ#getIdim <em>Idim</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Idim</em>' attribute.
   * @see #getIdim()
   * @generated
   */
  void setIdim(String value);

  /**
   * Returns the value of the '<em><b>Af</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the value of the '<em>Af</em>' attribute.
   * @see #setAf(String)
   * @see org.aiotml.xtext.aiotML.AiotMLPackage#getSequ_Af()
   * @model
   * @generated
   */
  String getAf();

  /**
   * Sets the value of the '{@link org.aiotml.xtext.aiotML.Sequ#getAf <em>Af</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Af</em>' attribute.
   * @see #getAf()
   * @generated
   */
  void setAf(String value);

} // Sequ
