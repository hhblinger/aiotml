from datetime import datetime
from tqdm import tqdm
import pandas as pd
import paho.mqtt.client as mqtt
from copy import deepcopy
import random
from collections import deque
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout
from tensorflow.keras.optimizers import Adam
import os

from cityflow_env import CityFlowEnvM
from utility import *

os.environ["CUDA_VISIBLE_DEVICES"] = "-1"  # use GPU


class DQNAgent(object):
    def __init__(self,
                 intersection_id,
                 state_size=9,
                 action_size=8,
                 batch_size=32,
                 phase_list=[],
                 timing_list=[],
                 env=None
                 ):
        self.env = env
        self.intersection_id = intersection_id
        self.state_size = state_size
        self.action_size = action_size
        self.memory = deque(maxlen=self.memory_size)
        self.batch_size = batch_size
        #$PROPERTYS$#
        self.model = dqnModel(self.state_size, self.action_size, self.learning_rate)

        self.phase_list = phase_list
        self.timing_list = timing_list

    def remember(self, state, action, reward, next_state):
        action = self.phase_list.index(action)  # index
        self.memory.append((state, action, reward, next_state))

    def remember_timing(self, state, timing, reward, next_state):
        timing = self.timing_list.index(timing)  # index
        self.memory.append((state, timing, reward, next_state))

    def choose_action(self, state):
        if np.random.uniform() < self.epsilon - self.step * 0.0002:
            return random.randrange(self.action_size)
        act_values = self.model.predict(state)
        return np.argmax(act_values[0])  # returns action

    def replay(self):  # Timing if flag for agent_timing
        if len(self.memory) < self.batch_size:
            return
        replay_batch = random.sample(self.memory, self.batch_size)
        s_batch = np.reshape(np.array([replay[0] for replay in replay_batch]), [self.batch_size, self.state_size])
        next_s_batch = np.reshape(np.array([replay[3] for replay in replay_batch]),
                                  [self.batch_size, self.state_size])

        Q = self.model.predict(s_batch)
        Q_next = self.model.predict(next_s_batch)

        lr = 1
        for i, replay in enumerate(replay_batch):
            _, a, reward, _ = replay
            Q[i][a] = (1 - lr) * Q[i][a] + lr * (reward + self.gamma * np.amax(Q_next[i]))

        # 传入网络训练
        # print("s_batch:\n", s_batch, "Q:\n", Q)
        self.model.fit(s_batch, Q, verbose=0)

    def load(self, name):
        self.model.load_weights(name)

    def save(self, name):
        self.model.save_weights(name)
        # print("model saved:{}".format(name))


def main():
    date = datetime.now().strftime('%Y%m%d_%H%M%S')
    dataset = "dongfeng"

    cityflow_config = {
        "interval": 1,
        "seed": 0,
        "laneChange": False,
        "dir": "data/",
        # "roadnetFile": "template_lsr/new/" + dataset + "/" + "roadnet_" + dataset + ".json",
        # "flowFile": "template_lsr/new/" + dataset + "/" + "syn_" + dataset + "_gaussian_500_1h.json",

        # "roadnetFile": "hangzhou_4x4_gudang_18041610_1h/roadnet_4_4.json",
        # "flowFile": "hangzhou_4x4_gudang_18041610_1h/hangzhou_4_4_gudang_18041610_1h.json",

        "roadnetFile": "dongfeng/jinan_roadnet.json",
        "flowFile": "dongfeng/jinan_flow.json",
        "rlTrafficLight": True,
        "saveReplay": False,
        "roadnetLogFile": "replayRoadNet.json",
        "replayLogFile": "replayLogFile.txt"
    }

    with open(os.path.join("data/", "cityflow.config"), "w") as json_file:
        json.dump(cityflow_config, json_file)

    config = {
        'cityflow_config_file': "data/cityflow.config",
        'epoch': 200,
        'num_step': 3600,  # 每个epoch的执行步数
        'save_freq': 1,
        'phase_step': 5,  # 每个相位的基础持续时间
        'model': 'DQN',
        'batch_size': 32
    }

    cityflow_config = json.load(open(config['cityflow_config_file']))
    roadnetFile = cityflow_config['dir'] + cityflow_config['roadnetFile']
    config["lane_phase_info"] = parse_roadnet(roadnetFile)

    intersection_id = list(config['lane_phase_info'].keys())  # all intersections
    config["intersection_id"] = intersection_id
    phase_list = {id_: config["lane_phase_info"][id_]["phase"] for id_ in intersection_id}
    config["phase_list"] = phase_list

    timing_list = {id_: [i * 5 + config['phase_step'] for i in range(1, 5)] for id_ in intersection_id}
    config['timing_list'] = timing_list

    model_dir = "model/{}_{}".format(config['model'], date)
    result_dir = "result/{}_{}".format(config['model'], date)
    config["result_dir"] = result_dir

    if not os.path.exists("model"):
        os.makedirs("model")
    if not os.path.exists("result"):
        os.makedirs("result")
    if not os.path.exists(model_dir):
        os.makedirs(model_dir)
    if not os.path.exists(result_dir):
        os.makedirs(result_dir)

    env = CityFlowEnvM(config["lane_phase_info"],
                       intersection_id,
                       num_step=config["num_step"],
                       thread_num=8,
                       cityflow_config_file=config["cityflow_config_file"]
                       )

    config["state_size"] = env.state_size

    Magents = {}
    for id_ in intersection_id:
        agent = DQNAgent(id_,
                         state_size=config["state_size"],
                         action_size=len(phase_list[id_]),
                         batch_size=config["batch_size"],
                         phase_list=phase_list[id_],
                         timing_list=timing_list[id_],
                         env=env)
        Magents[id_] = agent

    EPISODES = config['epoch']
    total_step = 0
    episode_travel_time = []

    with open(result_dir + "/" + "Travel Time-" + dataset + "-PCD-DQN.csv", 'a+') as ttf:
        ttf.write("travel time\n")
    ttf.close()

    for id_ in intersection_id:
        with open(result_dir + "/" + "Timing choose-" + dataset + "-" + id_ + ".txt", 'a+') as ttf:
            ttf.write("timing\n")
        ttf.close()

        with open(result_dir + "/" + "pressure-" + dataset + "-" + id_ + ".txt", 'a+') as ttf:
            ttf.write("pressure\n")
        ttf.close()

    #$PROINIT$#

    with tqdm(total=EPISODES * config['num_step']) as pbar:
        for i in range(EPISODES):
            timing_choose, pressure = TlStep(env, intersection_id, config, Magents, phase_list, total_step, pbar, i)
            episode_travel_time.append(env.eng.get_average_travel_time())
            with open(result_dir + "/" + "Travel Time-" + dataset + "-PCD-DQN.csv", 'a+') as ttf:
                ttf.write("{}\n".format(env.eng.get_average_travel_time()))
            ttf.close()

            for id_ in intersection_id:
                with open(result_dir + "/" + "Timing choose-" + dataset + "-" + id_ + ".txt", 'a+') as ttf:
                    ttf.write("epoch " + str(i + 1) + "\t")
                    ttf.write(str(timing_choose[id_]) + "\n\n")
                ttf.close()

                with open(result_dir + "/" + "pressure-" + dataset + "-" + id_ + ".txt", 'a+') as ttf:
                    ttf.write("epoch " + str(i + 1) + "\t")
                    ttf.write(str(pressure[id_]) + "\n\n")
                ttf.close()

            print('\n')
            print('Epoch {} travel time:'.format(i + 1), env.eng.get_average_travel_time())

        df = pd.DataFrame({"travel time": episode_travel_time})
        df.to_csv(result_dir + '/PCD.csv', index=False)

        # save figure
        plot_data_lists([episode_travel_time], ['travel time'], figure_name=result_dir + '/travel time.pdf')

#$DQNMODEL$#


#$FUNCTIONS$#


#$MQTT$#


if __name__ == '__main__':
    main()
