from datetime import datetime
from tqdm import tqdm
import pandas as pd
import paho.mqtt.client as mqtt
from copy import deepcopy
import random
from collections import deque
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout
from tensorflow.keras.optimizers import Adam
import os

from cityflow_env import CityFlowEnvM
from utility import *

os.environ["CUDA_VISIBLE_DEVICES"] = "-1"  # use GPU


class DQNAgent(object):
    def __init__(self,
                 intersection_id,
                 state_size=9,
                 action_size=8,
                 batch_size=32,
                 phase_list=[],
                 timing_list=[],
                 env=None
                 ):
        self.env = env
        self.intersection_id = intersection_id
        self.state_size = state_size
        self.action_size = action_size
        self.memory_size = 2000
        self.memory = deque(maxlen=self.memory_size)
        self.gamma = 0.95  # discount rate
        self.epsilon = 0.1  # exploration rate
        self.learning_rate = 0.001
        self.step = 0
        self.batch_size = batch_size
        self.model = dqnModel(self.state_size, self.action_size, self.learning_rate)

        self.phase_list = phase_list
        self.timing_list = timing_list

    def remember(self, state, action, reward, next_state):
        action = self.phase_list.index(action)  # index
        self.memory.append((state, action, reward, next_state))

    def remember_timing(self, state, timing, reward, next_state):
        timing = self.timing_list.index(timing)  # index
        self.memory.append((state, timing, reward, next_state))

    def choose_action(self, state):
        if np.random.uniform() < self.epsilon - self.step * 0.0002:
            return random.randrange(self.action_size)
        act_values = self.model.predict(state)
        return np.argmax(act_values[0])  # returns action

    def replay(self):  # Timing if flag for agent_timing
        if len(self.memory) < self.batch_size:
            return
        replay_batch = random.sample(self.memory, self.batch_size)
        s_batch = np.reshape(np.array([replay[0] for replay in replay_batch]), [self.batch_size, self.state_size])
        next_s_batch = np.reshape(np.array([replay[3] for replay in replay_batch]),
                                  [self.batch_size, self.state_size])

        Q = self.model.predict(s_batch)
        Q_next = self.model.predict(next_s_batch)

        lr = 1
        for i, replay in enumerate(replay_batch):
            _, a, reward, _ = replay
            Q[i][a] = (1 - lr) * Q[i][a] + lr * (reward + self.gamma * np.amax(Q_next[i]))

        # 传入网络训练
        # print("s_batch:\n", s_batch, "Q:\n", Q)
        self.model.fit(s_batch, Q, verbose=0)

    def load(self, name):
        self.model.load_weights(name)

    def save(self, name):
        self.model.save_weights(name)
        # print("model saved:{}".format(name))


def main():
    date = datetime.now().strftime('%Y%m%d_%H%M%S')
    dataset = "dongfeng"

    cityflow_config = {
        "interval": 1,
        "seed": 0,
        "laneChange": False,
        "dir": "data/",
        # "roadnetFile": "template_lsr/new/" + dataset + "/" + "roadnet_" + dataset + ".json",
        # "flowFile": "template_lsr/new/" + dataset + "/" + "syn_" + dataset + "_gaussian_500_1h.json",

        # "roadnetFile": "hangzhou_4x4_gudang_18041610_1h/roadnet_4_4.json",
        # "flowFile": "hangzhou_4x4_gudang_18041610_1h/hangzhou_4_4_gudang_18041610_1h.json",

        "roadnetFile": "dongfeng/jinan_roadnet.json",
        "flowFile": "dongfeng/jinan_flow.json",
        "rlTrafficLight": True,
        "saveReplay": False,
        "roadnetLogFile": "replayRoadNet.json",
        "replayLogFile": "replayLogFile.txt"
    }

    with open(os.path.join("data/", "cityflow.config"), "w") as json_file:
        json.dump(cityflow_config, json_file)

    config = {
        'cityflow_config_file': "data/cityflow.config",
        'epoch': 200,
        'num_step': 3600,  # 每个epoch的执行步数
        'save_freq': 1,
        'phase_step': 5,  # 每个相位的基础持续时间
        'model': 'DQN',
        'batch_size': 32
    }

    cityflow_config = json.load(open(config['cityflow_config_file']))
    roadnetFile = cityflow_config['dir'] + cityflow_config['roadnetFile']
    config["lane_phase_info"] = parse_roadnet(roadnetFile)

    intersection_id = list(config['lane_phase_info'].keys())  # all intersections
    config["intersection_id"] = intersection_id
    phase_list = {id_: config["lane_phase_info"][id_]["phase"] for id_ in intersection_id}
    config["phase_list"] = phase_list

    timing_list = {id_: [i * 5 + config['phase_step'] for i in range(1, 5)] for id_ in intersection_id}
    config['timing_list'] = timing_list

    model_dir = "model/{}_{}".format(config['model'], date)
    result_dir = "result/{}_{}".format(config['model'], date)
    config["result_dir"] = result_dir

    if not os.path.exists("model"):
        os.makedirs("model")
    if not os.path.exists("result"):
        os.makedirs("result")
    if not os.path.exists(model_dir):
        os.makedirs(model_dir)
    if not os.path.exists(result_dir):
        os.makedirs(result_dir)

    env = CityFlowEnvM(config["lane_phase_info"],
                       intersection_id,
                       num_step=config["num_step"],
                       thread_num=8,
                       cityflow_config_file=config["cityflow_config_file"]
                       )

    config["state_size"] = env.state_size

    Magents = {}
    for id_ in intersection_id:
        agent = DQNAgent(id_,
                         state_size=config["state_size"],
                         action_size=len(phase_list[id_]),
                         batch_size=config["batch_size"],
                         phase_list=phase_list[id_],
                         timing_list=timing_list[id_],
                         env=env)
        Magents[id_] = agent

    EPISODES = config['epoch']
    total_step = 0
    episode_travel_time = []

    with open(result_dir + "/" + "Travel Time-" + dataset + "-PCD-DQN.csv", 'a+') as ttf:
        ttf.write("travel time\n")
    ttf.close()

    for id_ in intersection_id:
        with open(result_dir + "/" + "Timing choose-" + dataset + "-" + id_ + ".txt", 'a+') as ttf:
            ttf.write("timing\n")
        ttf.close()

        with open(result_dir + "/" + "pressure-" + dataset + "-" + id_ + ".txt", 'a+') as ttf:
            ttf.write("pressure\n")
        ttf.close()

    with tqdm(total=EPISODES * config['num_step']) as pbar:
        for i in range(EPISODES):
            timing_choose, pressure = TlStep(env, intersection_id, config, Magents, phase_list, total_step, pbar, i)
            episode_travel_time.append(env.eng.get_average_travel_time())
            with open(result_dir + "/" + "Travel Time-" + dataset + "-PCD-DQN.csv", 'a+') as ttf:
                ttf.write("{}\n".format(env.eng.get_average_travel_time()))
            ttf.close()

            for id_ in intersection_id:
                with open(result_dir + "/" + "Timing choose-" + dataset + "-" + id_ + ".txt", 'a+') as ttf:
                    ttf.write("epoch " + str(i + 1) + "\t")
                    ttf.write(str(timing_choose[id_]) + "\n\n")
                ttf.close()

                with open(result_dir + "/" + "pressure-" + dataset + "-" + id_ + ".txt", 'a+') as ttf:
                    ttf.write("epoch " + str(i + 1) + "\t")
                    ttf.write(str(pressure[id_]) + "\n\n")
                ttf.close()

            print('\n')
            print('Epoch {} travel time:'.format(i + 1), env.eng.get_average_travel_time())

        df = pd.DataFrame({"travel time": episode_travel_time})
        df.to_csv(result_dir + '/PCD.csv', index=False)

        # save figure
        plot_data_lists([episode_travel_time], ['travel time'], figure_name=result_dir + '/travel time.pdf')

def dqnModel(state_size, action_size, learning_rate):
    model = Sequential()
    model.add(Dense(100, input_dim=state_size, activation='relu'))
    model.add(Dense(action_size, activation='linear'))
    model.compile(loss='mse', optimizer=Adam(lr=learning_rate))
    return model

def TlStep(env, intersection_id, config, Magents, phase_list, total_step, pbar, i):
    env.reset()
    state_n = {}
    action_n = {}
    next_state = {}
    action_phase = {}
    timing_phase = {}

    iid = 0
    reward_n = {}
    rest_timing = {}
    timing_choose = {}
    pressure = {}
    while iid < 12:
        id_ = intersection_id[iid]
        reward_n[id_] = 0
        rest_timing[id_] = 0
        timing_choose[id_] = []
        pressure[id_] = []
        state_n[id_] = env.get_state_(id_)
        iid = iid + 1

    num_step = config['num_step']
    episode_length = 0
    while episode_length < num_step:
        iid = 0
        while iid < 12:
            id_ = intersection_id[iid]
            t = rest_timing[id_]
            if t == 0:
                if episode_length != 0:
                    # remember and replay the last transition
                    reward_n[id_] = env.get_reward_(id_)
                    Magents[id_].remember(state_n[id_], action_phase[id_], reward_n[id_], next_state[id_])
                    Magents[id_].replay()
                    state_n[id_] = next_state[id_]

                action_n[id_] = Magents[id_].choose_action(state_n[id_])
                action_phase[id_] = phase_list[id_][action_n[id_]]

                p, timing_phase[id_] = env.get_timing_(id_, action_phase[id_])
                self.MQTT.client.publish("point", payload=str(matter.rosDevice.payload_latlon()), qos=0)
                rest_timing[id_] = timing_phase[id_]
                timing_choose[id_].append(timing_phase[id_])
                pressure[id_].append(p)
            iid = iid + 1
        next_state, reward_ = env.step(action_phase)  # one step
        total_step += 1
        pbar.update(1)
        print_reward = deepcopy(reward_n)
        pbar.set_description(
            "t_st:{}, epi:{}, st:{}, r:{} ".format(total_step, i + 1, episode_length, print_reward))

        iid = 0
        while (iid < 12):
            id_ = intersection_id[iid]
            rest_timing[id_] = rest_timing[id_] - 1
            iid = iid + 1

        episode_length = episode_length + 1
    return timing_choose, pressure

class MQTT:
    def __init__(self, name, ip, portnum, devid, proid, pwd, subscribes):
        self.client = mqtt.Client(client_id=devid, protocol=mqtt.MQTTv311)
        self.client.on_connect = self.on_connect
        self.client.on_publish = self.on_publish
        self.client.on_message = self.on_message
        self.client.username_pw_set(username=proid, password=pwd)
        self.client.connect(ip, port=portnum, keepalive=1200)
        for ss in subscribes:
            self.client.subscribe(ss, qos=0)
        self.client.loop_start()

    def on_publish(self, client, userdata, mid):
        pass

    def on_connect(self, client, userdata, flags, rc):
        pass

    def on_disconnect(self, client, userdata, flags, rc):
        pass

    def on_message(self, client, userdata, msg):
        rec = msg.payload.decode()
        data = rec.split(',')


if __name__ == '__main__':
    main()
