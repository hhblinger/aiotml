import json
import numpy as np
from websocket import create_connection


class NumpyEncoder(json.JSONEncoder):
    """ Special json encoder for numpy types """
    def default(self, obj):
        if isinstance(obj, (np.int_, np.intc, np.intp, np.int8,
            np.int16, np.int32, np.int64, np.uint8,
            np.uint16, np.uint32, np.uint64)):
            return int(obj)
        elif isinstance(obj, (np.float_, np.float16, np.float32,
            np.float64)):
            return float(obj)
        elif isinstance(obj,(np.ndarray,)): #### This is the fix
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


class WS:
    def __init__(self, name, ip, portnum, eventq):
        self.name = name
        self.ip = ip
        self.portnum = portnum
        self.ws = create_connection("ws://" + ip + ":" + portnum + "/")
        self.eventq = eventq

    #$SERIALIZERS$#
	def parse(self, msgStr):
		msg = {}
		parsed = json.loads(msgStr)
		for key in parsed:
			if key == 'terminal':
				msg.['_msg'] = 'terminal'
				msg['t'] = parsed['terminal']['t']
			elif key == 'add':
				msg.['_msg'] = 'add'
				msg['a'] = parsed['add']['a']
				msg['b'] = parsed['add']['b']
			elif key == 'start':
				msg.['_msg'] = 'start'
				msg['t'] = parsed['start']['t']
		return msg


    #$DISPATCH$#
	def receive_data(self):
		result = self.ws.recv()
		msg = self.parse(result)
		msg['_port'] = 'clock'
		if msg['_msg'] == 'start':
			self.eventq.put(msg)
		elif msg['_msg'] == 'add':
			self.eventq.put(msg)
		elif msg['_msg'] == 'terminal':
			self.eventq.put(msg)



    #$SENDMSGS$#
	def sendresultOnclock(self, result) :
		self.ws.send(self.resultToFormat(self, result['c'])

