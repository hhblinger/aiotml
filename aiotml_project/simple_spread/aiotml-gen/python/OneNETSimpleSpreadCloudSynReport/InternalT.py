class InternalT (object):
	def __init__(self):
		self.events = queue.Queue()
		self.send_events = queue.Queue()
#$PROPERTYS$#
		self.name_var = "calculator"
		self.index_var = 5


#$PROINIT$#
		self.WS = WS('WS', '''','''', self.events)


#$FUNCTIONS$#
	def add2(self, a_var, b_var):
		c_var = a_var + b_var
		return c_var



#$TRANSFUNCTION$#
	def state_internalT_MsgRs_connected_enter(self, fakePara):
		print("connected!\n")

	def transFunction_state_internalT_MsgRs_disconnectedtostate_internalT_MsgRs_connected(self,start):
		print("start!\n")
	def transFunction_state_internalT_MsgRs_connectedtostate_internalT_MsgRs_disconnected(self,terminal):
		print("end!\n")


#$MAINLOOP$#
	def Mainloop(self):
		if not self.events.empty():
			msg = self.events.get()
			if msg['_msg'] == 'start':
				self.start(msg)
			elif msg['_msg'] == 'terminal':
				self.terminal(msg)
		if not matter.send_events.empty():
			msg = matter.send_events.get()
			if msg["_msg"] == "msg_port!actionmsg":
				matter.WS.sendactionmsgOnmsg_port(msg["agent_action"])
			elif msg["_msg"] == "msg_port!reset":
				matter.WS.sendresetOnmsg_port(msg["y"])
			elif msg["_msg"] == "msg_port!done":
				matter.WS.senddoneOnmsg_port(msg["y"])
		if self.state == 'state_internalT_MsgRs_disconnected':
			self.WS.receive_data()
		elif self.state == 'state_internalT_MsgRs_connected':
			self.WS.receive_data()


def CreateStateMachine():
#$STATECREATE$#
	matter = InternalT()
	states = ["state_internalT_MsgRs_disconnected","state_internalT_MsgRs_connected"]
	machine = Machine(model=matter, states=states, initial= 'state_internalT_MsgRs_disconnected')
	machine.on_enter_state_internalT_MsgRs_connected('state_internalT_MsgRs_connected_enter')
	machine.add_transition('start', 'state_internalT_MsgRs_disconnected', 'state_internalT_MsgRs_connected', before='transFunction_state_internalT_MsgRs_disconnectedtostate_internalT_MsgRs_connected')
	machine.add_transition('terminal', 'state_internalT_MsgRs_connected', 'state_internalT_MsgRs_disconnected', before='transFunction_state_internalT_MsgRs_connectedtostate_internalT_MsgRs_disconnected')
	pass