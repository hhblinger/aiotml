import time
import paho.mqtt.client as mqtt
import RPi.GPIO as GPIO

#$MQTT$#
class MQTT:
    def __init__(self, name, ip, portnum, devid, proid, pwd, subscribes):
        self.client = mqtt.Client(client_id=devid, protocol=mqtt.MQTTv311)
        self.client.on_connect = self.on_connect
        self.client.on_publish = self.on_publish
        self.client.on_message = self.on_message
        self.client.username_pw_set(username=proid, password=pwd)
        self.client.connect(ip, port=portnum, keepalive=1200)
        for ss in subscribes:
            self.client.subscribe(ss, qos=0)
        self.client.loop_start()

    def on_publish(self, client, userdata, mid):
        pass

    def on_connect(self, client, userdata, flags, rc):
        pass

    def on_disconnect(self, client, userdata, flags, rc):
        pass

    def on_message(self, client, userdata, msg):
        rec = msg.payload.decode()
		rec = rec[1:len(rec)-1]
        data = rec.split(',')
		if data[2] == self.name:
			LightControl(int(data[0]),int(data[1]))


#$FUNCTIONS$#
def LightControl(command_var, duration_var):
	GPIO.setmode(GPIO.BCM)
	GPIO.setup(17, GPIO.OUT)
	GPIO.setup(27, GPIO.OUT)
	GPIO.setup(22, GPIO.OUT)
	if command_var == 0 : 
		print("close\n ")
		GPIO.output(17, GPIO.LOW)
		GPIO.output(27, GPIO.LOW)
		GPIO.output(22, GPIO.LOW)
		time.sleep(duration)
	if command_var == 1 : 
		print("East West\n ")
		GPIO.output(17, GPIO.HIGH)
		GPIO.output(27, GPIO.LOW)
		GPIO.output(22, GPIO.LOW)
		time.sleep(duration)
	if command_var == 2 : 
		print("South North\n ")
		GPIO.output(17, GPIO.LOW)
		GPIO.output(27, GPIO.HIGH)
		GPIO.output(22, GPIO.LOW)
		time.sleep(duration)
	if command_var == 3 : 
		print("Yellow\n ")
		GPIO.output(17, GPIO.LOW)
		GPIO.output(27, GPIO.LOW)
		GPIO.output(22, GPIO.HIGH)
		time.sleep(duration)



def main():
    #$PROINIT$#
	subscribes = ['singleLightActionMsg']
	mqtt = MQTT('intersection_1_1', '183.230.40.39', 6002, '760666463', '451976', 'xFBavuTR=mZ4MJOvHV9ipGx8p28=', subscribes)


    while (True):
        pass

if __name__ == '__main__':
    main()