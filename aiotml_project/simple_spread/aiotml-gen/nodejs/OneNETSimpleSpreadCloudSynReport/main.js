'use strict';

const Enum = require('./enums');
const InternalT = require('./InternalT');
/*$REQUIRE_PLUGINS$*/
const websocket = require('./WSJS');


const inst_it = new InternalT('it', null);
inst_it.Name_var = 'calculator';
inst_it.Index_var = 5;

/*$PLUGINS$*/
const ws = new websocket("WS", false, 9010, inst_it);

/*Connecting internal ports...*/
/*Connecting ports...*/
inst_it.bus.on('clock!result', (result) => ws.receiveresultOnclock(result));

/*$PLUGINS_CONNECTORS$*/
inst_it._init();
/*$PLUGINS_END$*/

function terminate() {
	inst_it._stop();
	inst_it._delete();
};
/*terminate all things on SIGINT (e.g. CTRL+C)*/
if (process && process.on) {
	process.on('SIGINT', function() {
		terminate();
		ws._stop();
/*$STOP_PLUGINS$*/

		setTimeout(() => {
			process.exit();
		}, 1000);
	});
}
