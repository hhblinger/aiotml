var Terminal = /** @class */ (function () {
  function Terminal(port,...params) {
    this.type = 'terminal';
    this.port = port;
    this.t = params[0];
  }

  Terminal.prototype.is = function (type) {
    return this.type === type;
  };

  Terminal.prototype.toString = function () {
    return 'event ' + this.type + '?' + this.port + '(' + t + ')';
  };

  return Terminal;
}());
exports.Terminal = Terminal;

var Start = /** @class */ (function () {
  function Start(port,...params) {
    this.type = 'start';
    this.port = port;
    this.t = params[0];
  }

  Start.prototype.is = function (type) {
    return this.type === type;
  };

  Start.prototype.toString = function () {
    return 'event ' + this.type + '?' + this.port + '(' + t + ')';
  };

  return Start;
}());
exports.Start = Start;

var Result = /** @class */ (function () {
  function Result(port,...params) {
    this.type = 'result';
    this.port = port;
    this.c = params[0];
  }

  Result.prototype.is = function (type) {
    return this.type === type;
  };

  Result.prototype.toString = function () {
    return 'event ' + this.type + '?' + this.port + '(' + c + ')';
  };

  return Result;
}());
exports.Result = Result;

var Add = /** @class */ (function () {
  function Add(port,...params) {
    this.type = 'add';
    this.port = port;
    this.a = params[0];
    this.b = params[1];
  }

  Add.prototype.is = function (type) {
    return this.type === type;
  };

  Add.prototype.toString = function () {
    return 'event ' + this.type + '?' + this.port + '(' + a + ', ' + b + ')';
  };

  return Add;
}());
exports.Add = Add;

