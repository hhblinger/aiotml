const WebSocketServer = require('ws').Server;
const events = require('./events');
const Format = require('./WSStringProtocol');
global.gws = null;
function WS(name, debug, portnum, instance) {
    this.name = name;
    this.debug = debug;
    this.ready = false;

    this.formatter = new Format();
    this.wss = new WebSocketServer({port:portnum});

	this.wss.on('connection', function (ws) {
	    /*$CALLBACK$*/
        console.log(`[SERVER] connection()`);
        gws = ws;
        ws.on('message', function (data) {
        console.log(`[SERVER] Received: ${data}`);
        const formatter = new Format();
        const msg = formatter.parse(data);
        /*$DISPATCH$*/
msg._port = 'clock';
switch(msg._msg) {
case'start':
var outEvent = new events.Start('clock', msg.t);
instance._receive(outEvent);
break;
case'add':
var outEvent = new events.Add('clock', msg.a, msg.b);
instance._receive(outEvent);
break;
case'terminal':
var outEvent = new events.Terminal('clock', msg.t);
instance._receive(outEvent);
break;
default: break;
}

        })
        
     })
    
}

/*$RECEIVERS$*/
WS.prototype.receiveresultOnclock = function(result) {
gws.send(this.formatter.resultToFormat(result.c), function ack(error) {if(error) console.error("error: " + error);});
};



WS.prototype._stop = function() {
	this.wss.close();
};

module.exports = WS;
