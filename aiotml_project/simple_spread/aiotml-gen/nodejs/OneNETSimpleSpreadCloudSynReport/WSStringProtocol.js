function WSStringProtocol(){
WSStringProtocol.prototype.parse = function(json) {
const msg = {};
try {
const parsed = JSON.parse(json);
JSON.parse(json, function(k, v) {
switch(k) {
case 'terminal':
msg._msg = 'terminal';
msg.t = parsed.terminal.t;
break;
case 'start':
msg._msg = 'start';
msg.t = parsed.start.t;
break;
case 'add':
msg._msg = 'add';
msg.a = parsed.add.a;
msg.b = parsed.add.b;
break;
default: break;
}
});
} catch (err) {
console.log("Cannot parse " + json + " because " + err);
};
return msg;
};

WSStringProtocol.prototype.resultToFormat = function(c) {
return JSON.stringify({result: {c : c}});
};

};

module.exports = WSStringProtocol;
