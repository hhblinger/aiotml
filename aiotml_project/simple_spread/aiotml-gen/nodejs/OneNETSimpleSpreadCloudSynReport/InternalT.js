'use strict';

const Enum = require('./enums');
const Event = require('./events');
const StateJS = require('@steelbreeze/state');
const EventEmitter = require('events').EventEmitter;
const util = require('util');


/*
 * Definition for type : internalT
 */

function InternalT(name, root) {
	this.name = name;
	this.root = (root === null)? this : root;
	this.ready = false;
	this.bus = (root === null)? new EventEmitter() : this.root.bus;
	
	this.build(name);
}

InternalT.prototype.build = function(session) {
	/*State machine (states and regions)*/
	/*Building root component*/
	this._statemachine = new StateJS.State('MsgRs').entry(() => {
		((process.stdout && process.stdout.write) || console.log).call(process.stdout, ''+'Init!\n');
	});
	let _initial_internalT_MsgRs = new StateJS.PseudoState('_initial', this._statemachine, StateJS.PseudoStateKind.Initial);
	let internalT_MsgRs_disconnected = new StateJS.State('disconnected', this._statemachine);
	let internalT_MsgRs_connected = new StateJS.State('connected', this._statemachine).entry(() => {
		((process.stdout && process.stdout.write) || console.log).call(process.stdout, ''+'connected!\n');
	});
	_initial_internalT_MsgRs.to(internalT_MsgRs_disconnected);
	internalT_MsgRs_connected.to(internalT_MsgRs_disconnected).on(Event.Terminal).when((terminal) => {
		return terminal.port === 'clock' && terminal.type === 'terminal';
	}).effect((terminal) => {
		((process.stdout && process.stdout.write) || console.log).call(process.stdout, ''+'end!\n');
	});
	internalT_MsgRs_disconnected.to(internalT_MsgRs_connected).on(Event.Start).when((start) => {
		return start.port === 'clock' && start.type === 'start';
	}).effect((start) => {
		((process.stdout && process.stdout.write) || console.log).call(process.stdout, ''+'start!\n');
	});
	internalT_MsgRs_connected.on(Event.Add).when((add) => {
		return add.port === 'clock' && add.type === 'add';
	}).effect((add) => {
		let c_var = this.add2(add.a, add.b);
		setImmediate(() => {this.bus.emit('clock!result', new Event.Result('clock'))});
	});
}
InternalT.prototype.add2 = function(a_var, b_var) {
	let c_var = a_var + b_var;
	return c_var;
}

InternalT.prototype._stop = function() {
	this.root = null;
	this.ready = false;
}

InternalT.prototype._delete = function() {
	this._statemachine = null;
	this._MsgRs_instance = null;
	this.bus.removeAllListeners();
}

InternalT.prototype._init = function() {
	this._MsgRs_instance = new StateJS.Instance("MsgRs_instance", this._statemachine);
	this.ready = true;
}

InternalT.prototype._receive = function(msg) {
	if (this.ready) {
		this._MsgRs_instance.evaluate(msg);
	} else {
		setTimeout(()=>this._receive(msg),0);
	}
}

InternalT.prototype.toString = function() {
	let result = 'instance ' + this.name + ':' + this.constructor.name + '\n';
	result += '\n\tname = ' + this.name_var;
	result += '\n\tindex = ' + this.index_var;
	result += '';
	return result;
}

module.exports = InternalT;
