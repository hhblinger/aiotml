<font color=black size=5>一、AIOTML</font>

<font color=red size=4>1.环境搭建及使用</font>

（1）下载eclipse-dsl版本，安装后打开AIOTML源代码文件夹AIOTML，导入源代码文件夹下各个文件夹，**注意compiler.bundle和parent开头的两个文件夹不要导入**。
![](images/1.PNG)
<center>`源代码目录结构`</center>

（2）安装aiotml插件

eclipse安装插件：`Help -> Install New Software... -> Add`，选择文件夹路径：`AIOTML -> new`，勾选 AiotML Feature 确认即可等待安装，安装完成后可选择重启eclipse。
![](images/2.PNG)
<center>`安装AiotML插件`</center>

（3）运行aiotml生成过程

在源代码目录结构中右击第一条：aiotml[AIoTML master]，Run as eclipse application，在弹出的窗口中选择第一条Launch Runtime Eclipse，运行插件；

选择已经编写好的aiotml文件进行生成：这里选择路径：`aiotml_project -> simple_spread -> src`，这个文件夹中都是已经编写好的 aiotml 文件，以 algorithm4 为例，右键 `HEADS/AiotML -> python`，生成完毕。

在路径：`aiotml_project -> simple_spread -> aiotml-gen`中可以找到生成的python文件
![](images/3.PNG)
![](images/4.PNG)
<center>`运行AiotML插件`</center>

<font color=red size=4>2.开发AiotML-以python为例</font>

（1）python编译源文件路径：

    compilers.python -> src -> ...五个java源文件：
    PyCompiler.java, PyContext,java, ..., PyThingImplCompiler.java

aiotml生成背景模板路径：

    compilers.python -> src -> main -> resources

语法路径：

    aiotml -> src -> org.aiotml.xtext -> AiotML.xtext，

这里包括了aiotml的所有语法，修改语法需要在这里进行

（2）修改语法

在 AiotML.xtext 文件中修改了AiotML语法之后，需要重新编译aiotml，具体流程为：

首先，右键 AiotML.xtext 选择 `Run as -> generate Xtext artifacts`;

然后，在路径 `aiotml -> src -> org.aiotml.xtext.scoping`中的 AiotMLScopeProvided.xtend 文件中为修改的语法添加函数，以寻找添加的语法中的关键词，具体添加方式可以参考其中函数的写法，如 zipEnumerate_Fname；

接下来，右击根目录下 AiotML-1.0[AiotML master] ，Run as -> 
run configurations, 在打开的窗口中选择 `Maven Build -> New_configuration`, 在Goals栏中输入`clean install`，确认即可重新编译出插件。第一次导出jar的过程会非常慢，后面就会非常快了。
![](images/5.PNG)
<center>`重新导出插件jar包`</center>

新编译出的jar包在路径 `AIoTML\compilers\bundle\target`中，即compilers.bundle-2.0.0-SNAPSHOT.jar。将其改名为compilers.bundle_2.0.0.202012030618.jar，并替换 \eclipse\plugins 路径中的同名文件， 并将jar包备份在路径 \AIoTML\new\plugins 中。

删除eclipse中已经安装的旧的 aiotml插件，然后以前面提到的方式重新选择new文件夹，安装改完语法后的aiotml插件。安装后重启eclipse后即可成功。

（3）修改语法解析规则

只需要修改python编译源文件路径：

    compilers.python -> src -> ...五个java源文件：
    PyCompiler.java, PyContext,java, ..., PyThingImplCompiler.java

中的五个源文件即可。

<font color=red size=3>【注意】修改compiler文件夹中任何一个文件，都需要maven重新编译出jar包，重新安装aiotml插件方可。</font>



<font color=red size=4>DEMO：强化学习控制Ros自动泊车</font>